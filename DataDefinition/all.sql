﻿-- TABLE DEFINITION
CREATE TABLE MAHASISWA (
	NPM 					CHAR(10),
	Nama					VARCHAR(100)	NOT NULL,
	Username				VARCHAR(30)		NOT NULL,
	Password				VARCHAR(20)		NOT NULL,
	Email					VARCHAR(100)	NOT NULL,
	Email_Alternatif		VARCHAR(100),
	Telepon					VARCHAR(100),
	Notelp					VARCHAR(100),
	PRIMARY KEY(NPM)
);

CREATE TABLE TERM (
	Tahun		INT	,
	Semester	INT	,
	UNIQUE(Tahun,Semester),
	PRIMARY KEY(Tahun,Semester)
);

CREATE TABLE PRODI (

	IDProdi		INT,
	NamaProdi	VARCHAR(50)		NOT NULL,
	PRIMARY KEY(IDProdi)

);

CREATE TABLE DOSEN (

	NIP 		VARCHAR(50),
	Nama		VARCHAR(100)	NOT NULL,
	Username	VARCHAR(50)		NOT NULL,
	Password	VARCHAR(50)		NOT NULL,
	Email		VARCHAR(100)	NOT NULL,
	Institusi	VARCHAR(100)	NOT NULL,
	PRIMARY KEY(NIP)
);

CREATE TABLE JENISMKS (
	ID	INT,
	NamaMKS		VARCHAR(50)		NOT NULL,
	UNIQUE(NamaMks),
	PRIMARY KEY(ID)
);

CREATE TABLE MATA_KULIAH_SPESIAL (
	IDMKS			INT UNIQUE,
	NPM				CHAR(10),
	Tahun			INT,
	Semester		INT,
	Judul			VARCHAR(250)	NOT NULL,
	IsSiapSidang	Boolean			DEFAULT FALSE,
	PengumpulanHardCopy		Boolean		DEFAULT FALSE,
	IjinMajuSidang		Boolean		DEFAULT FALSE,
	IdJenisMKS		INT NOT NULL,
	PRIMARY KEY(IDMKS, NPM, Tahun, Semester),
	FOREIGN KEY(IdJenisMKS) REFERENCES JENISMKS(ID),
	FOREIGN KEY(Tahun, Semester) REFERENCES TERM(Tahun, Semester)
);


CREATE TABLE DOSEN_PEMBIMBING (
	IDMKS	INT,
	NIPDosenPembimbing		VARCHAR(20),
	PRIMARY KEY(IDMKS,NIPDosenPembimbing),
	FOREIGN KEY(IDMKS) REFERENCES MATA_KULIAH_SPESIAL(IDMKS),
	FOREIGN KEY(NIPDosenPembimbing) REFERENCES DOSEN(NIP)
);

CREATE TABLE SARAN_DOSEN_PENGUJI (
	IDMKS	INT,
	NIPSaranPenguji		VARCHAR(20),
	PRIMARY KEY(IDMKS,NIPSaranPenguji),
	FOREIGN KEY(IDMKS) REFERENCES MATA_KULIAH_SPESIAL(IDMKS),
	FOREIGN KEY(NIPSaranPenguji) REFERENCES DOSEN(NIP)
);

CREATE TABLE DOSEN_PENGUJI (
	IDMKS	INT,
	NIPDosenPenguji		VARCHAR(20),
	PRIMARY KEY(IDMKS,NIPDosenPenguji),
	FOREIGN KEY(IDMKS) REFERENCES MATA_KULIAH_SPESIAL(IDMKS),
	FOREIGN KEY(NIPDosenPenguji) REFERENCES DOSEN(NIP)
);

CREATE TABLE TIMELINE (
	IDTimeline	INT,
	NamaEvent	VARCHAR(100)	NOT NULL,
	Tanggal		DATE	NOT NULL,
	Tahun		INT		NOT NULL,
	Semester	INT		NOT NULL,
	PRIMARY KEY(IDTimeline),
	FOREIGN KEY(Tahun,Semester) REFERENCES TERM(Tahun,Semester)
);

CREATE TABLE JADWAL_NON_SIDANG (
	IDJadwal	INT,
	TanggalMulai	DATE	NOT NULL,
	TanggalSelesai	DATE	NOT NULL,
	Alasan		VARCHAR(100)	NOT NULL,
	Repetisi	VARCHAR(50),
	NIPDosen 	VARCHAR(10) NOT NULL,
	PRIMARY KEY(IDJadwal),
	FOREIGN KEY(NIPDosen) REFERENCES DOSEN(NIP)
);

CREATE TABLE RUANGAN (
	IDRuangan INT,
	NamaRuangan		VARCHAR(20)		NOT NULL,
	PRIMARY KEY(IDRuangan),
	UNIQUE(NamaRuangan)
);

CREATE TABLE JADWAL_SIDANG (
	IDJadwal	INT,
	IDMKS	INT,
	Tanggal 	DATE	NOT NULL,
	JamMulai	TIME 	NOT NULL,
	JamSelesai	TIME 	NOT NULL,
	IDRuangan	INT		NOT NULL,
	PRIMARY KEY(IDJadwal, IDMKS),
	FOREIGN KEY(IDMKS) REFERENCES MATA_KULIAH_SPESIAL(IDMKS),
	FOREIGN KEY(IDRuangan) REFERENCES RUANGAN(IDRuangan)
);

CREATE TABLE BERKAS (
	IDBerkas	INT,
	IDMKS	INT,
	Nama 	VARCHAR(100)	NOT NULL,
	Alamat	VARCHAR(100)	NOT NULL,
	PRIMARY KEY(IDBerkas, IDMKS),
	FOREIGN KEY(IDMKS) REFERENCES MATA_KULIAH_SPESIAL(IDMKS)
);
-- END DATA DEFINITION --

-- MAHASISWA DATA --
INSERT INTO MAHASISWA VALUES ('1506724505', 'Fersandi Pratama', 'fersandi', 'tralalala', 'fersandi.pratama@ui.ac.id', 'fersandi_pratama@rocketmail.com', '081367173222', '0711410289');
INSERT INTO MAHASISWA VALUES ('1506688752', 'Aji Imawan Omi', 'aji52', 'aaabbcc', 'aji.imawan@ui.ac.id', NULL, '081323432431', '02132421231');
INSERT INTO MAHASISWA VALUES ('1506688765', 'Alif Fadila Safriyanto', 'alif65', 'dawqews', 'alif.fadila@ui.ac.id', NULL, '0854231321', '0213132131');
INSERT INTO MAHASISWA VALUES ('1506688834', 'Adityo Anggraito', 'adityo34', 'adsaswqe', 'adityo.anggraito@ui.ac.id', 'adityo@gmail.com', '08123646', '021437989');
INSERT INTO MAHASISWA VALUES ('1506688872', 'Vitosavero Avila Wibisono', 'Vitosavero72', 'dsaq3oiu', 'vitosavero.avila@ui.ac.id', 'vito_avila@yahoo.co.id', '08434234324', '0219382429');
INSERT INTO MAHASISWA VALUES ('1506688986', 'Andri Nur Rochman', 'andri86', 'skjhq4o3u23', 'andri.nur@ui.ac.id', 'andri86@live.net', '0832132341', '021243243');
INSERT INTO MAHASISWA VALUES ('1506689194', 'Nurhaya Kushadi Gitasari', 'nurhaya94', 'K*CSYsd', 'nurhaya.kushadi@ui.ac.id', 'nurhaya_kus@yahoo.com', '085364237', '0215430953');
INSERT INTO MAHASISWA VALUES ('1506689345', 'Bella Nadhifah Agustina', 'bella45', 'bbeellllaa', 'bella.nadhifah', '', '', '');
INSERT INTO MAHASISWA VALUES ('1506689396', 'Ivone Dona Khoirun Nisa', 'ivone96', 'qwerty', 'ivone.dona@ui.ac.id', NULL, '08934543222', NULL);
INSERT INTO MAHASISWA VALUES ('1506689452', 'Asyraf Ahmad Nadhil', 'asyraf52', 'asddfgh', 'asyraf.ahmad@ui.ac.id', 'asyrah@gmail.com', NULL, NULL);
INSERT INTO MAHASISWA VALUES ('1506689490', 'Enrico Jano Fiandi', 'enrico90', 'qwertyuio', 'enrico.jano@ui.ac.id', 'enrico.jano@gmail.com', '08984324212', NULL);
INSERT INTO MAHASISWA VALUES ('1506689585', 'Maktal Sakriadhi', 'maktal85', 'zxcvbfdsa', 'maktal51@ui.ac.id', 'maktal@yahoo.com', NULL, '0212312');
INSERT INTO MAHASISWA VALUES ('1506689641', 'Rizkah Shalihah', 'rizkah41', 'poiurerw', 'rizkah.shalihah@ui.ac.id', 'rizkahSha@gmail.com', '087312334', '021435432');
INSERT INTO MAHASISWA VALUES ('1506690391', 'Ibrahim', 'ibrahim91', 'AODsjdq203', 'ibrahim51@ui.ac.id', 'ibamGaring@gmail.com', '08213211', '021332423');
INSERT INTO MAHASISWA VALUES ('1506690460', 'Jiwo Nusantara Adji', 'jiwo60', 'llfjdkLQq3l', 'jiwo.nusantara@ui.ac.id', 'jiwo60@facebook.com', '0823123432', '02143521165');
INSERT INTO MAHASISWA VALUES ('1506721794', 'Ahmad Faqih', 'ahmad94', '213wzxc', 'ahmad.faqih@ui.ac.id', NULL, NULL, NULL);
INSERT INTO MAHASISWA VALUES ('1506722746', 'Muhammad Farhan', 'farham46', '31332rewds', 'muhammad.farhan@ui.ac.id', 'farhan51@yahoo.com', NULL, '0213343221');
INSERT INTO MAHASISWA VALUES ('1506722752', 'Ahmad Yazid', 'yazid52', 'pqwkd3q90', 'ahmad.yazid@ui.ac.id', NULL, '084132133231', '0213242341');
INSERT INTO MAHASISWA VALUES ('1506722765', 'Jordan Muhammad Andrianda', 'jordan65', 'ln31wOISA#', 'jordan.muhammad@ui.ac.id', 'jordan_andrianda@yahoo.com', '0832143223', '023212343');
INSERT INTO MAHASISWA VALUES ('1506722771', 'Muhammad Faiz', 'faiz71', 'fdsf343w', 'muhammad.faiz@ui.ac.id', 'faiz71@facebook.com', '08432123341', '02132415');
INSERT INTO MAHASISWA VALUES ('1506722821', 'Muhammad Givari Arnanda', 'gipaw21', 'sdofjoi32', 'muhammad.givari@ui.ac.id', 'givari51@gmail.com', '084312353232', '021323212');
INSERT INTO MAHASISWA VALUES ('1506722840', 'Muhammad Ramadhan', 'ramadhan40', 'qwe32ewIU', 'muhammad.ramadhan@ui.ac.id', NULL, '08312432213', NULL);
INSERT INTO MAHASISWA VALUES ('1506722853', 'Maula Faiz Adli', 'maula53', 'QwErTy', 'maula.faiz@ui.ac.id', 'molamoli@fakedomain.com', '0832134345', '021324312');
INSERT INTO MAHASISWA VALUES ('1506722866', 'Hizkia Nugroho', 'hizkia66', 'PAS93S3w', 'hizkia.nugroho@ui.ac.id', 'hizkia.nugroho@gmail.com', NULL, NULL);
INSERT INTO MAHASISWA VALUES ('1506722872', 'Arfi Renaldi', 'arfi72', 'AS32ASDw', 'arfi.renaldi@ui.ac.id', 'arfi_renaldi@rocketmail.com', '0812345312', '043212543');
INSERT INTO MAHASISWA VALUES ('1506723572', 'Muhammad Faisal Hardin Rangkuti', 'faisal72', 'ASQ2iwe3S', 'muhammad.faisal72@ui.ac.id', 'faisal_hardin@gmail.com', NULL, NULL);
INSERT INTO MAHASISWA VALUES ('1506724354', 'Valian Fil Ahli', 'valian54', ':K)#E:LD', 'valian.fil@ui.ac.id', 'valianahli@facebook.com', '0821324312', '02198443');
INSERT INTO MAHASISWA VALUES ('1506724606', 'Oksa Akbar Runa', 'oksa06', ')(asiU)', 'oksa.akbar@ui.ac.id', NULL, NULL, NULL);
INSERT INTO MAHASISWA VALUES ('1506725003', 'William Adjandra Hogan', 'william03', 'ASD309LDS', 'william.adjandra@ui.ac.id', 'williamhogan@yahoo.co.id', '0895495343', '0214366445');
INSERT INTO MAHASISWA VALUES ('1506725041', 'Jonathan Prasetya W.', 'jonathan41', 'ADS(#EWDSF', 'jonathan.Prasetya@ui.ac.id', 'jonathan.Prasetya@gmail.com', '0821343221', '021324235');
INSERT INTO MAHASISWA VALUES ('1506728453', 'Faqrul Anshurulloh', 'faqrul53', '#ewrw43324', 'faqrul.andhurulloh@ui.ac.id', 'faqrul51@facebook.com', NULL, '02132423');
INSERT INTO MAHASISWA VALUES ('1506730350', 'Ricky Putra Nursalim', 'ricky50', 'laskjdewq', 'ricky.putra@ui.ac.id', 'ricky.putra@gmail.com', '083124434', '0214353412');
INSERT INTO MAHASISWA VALUES ('1506731580', 'Farah Agia Ramadhina', 'farah80', '32rwfdsr23', 'farah.agia@ui.ac.id', 'farah.agia@gmail.com', '081234322', '021432143');
INSERT INTO MAHASISWA VALUES ('1506737666', 'Ismail Shalih Abdul Kholiq', 'ismail66', 'dslfJ#(ED3', 'ismail.shalih@ui.ac.id', NULL, '082314533', '02143243123');
INSERT INTO MAHASISWA VALUES ('1506738492', 'Muhammad Azri Renandi P. S', 'azri92', 'sai322', 'muhammad.azri@ui.ac.id', 'muhammad.azri@gmail.com', '0823143333', '02133543');
INSERT INTO MAHASISWA VALUES ('1506757440', 'Julio Jaya Handoyo', 'julio40', 'das231jk21', 'julio.jaya@gmail.com', 'julio.jaya@gmail.com', '0821343123', '0214323565');
INSERT INTO MAHASISWA VALUES ('1506757453', 'Joshua Valdamer Ghibran', 'joshua53', 'DSALJEOI', 'joshua.valdamer@ui.ac.id', NULL, '08213321343', '02164523');
INSERT INTO MAHASISWA VALUES ('1506757472', 'Kenny Reida Dharmawan', 'kenny72', 'SAIJ3o2ie', 'kenny.reida@ui.ac.id', 'kenny.reida@gmail.com', NULL, '082134312');
INSERT INTO MAHASISWA VALUES ('1506757522', 'Joachim Rachmad', 'joachim22', 'ASD@WIOE#', 'joachim.rachmad@ui.ac.id', 'joachim51@yahoo.com', '081234331', '02145678');
INSERT INTO MAHASISWA VALUES ('1506757655', 'Jessica Susanto', 'jessica55', 'ASDASEWQ', 'jessica.susanto@ui.ac.id', 'jessica.susanto@gmail.com', '082134324', '021432415');

INSERT INTO MAHASISWA VALUES ('1084659908', 'Natalie Cole', 'natalie76', 'dX2tays3OWQiLfQZ', 'natalie7656k@ui.ac.id', 'natalie76NHO@gmail.com', '0834985433535', '021671962');
INSERT INTO MAHASISWA VALUES ('1080591323', 'Lorenzo Harmon', 'lorenzo35', 'rfDQZF3iM', 'lorenzo35ZKs@ui.ac.id', 'lorenzo35gft@gmail.com', '0847068937911', '021127232');
INSERT INTO MAHASISWA VALUES ('1072915681', 'Mildred Murphy', 'mildred29', '1l31azrEK12N83JsmQFx', 'mildred29odS@ui.ac.id', 'mildred29PwN@gmail.com', '0883243764612', '021921580');
INSERT INTO MAHASISWA VALUES ('1041023494', 'Timothy Moran', 'timothy70', 'rIFvZdNOAW383B5Gzr', 'timothy70d7B@ui.ac.id', 'timothy70bWP@gmail.com', '0873324259126', '021503514');
INSERT INTO MAHASISWA VALUES ('1074369632', 'Vivian Powell', 'vivian24', 'nhe10HVGAu', 'vivian24GbM@ui.ac.id', 'vivian249tH@gmail.com', '0893347420671', '021685021');
INSERT INTO MAHASISWA VALUES ('1061170584', 'Scott Santos', 'scott57', '7NoshPRzKgOZosFIswd', 'scott57wgb@ui.ac.id', 'scott57YcS@gmail.com', '0852048961110', '021631501');
INSERT INTO MAHASISWA VALUES ('1024966894', 'Irving Vaughn', 'irving81', 'WrUooxYRdBhzdOYzh', 'irving81zlh@ui.ac.id', 'irving811iL@gmail.com', '0843784352179', '021353010');
INSERT INTO MAHASISWA VALUES ('1072553281', 'Jasmine Taylor', 'jasmine55', 'GLR5zZDcR', 'jasmine556m0@ui.ac.id', 'jasmine55dEK@gmail.com', '0880076394755', '021561129');
INSERT INTO MAHASISWA VALUES ('1099383131', 'Marguerite Jefferson', 'marguerite65', 'IkhbSFWm7xHKXPAhekF0', 'marguerite65GI4@ui.ac.id', 'marguerite65JOg@gmail.com', '0820780048153', '021182103');
INSERT INTO MAHASISWA VALUES ('1086770252', 'Audrey Black', 'audrey97', '6prBPI4YxJ', 'audrey97XjE@ui.ac.id', 'audrey97ROE@gmail.com', '0882791435932', '021543622');
INSERT INTO MAHASISWA VALUES ('1091370446', 'Marilyn Hoffman', 'marilyn93', 'N9yEkxBOVZJhiE', 'marilyn93F3M@ui.ac.id', 'marilyn93dem@gmail.com', '0867053594459', '021984553');
INSERT INTO MAHASISWA VALUES ('1061298167', 'Francis Little', 'francis08', 'nwSsukSjhP', 'francis08iAG@ui.ac.id', 'francis08a0z@gmail.com', '0890931996086', '021442075');
INSERT INTO MAHASISWA VALUES ('1055423676', 'Orville Soto', 'orville94', 'pPtbaoSyTvF2m8vx', 'orville94mJx@ui.ac.id', 'orville94Yed@gmail.com', '0858169012821', '021877963');
INSERT INTO MAHASISWA VALUES ('1054054119', 'Lynn Wheeler', 'lynn80', 'KnQy107yp2mlLdyfnV5D', 'lynn80xqW@ui.ac.id', 'lynn80Ket@gmail.com', '0837931061285', '021031851');
INSERT INTO MAHASISWA VALUES ('1051442990', 'Wade Burton', 'wade95', 'ZmzIFdquNOwsa7MG', 'wade95Ip4@ui.ac.id', 'wade95hRu@gmail.com', '0811651654917', '021723279');
INSERT INTO MAHASISWA VALUES ('1090089307', 'Phillip Yates', 'phillip87', 'GinJmZ4ouGVAzOnqjj', 'phillip87igG@ui.ac.id', 'phillip87qag@gmail.com', '0864380283176', '021903493');
INSERT INTO MAHASISWA VALUES ('1074653489', 'Christina Anderson', 'christina74', 'ZSTH43fn8W5GRi', 'christina74Vcm@ui.ac.id', 'christina74wuc@gmail.com', '0896376916677', '021599251');
INSERT INTO MAHASISWA VALUES ('1086497673', 'Kelly Cook', 'kelly01', '5lFccx4iUPJuvvfBqt', 'kelly01Lta@ui.ac.id', 'kelly01H7K@gmail.com', '0866415858846', '021834381');
INSERT INTO MAHASISWA VALUES ('1073354193', 'Warren Chandler', 'warren56', '1n1DfE64kD', 'warren560kx@ui.ac.id', 'warren56Jdv@gmail.com', '0815537219153', '021360119');
INSERT INTO MAHASISWA VALUES ('1067275526', 'Virgil Clark', 'virgil66', 'VGcL66nJY6vjpimEvpik', 'virgil66jHn@ui.ac.id', 'virgil66lKZ@gmail.com', '0898422992413', '021984720');
INSERT INTO MAHASISWA VALUES ('1025909797', 'Elisa Saunders', 'elisa46', 'cGnTQn4IqxAItUfB9', 'elisa46nBc@ui.ac.id', 'elisa462O0@gmail.com', '0816872025113', '021034394');
INSERT INTO MAHASISWA VALUES ('1055660493', 'Florence Summers', 'florence81', 'iYScqJCEii4s7', 'florence81jRH@ui.ac.id', 'florence81sfo@gmail.com', '0814566996305', '021639093');
INSERT INTO MAHASISWA VALUES ('1086042928', 'Wilfred Francis', 'wilfred13', 'CTQa593E', 'wilfred1336F@ui.ac.id', 'wilfred13gx1@gmail.com', '0817907418969', '021586509');
INSERT INTO MAHASISWA VALUES ('1044299113', 'Mario Tyler', 'mario27', 'ugQiPAU0', 'mario27hot@ui.ac.id', 'mario27gsJ@gmail.com', '0806583707072', '021591523');
INSERT INTO MAHASISWA VALUES ('1051096520', 'Lois Payne', 'lois16', 'w1SVg0AqsnGeiKhWhHi', 'lois16NNW@ui.ac.id', 'lois16ghO@gmail.com', '0862571396738', '021852986');
INSERT INTO MAHASISWA VALUES ('1040643041', 'Yolanda Valdez', 'yolanda45', 'r4Pz9gZR', 'yolanda45dIs@ui.ac.id', 'yolanda45YM5@gmail.com', '0841535488581', '021626400');
INSERT INTO MAHASISWA VALUES ('1083064172', 'Lorene Marsh', 'lorene44', 'NFwbJ1fO', 'lorene44C2A@ui.ac.id', 'lorene44SxC@gmail.com', '0878220408146', '021424382');
INSERT INTO MAHASISWA VALUES ('1000772392', 'Cristina Obrien', 'cristina27', 'dXaDFXpnxtN6xazD2a', 'cristina27Wxt@ui.ac.id', 'cristina279b3@gmail.com', '0839282734784', '021285376');
INSERT INTO MAHASISWA VALUES ('1027004892', 'Jimmie Gilbert', 'jimmie22', 'K8PDAckbAU873tIF', 'jimmie22k5E@ui.ac.id', 'jimmie22nYG@gmail.com', '0814054025035', '021277857');
INSERT INTO MAHASISWA VALUES ('1077829852', 'Angel Cox', 'angel84', 'SKmLm6quh3rctd', 'angel84nuI@ui.ac.id', 'angel84D2U@gmail.com', '0806107519666', '021188751');
INSERT INTO MAHASISWA VALUES ('1029246577', 'Thomas Lewis', 'thomas92', 'Zs9zxN0fxF8RjA4dfOZb', 'thomas92eHi@ui.ac.id', 'thomas92zRT@gmail.com', '0822287928521', '021685859');
INSERT INTO MAHASISWA VALUES ('1080469447', 'Irma Lloyd', 'irma73', 'nPzeeul6yzwv', 'irma73lZi@ui.ac.id', 'irma73CkH@gmail.com', '0851325034483', '021787692');
INSERT INTO MAHASISWA VALUES ('1063041490', 'Geraldine Robertson', 'geraldine81', '7fNTdVoyVGz8VGG3ba9v', 'geraldine813vl@ui.ac.id', 'geraldine81V39@gmail.com', '0837107716952', '021206703');
INSERT INTO MAHASISWA VALUES ('1082153772', 'Oscar Gardner', 'oscar57', 'eTNrVxieftG', 'oscar57ECo@ui.ac.id', 'oscar57GcO@gmail.com', '0816875740275', '021962282');
INSERT INTO MAHASISWA VALUES ('1017913215', 'Sherry Reynolds', 'sherry76', 'KJKcAeHowKrAeugDAZcx', 'sherry76fpM@ui.ac.id', 'sherry766T9@gmail.com', '0804181850568', '021737093');
INSERT INTO MAHASISWA VALUES ('1027083378', 'Phyllis Lyons', 'phyllis19', 'iMKSkzwE', 'phyllis19cGL@ui.ac.id', 'phyllis194Je@gmail.com', '0883793639868', '021784325');
INSERT INTO MAHASISWA VALUES ('1013746596', 'Willis Peterson', 'willis52', 'yiFVHQ6urA', 'willis52XGd@ui.ac.id', 'willis52iDH@gmail.com', '0860243052221', '021834683');
INSERT INTO MAHASISWA VALUES ('1049532348', 'Erika Beck', 'erika60', 'LvsBjBHiBcu7', 'erika60GSj@ui.ac.id', 'erika60Feh@gmail.com', '0866864607450', '021948082');
INSERT INTO MAHASISWA VALUES ('1040998167', 'Raul Burke', 'raul98', 'ORTiVUgdeZhr5fm', 'raul98Q0p@ui.ac.id', 'raul98nfd@gmail.com', '0833457943163', '021667972');
INSERT INTO MAHASISWA VALUES ('1072157384', 'Roderick Boone', 'roderick21', '1UiYQoyriT', 'roderick21Vr4@ui.ac.id', 'roderick2180v@gmail.com', '0894883088959', '021966672');
INSERT INTO MAHASISWA VALUES ('1065990475', 'Carol Hodges', 'carol29', 'hcTp8DUiDj6LR0Ys07', 'carol29gXj@ui.ac.id', 'carol29FDs@gmail.com', '0870613520154', '021702169');
INSERT INTO MAHASISWA VALUES ('1052854208', 'Billie Lopez', 'billie77', 'y3fdC7XrECJlwk', 'billie77Vps@ui.ac.id', 'billie77Ow8@gmail.com', '0816054541650', '021505840');
INSERT INTO MAHASISWA VALUES ('1053273200', 'Michele Kelley', 'michele70', 'URDQecDwZr0x3eSylIhP', 'michele70Rj9@ui.ac.id', 'michele70KXS@gmail.com', '0816987507321', '021170580');
INSERT INTO MAHASISWA VALUES ('1084044178', 'Shawn Garza', 'shawn40', 'Q12EpSdRd', 'shawn401If@ui.ac.id', 'shawn40Hjd@gmail.com', '0803406148473', '021504530');
INSERT INTO MAHASISWA VALUES ('1090104294', 'Ron Porter', 'ron04', 'bQSzHX7UZ', 'ron04sPj@ui.ac.id', 'ron048u5@gmail.com', '0833846309916', '021643545');
INSERT INTO MAHASISWA VALUES ('1053785089', 'Wesley Phillips', 'wesley99', 'z5261RR6xIxe026', 'wesley99YKy@ui.ac.id', 'wesley99y0z@gmail.com', '0871824293894', '021464450');
INSERT INTO MAHASISWA VALUES ('1079148888', 'Joseph Vega', 'joseph67', 'EIZi0cjre2GMD', 'joseph67jNQ@ui.ac.id', 'joseph678ip@gmail.com', '0876110088702', '021101000');
INSERT INTO MAHASISWA VALUES ('1092038264', 'Marlene Hogan', 'marlene67', 'JRoTex3uVeomv0gF', 'marlene67q0k@ui.ac.id', 'marlene67QcO@gmail.com', '0849026864231', '021836633');
INSERT INTO MAHASISWA VALUES ('1023323563', 'Cynthia Luna', 'cynthia47', 'YP5L1iGYQJ1bBN0VOlaf', 'cynthia47Mtg@ui.ac.id', 'cynthia47SQU@gmail.com', '0886197425607', '021921770');
INSERT INTO MAHASISWA VALUES ('1059724148', 'Lindsay Tucker', 'lindsay22', 'MlJGQslzAEZ8j0jtyGg', 'lindsay22U8u@ui.ac.id', 'lindsay22Jd6@gmail.com', '0841267339695', '021491475');
INSERT INTO MAHASISWA VALUES ('1040045860', 'Lula Burns', 'lula09', 'f0MzC3PbP5Gi5', 'lula09Op1@ui.ac.id', 'lula09KLk@gmail.com', '0869610849350', '021311643');
INSERT INTO MAHASISWA VALUES ('1091972983', 'Lorena Carter', 'lorena79', 'upipnpwZ6Rfs9W', 'lorena79TbU@ui.ac.id', 'lorena79VUh@gmail.com', '0851193243362', '021277768');
INSERT INTO MAHASISWA VALUES ('1024744438', 'Ramon Cooper', 'ramon75', 'iTZTadu4i2dgjJccsd', 'ramon75Drk@ui.ac.id', 'ramon750Qu@gmail.com', '0819411896652', '021386255');
INSERT INTO MAHASISWA VALUES ('1085922635', 'Alice Jenkins', 'alice95', 'kaqbrvwgOoaK', 'alice950qk@ui.ac.id', 'alice95tel@gmail.com', '0875454425126', '021641265');
INSERT INTO MAHASISWA VALUES ('1070638832', 'Victor Bennett', 'victor30', 'UdVANxNUx', 'victor30ucq@ui.ac.id', 'victor30YMz@gmail.com', '0880622436685', '021047892');
INSERT INTO MAHASISWA VALUES ('1048547979', 'Jackie Lawrence', 'jackie56', 'GSUZaJzTCC4WB1v', 'jackie56d9K@ui.ac.id', 'jackie56Xi2@gmail.com', '0840903120615', '021594337');
INSERT INTO MAHASISWA VALUES ('1061475015', 'Angelica Morris', 'angelica94', '7FQpS97v5qlp', 'angelica94t21@ui.ac.id', 'angelica94zn1@gmail.com', '0890573023628', '021065410');
INSERT INTO MAHASISWA VALUES ('1011563721', 'Pamela Warner', 'pamela21', 'ZSwbckIK', 'pamela213IL@ui.ac.id', 'pamela219hH@gmail.com', '0865837387338', '021254821');
INSERT INTO MAHASISWA VALUES ('1030427651', 'Steve Bush', 'steve99', 'tGMgOXJS3YVfwTc4', 'steve99Q6F@ui.ac.id', 'steve99ZXl@gmail.com', '0840981268133', '021357504');
INSERT INTO MAHASISWA VALUES ('1010519774', 'Floyd Wilkins', 'floyd90', 'ljvDYg44qkt145KmrH', 'floyd900cU@ui.ac.id', 'floyd90dg6@gmail.com', '0845590946953', '021547797');
INSERT INTO MAHASISWA VALUES ('1088306086', 'Alexandra Hopkins', 'alexandra35', 'WXEgdDYH', 'alexandra35zp1@ui.ac.id', 'alexandra35hms@gmail.com', '0899551731759', '021129266');
INSERT INTO MAHASISWA VALUES ('1049112069', 'May Ruiz', 'may75', 'NzfQdp02ET7FTrbtt4Q', 'may75XhV@ui.ac.id', 'may75IRy@gmail.com', '0841045202134', '021756026');
INSERT INTO MAHASISWA VALUES ('1070735889', 'Mabel Bailey', 'mabel88', 'dxaTmrdf0SEDEc0', 'mabel88Aeq@ui.ac.id', 'mabel88LmQ@gmail.com', '0894970557407', '021147940');
INSERT INTO MAHASISWA VALUES ('1046603772', 'Rachael Caldwell', 'rachael11', 'JbCcRQ3C', 'rachael11pIZ@ui.ac.id', 'rachael11ZwZ@gmail.com', '0848334573715', '021747678');
INSERT INTO MAHASISWA VALUES ('1043204850', 'Leonard Lambert', 'leonard57', '1REOKGMUKp', 'leonard57AWB@ui.ac.id', 'leonard57n8L@gmail.com', '0895504220234', '021731006');
INSERT INTO MAHASISWA VALUES ('1079770745', 'Kurt Santiago', 'kurt46', 'FMxtCryVC90KwXeKRn', 'kurt46GM4@ui.ac.id', 'kurt46TP8@gmail.com', '0810262257146', '021514479');
INSERT INTO MAHASISWA VALUES ('1018416428', 'Vera Jackson', 'vera84', 'm3FChdoAI2pc5vOrJQBi', 'vera84knW@ui.ac.id', 'vera84OYf@gmail.com', '0802487876843', '021496042');
INSERT INTO MAHASISWA VALUES ('1010220843', 'Sarah Roberson', 'sarah24', 'nNjbqLH2W0p', 'sarah24a1j@ui.ac.id', 'sarah24GSc@gmail.com', '0846380995266', '021787628');
INSERT INTO MAHASISWA VALUES ('1037265968', 'Kelley Bates', 'kelley94', 'Ge1oaRgqEsRge6', 'kelley94avn@ui.ac.id', 'kelley94b3W@gmail.com', '0848601597565', '021945249');
INSERT INTO MAHASISWA VALUES ('1028811620', 'Dolores Kelly', 'dolores47', '5nvsQMd1h1OaLu3c', 'dolores47152@ui.ac.id', 'dolores47syo@gmail.com', '0874675011523', '021355422');
INSERT INTO MAHASISWA VALUES ('1048084636', 'Leticia Dennis', 'leticia09', 'gQxqZYaQgM98', 'leticia09Lkf@ui.ac.id', 'leticia09WzM@gmail.com', '0831961044677', '021360837');
INSERT INTO MAHASISWA VALUES ('1075153227', 'Bill Sparks', 'bill86', 'MoBRm05EPGTj', 'bill86XBe@ui.ac.id', 'bill86TXB@gmail.com', '0843858734656', '021015104');
INSERT INTO MAHASISWA VALUES ('1042054906', 'Kelly Wilson', 'kelly03', 'xQUnox7CkBc', 'kelly03s20@ui.ac.id', 'kelly038u6@gmail.com', '0883245392195', '021214891');
INSERT INTO MAHASISWA VALUES ('1076832598', 'Patti Woods', 'patti66', 'FDftbwOIoKagUSlxUA', 'patti66XEg@ui.ac.id', 'patti66wrm@gmail.com', '0860644482455', '021371719');
INSERT INTO MAHASISWA VALUES ('1066815199', 'Gabriel Baldwin', 'gabriel53', 'IKV19DdHdCFjwyDR62xi', 'gabriel53WVu@ui.ac.id', 'gabriel53WU5@gmail.com', '0867040435813', '021792048');
INSERT INTO MAHASISWA VALUES ('1056370728', 'Mack Mcbride', 'mack98', '9NO599bpdTRTji8tJTh', 'mack98TjU@ui.ac.id', 'mack98BV2@gmail.com', '0855928361994', '021289087');
INSERT INTO MAHASISWA VALUES ('1048652655', 'Jeremy Garrett', 'jeremy20', 'AZ8xYdQfX6x', 'jeremy20nCg@ui.ac.id', 'jeremy205i1@gmail.com', '0840020268251', '021912310');
INSERT INTO MAHASISWA VALUES ('1026407289', 'Jeremiah Richards', 'jeremiah53', 'S7SkexMfd69k6IJeK', 'jeremiah53rJu@ui.ac.id', 'jeremiah53bsC@gmail.com', '0846206646800', '021900906');
INSERT INTO MAHASISWA VALUES ('1016295983', 'Jennifer Thomas', 'jennifer42', 'wH65f67NgntSSY1bMZ', 'jennifer42LJA@ui.ac.id', 'jennifer42heu@gmail.com', '0875573510741', '021392199');
INSERT INTO MAHASISWA VALUES ('1043257528', 'Lawrence Barber', 'lawrence20', 'W9lpjDmoXHHgTCZ', 'lawrence20T8F@ui.ac.id', 'lawrence20C9g@gmail.com', '0875889227169', '021033284');
INSERT INTO MAHASISWA VALUES ('1077041018', 'Stephen Ramirez', 'stephen79', 'jL7toxjgaVc1Po', 'stephen790DW@ui.ac.id', 'stephen79xac@gmail.com', '0858875442376', '021562806');
INSERT INTO MAHASISWA VALUES ('1022935503', 'Leo Collins', 'leo50', 'WRWJ3IKI', 'leo50c5e@ui.ac.id', 'leo50TGx@gmail.com', '0806127491765', '021556167');
INSERT INTO MAHASISWA VALUES ('1068380616', 'Calvin Weber', 'calvin92', '5TNS91kf6VtFuiq', 'calvin92sCI@ui.ac.id', 'calvin92n0X@gmail.com', '0805524696995', '021034489');
INSERT INTO MAHASISWA VALUES ('1004340857', 'Fredrick Greer', 'fredrick18', 'lcYpmCgjD5', 'fredrick18lre@ui.ac.id', 'fredrick18CBS@gmail.com', '0881512891372', '021043773');
INSERT INTO MAHASISWA VALUES ('1009186385', 'Sam Townsend', 'sam40', 'TGR4bMXZ0bb2m', 'sam40F3k@ui.ac.id', 'sam405ZJ@gmail.com', '0861621943961', '021963394');
INSERT INTO MAHASISWA VALUES ('1034403194', 'Daniel Howard', 'daniel74', 'pm5FnajE', 'daniel74h63@ui.ac.id', 'daniel74PNF@gmail.com', '0888382989812', '021709550');
INSERT INTO MAHASISWA VALUES ('1042456970', 'Bruce Torres', 'bruce82', 'tJYoIGWkzytLg259p', 'bruce823Oc@ui.ac.id', 'bruce82tLL@gmail.com', '0826652253626', '021497210');
INSERT INTO MAHASISWA VALUES ('1081595558', 'Angie Douglas', 'angie25', '9i242lrt5aZ', 'angie250jo@ui.ac.id', 'angie25nuu@gmail.com', '0850630583722', '021167926');
INSERT INTO MAHASISWA VALUES ('1028431002', 'Rebecca Parker', 'rebecca38', 'E2grZAGh7WonNYwhpJwf', 'rebecca38Eud@ui.ac.id', 'rebecca38Gkj@gmail.com', '0899702973539', '021522151');
INSERT INTO MAHASISWA VALUES ('1055131207', 'Ellen Sandoval', 'ellen70', 'gGXxM67KD3iFVJlB3A', 'ellen70CjP@ui.ac.id', 'ellen70SHv@gmail.com', '0839028845412', '021278602');
INSERT INTO MAHASISWA VALUES ('1073799959', 'Edward Osborne', 'edward30', '44JzclLbzhYvnIsm', 'edward30ySB@ui.ac.id', 'edward30rB9@gmail.com', '0863230213308', '021200715');
INSERT INTO MAHASISWA VALUES ('1045002529', 'Glen Moss', 'glen11', 'LY6UX0A10SgBEkN1Q', 'glen118Ff@ui.ac.id', 'glen11bLE@gmail.com', '0813282659806', '021441471');
INSERT INTO MAHASISWA VALUES ('1026359328', 'Guy Walters', 'guy28', 'PYk3upuqFEz0Hhy', 'guy28iei@ui.ac.id', 'guy28rf6@gmail.com', '0829906831494', '021715498');
INSERT INTO MAHASISWA VALUES ('1073349228', 'Edith Gibbs', 'edith48', 'CUMvBFLG52hz', 'edith48jw1@ui.ac.id', 'edith48hSZ@gmail.com', '0876852494169', '021000572');
INSERT INTO MAHASISWA VALUES ('1025678468', 'Greg Hicks', 'greg96', 'MBWPHuz6', 'greg96hz5@ui.ac.id', 'greg96NCu@gmail.com', '0853649761799', '021045876');
INSERT INTO MAHASISWA VALUES ('1029939178', 'Jason Fleming', 'jason29', '5EqCdS7fLt', 'jason29hJk@ui.ac.id', 'jason29sZv@gmail.com', '0864048152201', '021674449');
INSERT INTO MAHASISWA VALUES ('1078547522', 'Albert Singleton', 'albert47', 'smP0POWS', 'albert47ncP@ui.ac.id', 'albert47N5q@gmail.com', '0854473037486', '021963121');
INSERT INTO MAHASISWA VALUES ('1054493546', 'Daryl Chapman', 'daryl29', 'Q6OGsIhnCkkxLgIoCOJi', 'daryl29gTo@ui.ac.id', 'daryl29FuG@gmail.com', '0817073371688', '021140541');
INSERT INTO MAHASISWA VALUES ('1082588030', 'Louis Warren', 'louis27', 'ZeVS2xAU2CfDE', 'louis27r1S@ui.ac.id', 'louis27I3s@gmail.com', '0874287956072', '021770155');
INSERT INTO MAHASISWA VALUES ('1003292149', 'Erik Joseph', 'erik63', '2pBg6w6XZapZdOgAsh', 'erik63PT1@ui.ac.id', 'erik63X4Q@gmail.com', '0836576207733', '021776596');

-- END MAHASISWA DATA --

-- DOSEN DATA --

INSERT INTO DOSEN VALUES (101111111,'Cruz Tabor','cruz11','sadKNO','cruz.tabor@cs.ui.ac.id','Universitas Indonesia');
INSERT INTO DOSEN VALUES (101111112,'Jewell Heffernan','jewell11','c234c2342x3','jewell.heffernan@cs.ui.ac.id','Institut Teknologi Bandung');
INSERT INTO DOSEN VALUES (101111113,'Ernesto Carlson','ernesto11','4x234zsqdw','ernesto.carlson@cs.ui.ac.id','Universitas Gadjah Mada');
INSERT INTO DOSEN VALUES (101111114,'Reid Urrutia','reid11','2x34s32d23','reid.urrutia@cs.ui.ac.id','Universitas Indonesia');
INSERT INTO DOSEN VALUES (101111115,'Abraham Shirk','abraham11','232LKJOsd','abraham.shirk@cs.ui.ac.id','Institut Teknologi Bandung');
INSERT INTO DOSEN VALUES (101111116,'Herschel Cosey','herschel11','exqw32984zj','herschel.cosey@cs.ui.ac.id','Universitas Gadjah Mada');
INSERT INTO DOSEN VALUES (101111117,'Marcel Waugh','marcel11','EOX*@ulqwk','marcel.waugh@cs.ui.ac.id','Universitas Indonesia');
INSERT INTO DOSEN VALUES (101111118,'Sherman Freund','sherman11','ADSKu32oh32','sherman.freund@cs.ui.ac.id','Institut Teknologi Bandung');
INSERT INTO DOSEN VALUES (101111119,'Britt Wilcoxon','britt11','ASKd832or','britt.wilcoxon@cs.ui.ac.id','Universitas Gadjah Mada');
INSERT INTO DOSEN VALUES (101111120,'Lucien Dafoe','lucien11','LDlw48jl','lucien.dafoe@cs.ui.ac.id','Universitas Indonesia');
INSERT INTO DOSEN VALUES (101111121,'Abel Hollmann','abel11','LKJDSAJioer','abel.hollmann@cs.ui.ac.id','Institut Teknologi Bandung');
INSERT INTO DOSEN VALUES (101111122,'Angelo Tawney','angelo11','LAKSuow','angelo.tawney@cs.ui.ac.id','Universitas Gadjah Mada');
INSERT INTO DOSEN VALUES (101111123,'Mack Flakes','mack11','LKU9ow4','mack.flakes@cs.ui.ac.id','Universitas Indonesia');
INSERT INTO DOSEN VALUES (101111124,'Napoleon Fricke','napoleon11','ldu94o3i','napoleon.fricke@cs.ui.ac.id','Institut Teknologi Bandung');
INSERT INTO DOSEN VALUES (101111125,'Willie Woodburn','willie11','lwe9L(4llkj','willie.woodburn@cs.ui.ac.id','Universitas Gadjah Mada');
INSERT INTO DOSEN VALUES (101111126,'Oren Troia','oren11','ewuhlsdkfjk','oren.troia@cs.ui.ac.id','Universitas Indonesia');
INSERT INTO DOSEN VALUES (101111127,'Lloyd Steptoe','lloyd11','lewkj932k32','lloyd.steptoe@cs.ui.ac.id','Institut Teknologi Bandung');
INSERT INTO DOSEN VALUES (101111128,'Brett Hymas','brett11','lfdsijflwkj44','brett.hymas@cs.ui.ac.id','Universitas Gadjah Mada');
INSERT INTO DOSEN VALUES (101111129,'Hilario Basler','hilario11','lwekrj39kjer','hilario.basler@cs.ui.ac.id','Universitas Indonesia');
INSERT INTO DOSEN VALUES (101111130,'Loyd Mone','loyd11','werlkjewjljs','loyd.mone@cs.ui.ac.id','Institut Teknologi Bandung');

INSERT INTO DOSEN VALUES ('9642732632', 'Belva Foote', 'belva18', 'K8lPDcdac', 'belva18V4d@cs.ui.ac.id','Institut Teknologi Sepuluh November');
INSERT INTO DOSEN VALUES ('8655528560', 'Sharee Emmons', 'sharee57', 'KfjWXdw2lFzNXO', 'sharee57lvu@cs.ui.ac.id','Universitas Pancasila');
INSERT INTO DOSEN VALUES ('7005155735', 'Kati Couture', 'kati25', 'N4Wx87G87UzOsv', 'kati25HvH@cs.ui.ac.id','Institut Teknologi Sepuluh November');
INSERT INTO DOSEN VALUES ('1674449723', 'Leanora Oneal', 'leanora64', 'yJRx49FgxgLFKK7fBp4v', 'leanora64O0T@cs.ui.ac.id','Universitas Terbuka');
INSERT INTO DOSEN VALUES ('2220206656', 'Dayle Amaya', 'dayle06', 'kKrWBRHm0GZqLidC7X', 'dayle063Fp@cs.ui.ac.id','Universitas Terbuka');
INSERT INTO DOSEN VALUES ('1839964410', 'Kanesha Redding', 'kanesha76', 'LoqO4GpW1SgM6nWefxbE', 'kanesha76u5e@cs.ui.ac.id','Universitas Gunadarma');
INSERT INTO DOSEN VALUES ('5208841006', 'Danica Sallee', 'danica43', 'Co0hPYhzeg', 'danica43zj0@cs.ui.ac.id','Institut Teknologi Sepuluh November');
INSERT INTO DOSEN VALUES ('8257919474', 'Angelia Tidwell', 'angelia73', '9zXFLwMWQyM', 'angelia73Fnl@cs.ui.ac.id','Institut Teknologi Bandung');
INSERT INTO DOSEN VALUES ('7619772623', 'Renita Kolb', 'renita42', 'CtCYcjAAwuV', 'renita42JXZ@cs.ui.ac.id','Universitas Terbuka');
INSERT INTO DOSEN VALUES ('6262715718', 'Madelaine Pettis', 'madelaine15', 'x9vJGmWoBP', 'madelaine15LNY@cs.ui.ac.id','Institut Teknologi Bandung');
INSERT INTO DOSEN VALUES ('8932025337', 'Giselle Rounds', 'giselle22', 'PJ9NTI6EvrovFh', 'giselle22OpC@cs.ui.ac.id','Institut Teknologi Sepuluh November');
INSERT INTO DOSEN VALUES ('3068327584', 'Jordon Worth', 'jordon93', 'vJTc3jo6CBNB', 'jordon934CP@cs.ui.ac.id','Universitas Padjajaran');
INSERT INTO DOSEN VALUES ('3357936045', 'Juliana Bowlin', 'juliana14', 'MWchkfdDzcG', 'juliana14xr8@cs.ui.ac.id','Institut Teknologi Bandung');
INSERT INTO DOSEN VALUES ('4840774438', 'Catharine Hargrove', 'catharine87', 'gpozNd4oWGFo5T', 'catharine87uPi@cs.ui.ac.id','Institut Teknologi Bandung');
INSERT INTO DOSEN VALUES ('0904434284', 'Daine Mcvey', 'daine00', '5dhislkxN4AqYyhlX3v', 'daine00f60@cs.ui.ac.id','Universitas Terbuka');
INSERT INTO DOSEN VALUES ('3074378873', 'Shelba Dorsey', 'shelba48', 'aInEgEDjf3v12c', 'shelba48zGp@cs.ui.ac.id','Universitas Gadjah Mada');
INSERT INTO DOSEN VALUES ('5691371135', 'Elfrieda Close', 'elfrieda23', 'D2GoTEDHqiHyoT', 'elfrieda23JvB@cs.ui.ac.id','Universitas Indonesia');
INSERT INTO DOSEN VALUES ('9264771375', 'Hermila Leary', 'hermila95', 'WNTEmAqsa5uyGcK', 'hermila9528Y@cs.ui.ac.id','Univeritas Negeri Jakarta');
INSERT INTO DOSEN VALUES ('9492643724', 'Ronda Pringle', 'ronda62', 'LyTspWQ9', 'ronda62ma5@cs.ui.ac.id','Universitas Gadjah Mada');
INSERT INTO DOSEN VALUES ('6824071607', 'Shala Cowart', 'shala35', 'O9hMBtnrQrkyMAZQ', 'shala35Ugj@cs.ui.ac.id','Institut Teknologi Bandung');
INSERT INTO DOSEN VALUES ('2477047001', 'Martine Khan', 'martine37', 'vXVjj2p7Ut', 'martine373pr@cs.ui.ac.id','Universitas Indonesia');
INSERT INTO DOSEN VALUES ('5937082790', 'Felicita Crouse', 'felicita57', 'gRlJiN8wOQxNT7hwY', 'felicita57ek1@cs.ui.ac.id','Universitas Gunadarma');
INSERT INTO DOSEN VALUES ('6950685663', 'Laurinda Harley', 'laurinda60', 'oPN3q52C1QgOsxjH', 'laurinda60KmF@cs.ui.ac.id','Universitas Indonesia');
INSERT INTO DOSEN VALUES ('6963282590', 'Tajuana Merritt', 'tajuana43', 'Ix131MD37W4bw1G7', 'tajuana435JL@cs.ui.ac.id','Institut Teknologi Bandung');
INSERT INTO DOSEN VALUES ('1077920761', 'Aron Kelso', 'aron29', 'Bu8MnaD7qLIdndhI7B', 'aron29aka@cs.ui.ac.id','Universitas Terbuka');
INSERT INTO DOSEN VALUES ('7977608466', 'Vonda Bui', 'vonda30', 'uMoAaOl98gxi4NB7n', 'vonda30FVI@cs.ui.ac.id','Universitas Gunadarma');
INSERT INTO DOSEN VALUES ('9819895535', 'Marna Eddy', 'marna69', 'O3boXM2ksYkEUgmeq2a0', 'marna69DWt@cs.ui.ac.id','Universitas Pancasila');
INSERT INTO DOSEN VALUES ('0750059322', 'Antony Hutchinson', 'antony74', '8s3yglyc0C5ccmH', 'antony7437o@cs.ui.ac.id','Institut Pertanian Bogor');
INSERT INTO DOSEN VALUES ('8578731551', 'Dean Engel', 'dean51', 'GIPX0vbFXnPrSbd', 'dean51xRJ@cs.ui.ac.id','Universitas Padjajaran');
INSERT INTO DOSEN VALUES ('5458184775', 'Daniela Spivey', 'daniela93', 'ynouEZ1IijnvXQkYT', 'daniela931QQ@cs.ui.ac.id','Univeritas Negeri Jakarta');

-- END DOSEN DATA --

-- TERM DATA --

INSERT INTO TERM VALUES (2015,2);
INSERT INTO TERM VALUES (2016,1);
INSERT INTO TERM VALUES (2016,2);

-- END TERM DATA --

-- PRODI DATA --

INSERT INTO PRODI VALUES (1510,'S1 Ilmu Komputer');
INSERT INTO PRODI VALUES (1511,'S1 Sistem Informasi');
INSERT INTO PRODI VALUES (1512,'S1 Ilkom Kelas Internasional');
INSERT INTO PRODI VALUES (1513,'S2 MIK');
INSERT INTO PRODI VALUES (1514,'S2 MTI');
INSERT INTO PRODI VALUES (1515,'S3 DIK');

-- END PRODI DATA --

-- JENISMKS DATA --

INSERT INTO JENISMKS VALUES (1,'Skripsi');
INSERT INTO JENISMKS VALUES (2, 'Karya Akhir');
INSERT INTO JENISMKS VALUES (3, 'Proposal Tesis');
INSERT INTO JENISMKS VALUES (4, 'Usulan Penelitian');
INSERT INTO JENISMKS VALUES (5, 'Seminar Hasil Penelitian S3');
INSERT INTO JENISMKS VALUES (6, 'Pra Promosi');
INSERT INTO JENISMKS VALUES (7, 'Promosi');
INSERT INTO JENISMKS VALUES (8, 'Tesis');
INSERT INTO JENISMKS VALUES (9, 'Lain-lain');

-- END JENISMKS DATA --

-- MKS DATA --
INSERT INTO MATA_KULIAH_SPESIAL VALUES (1, '1506724505',2015,2, 'Nullam porttitor lacus at turpis', FALSE, FALSE, FALSE, 1);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (2, '1506688752',2016,2, 'Maecenas tristique, est et tempus semper', FALSE, FALSE, FALSE, 3);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (3, '1506688834',2016,2, 'Sed sagittis. Nam congue, risus semper porta volutpat', FALSE, FALSE, FALSE, 4);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (4, '1506688765',2016,2, 'Morbi porttitor lorem id ligula.', FALSE, FALSE, FALSE, 2);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (5, '1506688872',2016,2, 'Suspendisse ornare consequat lectus.', FALSE, FALSE, FALSE, 6);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (6, '1506688986',2016,2, 'Est risus, auctor sed, tristique in, tempus sit amet, sem.', FALSE, FALSE, FALSE, 7);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (7, '1506689194',2016,2, 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', FALSE, FALSE, FALSE, 2);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (8, '1506689345',2016,2, 'Duis aliquam convallis nunc.', FALSE, FALSE, FALSE, 6);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (9, '1506689396',2016,2, 'Proin at turpis a pede posuere nonummy.', FALSE, FALSE, FALSE, 7);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (10, '1506689452',2016,2, 'Aenean fermentum. Donec ut mauris eget massa tempor convallis.', FALSE, FALSE, FALSE, 3);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (11, '1506689490',2016,2, 'Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', FALSE, FALSE, FALSE, 4);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (12, '1506689585',2016,2, 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla.', FALSE, FALSE, FALSE, 5);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (13, '1506689641',2016,2, 'Sed accumsan felis.', FALSE, FALSE, FALSE, 1);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (14, '1506690391',2016,2, 'Ut at dolor quis odio consequat varius.', FALSE, FALSE, FALSE, 5);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (15, '1506690460',2016,2, 'Morbi non lectus.', FALSE, FALSE, FALSE, 2);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (16, '1506721794',2016,2, 'Nulla ut erat id mauris vulputate elementum.', FALSE, FALSE, FALSE, 1);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (17, '1506722746',2016,2, 'Phasellus in felis. Donec semper sapien a libero.', FALSE, FALSE, FALSE, 5);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (18, '1506722752',2016,2, 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim.', FALSE, FALSE, FALSE, 5);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (19, '1506722765',2016,2, 'Nullam porttitor lacus at turpis.', FALSE, FALSE, FALSE, 4);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (20, '1506722771',2016,2, 'Donec posuere metus vitae ipsum.', FALSE, FALSE, FALSE, 2);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (21, '1506722821',2016,2, 'Integer ac leo.', FALSE, FALSE, FALSE, 2);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (22, '1506722840',2016,2, 'In congue. Etiam justo. Etiam pretium iaculis justo.', FALSE, FALSE, FALSE, 6);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (23, '1506722853',2016,2, 'In hac habitasse platea dictumst.', FALSE, FALSE, FALSE, 6);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (24, '1506722866',2016,2, 'Morbi vestibulum, velit id pretium iaculis', FALSE, FALSE, FALSE, 6);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (25, '1506722872',2016,2, 'Nullam porttitor lacus at turpis.', FALSE, FALSE, FALSE, 5);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (26, '1506723572',2016,2, 'Etiam vel augue.', FALSE, FALSE, FALSE, 7);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (27, '1506724354',2016,2, 'Duis consequat dui nec nisi volutpat eleifend.', FALSE, FALSE, FALSE, 5);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (28, '1506724606',2016,2, 'Phasellus in felis.', FALSE, FALSE, FALSE, 3);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (29, '1506725003',2016,2, 'Cras in purus eu magna vulputate luctus.', FALSE, FALSE, FALSE, 3);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (30, '1506725041',2016,2, 'Turpis enim blandit mi, in porttitor pede justo eu massa.', FALSE, FALSE, FALSE, 4);

INSERT INTO MATA_KULIAH_SPESIAL VALUES (31, '1084659908',2016,2, 'nec condimentum neque sapien placerat ante nulla justo aliquam quis turpis eget elit', TRUE, FALSE, TRUE, 5);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (32, '1056370728',2016,1, 'magna vulputate luctus cum sociis natoque penatibus et magnis dis', FALSE, TRUE, FALSE, 2);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (33, '1090104294',2015,2, 'elit proin interdum mauris non ligula pellentesque ultrices phasellus id sapien in sapien', FALSE, FALSE, TRUE, 6);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (34, '1053273200',2015,2, 'adipiscing molestie hendrerit at vulputate vitae nisl aenean lectus pellentesque eget nunc donec quis', FALSE, FALSE, TRUE, 2);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (35, '1048084636',2015,2, 'fusce consequat nulla nisl nunc nisl duis bibendum', FALSE, FALSE, TRUE, 3);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (36, '1037265968',2015,2, 'mattis pulvinar nulla pede ullamcorper augue a suscipit nulla elit ac', TRUE, TRUE, TRUE, 5);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (37, '1077829852',2015,2, 'in blandit ultrices enim lorem ipsum dolor sit amet consectetuer adipiscing elit proin interdum', TRUE, TRUE, FALSE, 1);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (38, '1003292149',2016,2, 'lacus curabitur at ipsum ac tellus semper interdum mauris ullamcorper', TRUE, FALSE, FALSE, 2);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (39, '1085922635',2016,1, 'justo nec condimentum neque sapien placerat ante nulla justo aliquam quis', TRUE, TRUE, TRUE, 9);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (40, '1083064172',2016,1, 'gravida nisi at nibh in', TRUE, FALSE, FALSE, 4);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (41, '1081595558',2016,1, 'sed magna at nunc commodo placerat praesent blandit nam nulla integer pede justo lacinia eget', TRUE, TRUE, FALSE, 2);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (42, '1028431002',2015,2, 'vitae nisl aenean lectus pellentesque', TRUE, FALSE, FALSE, 4);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (43, '1075153227',2015,2, 'et magnis dis parturient montes nascetur ridiculus', TRUE, TRUE, FALSE, 8);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (44, '1082588030',2016,1, 'amet eleifend pede libero quis orci nullam molestie nibh in', FALSE, TRUE, TRUE, 5);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (45, '1055660493',2016,2, 'ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae donec pharetra magna', TRUE, FALSE, FALSE, 3);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (46, '1042054906',2015,2, 'in lectus pellentesque at nulla suspendisse potenti', FALSE, FALSE, TRUE, 2);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (47, '1004340857',2015,2, 'felis sed interdum venenatis turpis enim blandit mi in', TRUE, TRUE, FALSE, 7);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (48, '1080469447',2015,2, 'non mauris morbi non lectus aliquam sit amet diam in magna bibendum', FALSE, FALSE, FALSE, 9);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (49, '1073799959',2015,2, 'ipsum dolor sit amet consectetuer adipiscing elit proin interdum mauris non ligula pellentesque ultrices', FALSE, TRUE, TRUE, 7);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (50, '1025909797',2015,2, 'suspendisse potenti cras in purus eu magna vulputate luctus cum sociis natoque penatibus et', TRUE, TRUE, FALSE, 1);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (51, '1073349228',2016,2, 'in est risus auctor sed tristique in', FALSE, TRUE, FALSE, 5);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (52, '1074653489',2015,2, 'mauris sit amet eros suspendisse accumsan', FALSE, FALSE, FALSE, 1);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (53, '1055423676',2015,2, 'morbi vestibulum velit id pretium iaculis diam erat fermentum justo nec condimentum', TRUE, TRUE, TRUE, 7);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (54, '1070735889',2016,2, 'penatibus et magnis dis parturient', FALSE, FALSE, TRUE, 1);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (55, '1040643041',2015,2, 'libero nullam sit amet turpis elementum ligula vehicula consequat morbi', TRUE, FALSE, TRUE, 7);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (56, '1029939178',2016,1, 'sem praesent id massa id', TRUE, TRUE, TRUE, 9);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (57, '1026407289',2015,2, 'vel enim sit amet nunc viverra dapibus nulla suscipit ligula in lacus curabitur at ipsum', FALSE, TRUE, TRUE, 7);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (58, '1049532348',2016,1, 'turpis a pede posuere nonummy integer non velit donec diam', FALSE, TRUE, FALSE, 8);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (59, '1042456970',2015,2, 'eros vestibulum ac est lacinia nisi venenatis tristique fusce congue diam id', FALSE, FALSE, TRUE, 9);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (60, '1070638832',2015,2, 'tempor turpis nec euismod scelerisque quam turpis', TRUE, FALSE, TRUE, 8);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (61, '1052854208',2016,1, 'neque libero convallis eget eleifend luctus ultricies eu nibh quisque', FALSE, TRUE, TRUE, 2);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (62, '1063041490',2016,2, 'dictumst aliquam augue quam sollicitudin vitae consectetuer eget rutrum at lorem integer tincidunt', TRUE, FALSE, TRUE, 6);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (63, '1072553281',2016,1, 'orci nullam molestie nibh in lectus pellentesque at nulla suspendisse potenti cras', FALSE, TRUE, FALSE, 7);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (64, '1099383131',2015,2, 'aenean lectus pellentesque eget nunc donec quis orci eget orci vehicula condimentum', FALSE, FALSE, TRUE, 5);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (65, '1073354193',2016,1, 'vulputate vitae nisl aenean lectus pellentesque eget nunc donec quis', FALSE, FALSE, FALSE, 3);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (66, '1080591323',2016,2, 'accumsan odio curabitur convallis duis consequat dui nec nisi volutpat', FALSE, FALSE, FALSE, 4);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (67, '1027004892',2015,2, 'amet turpis elementum ligula vehicula consequat', TRUE, TRUE, FALSE, 7);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (68, '1053785089',2016,2, 'ante vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae duis', FALSE, TRUE, FALSE, 1);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (69, '1018416428',2016,2, 'dictumst morbi vestibulum velit id pretium', TRUE, TRUE, FALSE, 2);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (70, '1022935503',2016,2, 'luctus et ultrices posuere cubilia curae mauris viverra diam', FALSE, FALSE, TRUE, 6);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (71, '1000772392',2016,1, 'ullamcorper purus sit amet nulla quisque arcu libero rutrum ac lobortis', TRUE, TRUE, FALSE, 3);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (72, '1054493546',2016,2, 'condimentum neque sapien placerat ante nulla justo aliquam', FALSE, TRUE, TRUE, 1);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (73, '1072157384',2015,2, 'quis orci nullam molestie nibh in lectus', FALSE, FALSE, TRUE, 4);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (74, '1084044178',2015,2, 'nec euismod scelerisque quam turpis adipiscing lorem vitae', TRUE, TRUE, FALSE, 3);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (75, '1077041018',2015,2, 'lectus in quam fringilla rhoncus mauris enim', TRUE, FALSE, TRUE, 8);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (76, '1025678468',2015,2, 'diam in magna bibendum imperdiet nullam orci', TRUE, TRUE, FALSE, 3);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (77, '1090089307',2016,2, 'morbi vestibulum velit id pretium iaculis diam erat fermentum justo nec condimentum neque sapien placerat', FALSE, TRUE, TRUE, 2);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (78, '1048652655',2015,2, 'in consequat ut nulla sed accumsan felis ut at dolor quis odio consequat varius integer', FALSE, FALSE, FALSE, 4);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (79, '1051096520',2015,2, 'nonummy integer non velit donec diam neque vestibulum eget', FALSE, FALSE, FALSE, 7);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (80, '1045002529',2015,2, 'praesent blandit nam nulla integer pede justo lacinia eget', FALSE, FALSE, TRUE, 6);
-- END MKS DATA --

-- DOSEN_PEMBIMBING DATA --
INSERT INTO DOSEN_PEMBIMBING VALUES (1,101111130);
INSERT INTO DOSEN_PEMBIMBING VALUES (2,101111111);
INSERT INTO DOSEN_PEMBIMBING VALUES (3,101111113);
INSERT INTO DOSEN_PEMBIMBING VALUES (4,101111120);
INSERT INTO DOSEN_PEMBIMBING VALUES (5,101111115);
INSERT INTO DOSEN_PEMBIMBING VALUES (6,101111112);
INSERT INTO DOSEN_PEMBIMBING VALUES (7,101111125);
INSERT INTO DOSEN_PEMBIMBING VALUES (8,101111122);
INSERT INTO DOSEN_PEMBIMBING VALUES (9,101111112);
INSERT INTO DOSEN_PEMBIMBING VALUES (10,101111117);
INSERT INTO DOSEN_PEMBIMBING VALUES (11,101111127);
INSERT INTO DOSEN_PEMBIMBING VALUES (12,101111128);
INSERT INTO DOSEN_PEMBIMBING VALUES (13,101111129);
INSERT INTO DOSEN_PEMBIMBING VALUES (14,101111119);
INSERT INTO DOSEN_PEMBIMBING VALUES (15,101111115);
INSERT INTO DOSEN_PEMBIMBING VALUES (16,101111120);
INSERT INTO DOSEN_PEMBIMBING VALUES (17,101111114);
INSERT INTO DOSEN_PEMBIMBING VALUES (18,101111122);
INSERT INTO DOSEN_PEMBIMBING VALUES (19,101111127);
INSERT INTO DOSEN_PEMBIMBING VALUES (20,101111128);
INSERT INTO DOSEN_PEMBIMBING VALUES (21,101111113);
INSERT INTO DOSEN_PEMBIMBING VALUES (22,101111111);
INSERT INTO DOSEN_PEMBIMBING VALUES (23,101111126);
INSERT INTO DOSEN_PEMBIMBING VALUES (24,101111116);
INSERT INTO DOSEN_PEMBIMBING VALUES (25,101111123);
INSERT INTO DOSEN_PEMBIMBING VALUES (26,101111119);
INSERT INTO DOSEN_PEMBIMBING VALUES (27,101111120);
INSERT INTO DOSEN_PEMBIMBING VALUES (28,101111124);
INSERT INTO DOSEN_PEMBIMBING VALUES (29,101111113);
INSERT INTO DOSEN_PEMBIMBING VALUES (30,101111115);

insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('73', '8257919474');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('73', '6950685663');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('75', '6950685663');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('45', '6950685663');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('71', '8257919474');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('70', '6950685663');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('49', '3074378873');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('53', '6950685663');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('78', '6950685663');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('76', '7619772623');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('71', '6950685663');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('74', '8257919474');


insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('77', '3074378873');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('71', '3074378873');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('77', '8257919474');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('70', '3074378873');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('76', '6950685663');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('72', '3068327584');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('57', '6950685663');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('70', '8257919474');


insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('77', '6950685663');

insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('76', '8257919474');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('62', '8257919474');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('58', '3074378873');

insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('72', '3074378873');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('61', '6950685663');


insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('51', '6950685663');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('60', '3074378873');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('70', '7619772623');

insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('65', '7619772623');




insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('41', '6950685663');


insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('75', '7619772623');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('78', '8257919474');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('55', '3074378873');

insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('66', '8257919474');


insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('70', '0904434284');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('58', '6950685663');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('72', '8257919474');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('57', '8257919474');


insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('73', '0904434284');

insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('31', '6950685663');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('32', '8257919474');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('33', '6950685663');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('34', '3074378873');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('35', '0904434284');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('36', '6950685663');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('37', '8257919474');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('38', '8257919474');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('39', '6950685663');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('42', '6950685663');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('44', '0904434284');

insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('02', '6950685663');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('03', '3074378873');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('04', '8257919474');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('05', '6950685663');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('06', '7619772623');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('07', '8257919474');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('08', '8257919474');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('09', '3074378873');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('10', '6950685663');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('11', '6950685663');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('12', '8257919474');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('13', '6950685663');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('14', '6950685663');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('15', '6950685663');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('16', '6950685663');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('17', '3074378873');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('18', '6950685663');
insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('19', '6950685663');

insert into dosen_pembimbing (idmks, nipdosenpembimbing) values ('79', '6950685663');

-- END DOSEN_PEMBIMBING DATA --

-- SARAN_DOSEN_PENGUJI DATA --
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (8, '101111119');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (2, '101111119');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (9, '101111119');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (3, '101111118');

insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (1, '101111119');

insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (5, '101111118');


insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (20, '101111119');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (5, '101111119');

insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (3, '101111119');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (6, '101111119');


insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (4, '101111116');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (4, '101111118');

insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (7, '101111118');



insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (9, '101111118');


insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (5, '101111115');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (7, '101111117');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (3, '101111117');


insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (9, '101111117');


insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (7, '101111119');

insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (4, '101111119');

insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (10, '101111119');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (11, '101111119');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (12, '101111119');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (13, '101111118');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (14, '101111119');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (15, '101111119');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (16, '101111119');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (17, '101111119');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (18, '101111119');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (19, '101111118');

insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (21, '101111118');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (22, '101111118');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (23, '101111118');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (24, '101111118');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (25, '101111118');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (26, '101111119');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (27, '101111119');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (28, '101111118');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (29, '101111119');

insert into saran_dosen_penguji (idmks, nipsaranpenguji) values (30, '101111119');

insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('75', '6950685663');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('51', '6950685663');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('76', '6950685663');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('75', '9492643724');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('29', '6950685663');

insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('62', '3074378873');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('78', '0904434284');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('74', '9492643724');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('64', '6950685663');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('77', '0904434284');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('79', '6950685663');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('73', '8257919474');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('77', '3074378873');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('66', '8257919474');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('65', '6950685663');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('68', '8257919474');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('70', '8257919474');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('74', '3074378873');

insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('72', '3074378873');

insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('72', '6950685663');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('71', '6950685663');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('77', '6950685663');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('46', '6950685663');

insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('70', '7619772623');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('79', '8257919474');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('58', '3074378873');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('76', '8257919474');

insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('78', '6950685663');


insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('66', '7619772623');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('21', '8257919474');

insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('72', '8257919474');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('70', '6950685663');

insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('76', '3074378873');


insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('55', '7619772623');


insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('61', '6950685663');


insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('75', '8257919474');






insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('31', '9492643724');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('32', '8257919474');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('33', '6950685663');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('34', '6950685663');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('35', '3074378873');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('36', '7619772623');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('37', '3074378873');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('38', '7619772623');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('39', '6950685663');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('40', '6950685663');

insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('01', '6950685663');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('02', '3074378873');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('03', '6950685663');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('04', '6950685663');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('05', '6950685663');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('06', '8257919474');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('07', '6950685663');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('08', '6950685663');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('09', '6950685663');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('10', '6950685663');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('11', '6950685663');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('12', '3074378873');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('13', '6950685663');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('14', '8257919474');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('15', '6950685663');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('16', '3074378873');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('17', '6950685663');

insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('18', '9492643724');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('19', '6950685663');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('20', '7619772623');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('21', '6950685663');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('22', '6950685663');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('23', '7619772623');
insert into saran_dosen_penguji (idmks, nipsaranpenguji) values ('24', '6950685663');
-- END SARAN_DOSEN_PENGUJI DATA --

-- DOSEN_PENGUJI DATA --
insert into dosen_penguji (idmks, nipdosenpenguji) values (1, '101111116');
insert into dosen_penguji (idmks, nipdosenpenguji) values (2, '101111116');
insert into dosen_penguji (idmks, nipdosenpenguji) values (3, '101111119');
insert into dosen_penguji (idmks, nipdosenpenguji) values (4, '101111117');
insert into dosen_penguji (idmks, nipdosenpenguji) values (5, '101111119');
insert into dosen_penguji (idmks, nipdosenpenguji) values (6, '101111119');
insert into dosen_penguji (idmks, nipdosenpenguji) values (7, '101111118');
insert into dosen_penguji (idmks, nipdosenpenguji) values (8, '101111119');
insert into dosen_penguji (idmks, nipdosenpenguji) values (9, '101111119');
insert into dosen_penguji (idmks, nipdosenpenguji) values (10, '101111119');
insert into dosen_penguji (idmks, nipdosenpenguji) values (11, '101111118');
insert into dosen_penguji (idmks, nipdosenpenguji) values (12, '101111117');
insert into dosen_penguji (idmks, nipdosenpenguji) values (13, '101111118');
insert into dosen_penguji (idmks, nipdosenpenguji) values (14, '101111119');
insert into dosen_penguji (idmks, nipdosenpenguji) values (15, '101111119');
insert into dosen_penguji (idmks, nipdosenpenguji) values (16, '101111118');
insert into dosen_penguji (idmks, nipdosenpenguji) values (17, '101111116');
insert into dosen_penguji (idmks, nipdosenpenguji) values (18, '101111119');
insert into dosen_penguji (idmks, nipdosenpenguji) values (19, '101111117');
insert into dosen_penguji (idmks, nipdosenpenguji) values (20, '101111118');
insert into dosen_penguji (idmks, nipdosenpenguji) values (21, '101111119');
insert into dosen_penguji (idmks, nipdosenpenguji) values (22, '101111117');
insert into dosen_penguji (idmks, nipdosenpenguji) values (23, '101111117');
insert into dosen_penguji (idmks, nipdosenpenguji) values (24, '101111119');
insert into dosen_penguji (idmks, nipdosenpenguji) values (25, '101111117');
insert into dosen_penguji (idmks, nipdosenpenguji) values (26, '101111119');
insert into dosen_penguji (idmks, nipdosenpenguji) values (27, '101111116');
insert into dosen_penguji (idmks, nipdosenpenguji) values (28, '101111119');
insert into dosen_penguji (idmks, nipdosenpenguji) values (29, '101111117');
insert into dosen_penguji (idmks, nipdosenpenguji) values (30, '101111119');
insert into dosen_penguji (idmks, nipdosenpenguji) values (21, '101111117');
insert into dosen_penguji (idmks, nipdosenpenguji) values (25, '101111119');
insert into dosen_penguji (idmks, nipdosenpenguji) values (19, '101111119');
insert into dosen_penguji (idmks, nipdosenpenguji) values (10, '101111118');
insert into dosen_penguji (idmks, nipdosenpenguji) values (22, '101111119');
insert into dosen_penguji (idmks, nipdosenpenguji) values (25, '101111118');

insert into dosen_penguji (idmks, nipdosenpenguji) values (23, '101111119');
insert into dosen_penguji (idmks, nipdosenpenguji) values (22, '101111115');

insert into dosen_penguji (idmks, nipdosenpenguji) values (11, '101111119');
insert into dosen_penguji (idmks, nipdosenpenguji) values (21, '101111118');

insert into dosen_penguji (idmks, nipdosenpenguji) values ('71', '8257919474');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('72', '6950685663');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('79', '9492643724');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('79', '3074378873');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('71', '9492643724');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('78', '8257919474');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('75', '3074378873');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('67', '6950685663');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('77', '6950685663');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('73', '7619772623');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('78', '3074378873');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('62', '3074378873');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('75', '6950685663');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('62', '8257919474');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('71', '6950685663');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('46', '6950685663');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('73', '6950685663');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('74', '6950685663');

insert into dosen_penguji (idmks, nipdosenpenguji) values ('69', '6950685663');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('75', '8257919474');

insert into dosen_penguji (idmks, nipdosenpenguji) values ('78', '6950685663');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('79', '8257919474');

insert into dosen_penguji (idmks, nipdosenpenguji) values ('63', '8257919474');

insert into dosen_penguji (idmks, nipdosenpenguji) values ('50', '6950685663');


insert into dosen_penguji (idmks, nipdosenpenguji) values ('61', '6950685663');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('31', '6950685663');

insert into dosen_penguji (idmks, nipdosenpenguji) values ('76', '6950685663');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('71', '7619772623');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('66', '6950685663');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('70', '8257919474');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('70', '5937082790');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('76', '3068327584');





insert into dosen_penguji (idmks, nipdosenpenguji) values ('72', '8257919474');

insert into dosen_penguji (idmks, nipdosenpenguji) values ('52', '6950685663');




insert into dosen_penguji (idmks, nipdosenpenguji) values ('51', '6950685663');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('73', '8257919474');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('64', '6950685663');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('76', '8257919474');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('74', '7619772623');

insert into dosen_penguji (idmks, nipdosenpenguji) values ('56', '6950685663');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('68', '6950685663');


insert into dosen_penguji (idmks, nipdosenpenguji) values ('31', '8257919474');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('32', '8257919474');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('33', '3074378873');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('34', '6950685663');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('35', '6950685663');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('36', '9492643724');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('37', '6950685663');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('38', '6950685663');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('39', '6950685663');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('40', '8257919474');


insert into dosen_penguji (idmks, nipdosenpenguji) values ('57', '6950685663');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('55', '8257919474');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('55', '6950685663');

insert into dosen_penguji (idmks, nipdosenpenguji) values ('59', '6950685663');

insert into dosen_penguji (idmks, nipdosenpenguji) values ('51', '7619772623');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('53', '6950685663');


insert into dosen_penguji (idmks, nipdosenpenguji) values ('58', '8257919474');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('55', '3074378873');

insert into dosen_penguji (idmks, nipdosenpenguji) values ('51', '9492643724');



insert into dosen_penguji (idmks, nipdosenpenguji) values ('58', '6950685663');

insert into dosen_penguji (idmks, nipdosenpenguji) values ('01', '6950685663');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('02', '6950685663');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('03', '6950685663');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('04', '6950685663');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('05', '6950685663');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('06', '6950685663');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('07', '6950685663');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('08', '6950685663');
insert into dosen_penguji (idmks, nipdosenpenguji) values ('09', '6950685663');
-- END DOSEN_PENGUJI DATA --

-- TIMELINE DATA --
insert into timeline (idtimeline, namaevent, tanggal, tahun, semester) values (1, 'Pemberian izin maju sidang oleh pembimbing', '2015-12-10', '2015', '2');
insert into timeline (idtimeline, namaevent, tanggal, tahun, semester) values (2, 'Pengisian jadwal pribadi oleh dosen', '2015-11-20', '2015', '2');
insert into timeline (idtimeline, namaevent, tanggal, tahun, semester) values (3, 'Berkas sidang', '2015-10-29', '2015', '2');

insert into timeline (idtimeline, namaevent, tanggal, tahun, semester) values (4, 'Pemberian izin maju sidang oleh pembimbing', '2016-04-09', '2016', '1');
insert into timeline (idtimeline, namaevent, tanggal, tahun, semester) values (5, 'Pengisian jadwal pribadi oleh dosen', '2016-05-19', '2016', '1');
insert into timeline (idtimeline, namaevent, tanggal, tahun, semester) values (6, 'Berkas sidang', '2016-06-28', '2016', '1');

insert into timeline (idtimeline, namaevent, tanggal, tahun, semester) values (7, 'Pemberian izin maju sidang oleh pembimbing', '2016-12-8', '2016', '2');
insert into timeline (idtimeline, namaevent, tanggal, tahun, semester) values (8, 'Pengisian jadwal pribadi oleh dosen', '2016-11-18', '2016', '2');
insert into timeline (idtimeline, namaevent, tanggal, tahun, semester) values (9, 'Berkas sidang', '2016-10-27', '2016', '2');
-- END TIMELINE DATA --

-- JADWAL_NON_SIDANG DATA --
INSERT INTO JADWAL_NON_SIDANG VALUES (61,'2016-01-15','2016-02-27','Ke luar kota','mingguan',101111111);

INSERT INTO JADWAL_NON_SIDANG VALUES (62,'2016-04-30','2016-05-28','Ke luar kota',NULL,101111113);
INSERT INTO JADWAL_NON_SIDANG VALUES (63,'2016-01-01','2016-01-02','Cuti',NULL,101111114);
INSERT INTO JADWAL_NON_SIDANG VALUES (64,'2015-08-07','2015-09-07','Ke luar kota','bulanan',101111115);
INSERT INTO JADWAL_NON_SIDANG VALUES (65,'2015-08-06','2015-08-30','Cuti',NULL,101111116);
INSERT INTO JADWAL_NON_SIDANG VALUES (66,'2016-03-07','2016-05-09','Cuti',NULL,101111117);
INSERT INTO JADWAL_NON_SIDANG VALUES (67,'2015-09-02','2015-11-03','Rapat',NULL,101111118);
INSERT INTO JADWAL_NON_SIDANG VALUES (68,'2015-10-01','2015-11-07','Rapat','harian',101111119);
INSERT INTO JADWAL_NON_SIDANG VALUES (69,'2015-08-30','2015-10-20','Cuti',NULL,101111120);

INSERT INTO JADWAL_NON_SIDANG VALUES (70,'2016-02-01','2016-04-05','Rapat','harian',101111122);

INSERT INTO JADWAL_NON_SIDANG VALUES (71,'2016-05-03','2016-07-05','Cuti',NULL,101111124);
INSERT INTO JADWAL_NON_SIDANG VALUES (72,'2016-08-29','2016-10-09','Rapat','harian',101111125);
INSERT INTO JADWAL_NON_SIDANG VALUES (73,'2016-07-03','2016-09-20','Ke luar kota',NULL,101111126);
INSERT INTO JADWAL_NON_SIDANG VALUES (74,'2016-03-30','2016-04-27','Rapat','mingguan',101111127);
INSERT INTO JADWAL_NON_SIDANG VALUES (75,'2016-07-20','2016-10-03','Cuti',NULL,101111128);
INSERT INTO JADWAL_NON_SIDANG VALUES (76,'2016-03-30','2016-05-30','Rapat','bulanan',101111129);
INSERT INTO JADWAL_NON_SIDANG VALUES (77,'2016-06-20','2016-08-19','Ke luar kota','mingguan',101111130);
INSERT INTO JADWAL_NON_SIDANG VALUES (78,'2015-10-10','2016-01-01','Rapat',NULL,101111126);
INSERT INTO JADWAL_NON_SIDANG VALUES (79,'2015-11-01','2015-12-30','Cuti',NULL,101111126);
INSERT INTO JADWAL_NON_SIDANG VALUES (80,'2015-09-10','2015-10-30','Rapat','mingguan',101111111);
INSERT INTO JADWAL_NON_SIDANG VALUES (81,'2015-08-30','2015-10-21','Ke luar kota','mingguan',101111119);
INSERT INTO JADWAL_NON_SIDANG VALUES (82,'2016-04-27','2016-06-01','Cuti',NULL,101111112);
INSERT INTO JADWAL_NON_SIDANG VALUES (83,'2015-08-02','2015-09-10','Ke luar kota','bulanan',101111120);
INSERT INTO JADWAL_NON_SIDANG VALUES (84,'2015-08-07','2015-08-10','Cuti',NULL,101111111);
INSERT INTO JADWAL_NON_SIDANG VALUES (85,'2016-09-10','2016-10-11','Rapat','mingguan',101111115);

INSERT INTO JADWAL_NON_SIDANG VALUES (87,'2016-02-29','2016-03-10','Cuti',NULL,101111129);

INSERT INTO JADWAL_NON_SIDANG VALUES (88,'2015-08-14','2015-09-14','Cuti','bulanan',101111112);
INSERT INTO JADWAL_NON_SIDANG VALUES (89,'2016-09-30','2016-10-11','Ke luar kota','mingguan',101111121);
INSERT INTO JADWAL_NON_SIDANG VALUES (90,'2016-11-07','2016-12-31','Ke luar kota','mingguan',101111123);

INSERT INTO JADWAL_NON_SIDANG VALUES (86,'2015-10-30','2015-11-27','Rapat','harian',101111113);

insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (1, '2017-06-30', '2017-01-05', 'consequatdio donec vitae', 'bulanan', '6950685663');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (2, '2017-02-19', '2017-09-19', 'tortor quis turpi nulla neque libero convallis eget eleifend luctus ultricies eu nibh', 'bulanan', '6963282590');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (3, '2017-02-02', '2017-03-23', 'ligula in lacus curabitur arcu libero rutrum ac lobortis vel dapibus at diam', 'harian', '6824071607');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (4, '2017-07-03', '2016-11-21', 'lectus aliquam senatis non sodales sed tincidunt eu', 'bulanan', '6824071607');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (5, '2017-01-30', '2017-05-27', 'blandit ultrices enim lorem roin interdum mauris non ligula pellentesque ultrices', 'mingguan', '6950685663');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (6, '2017-04-29', '2016-10-16', 'a nibh in quis justo maecenacenas leo odio condimentum id luctus nec molestie sed justo', 'bulanan', '6824071607');

insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (8, '2017-06-02', '2017-03-25', 'auctor sed tristique in temps sit amet sem fusce consequat nulla nisl nunc', 'mingguan', '6824071607');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (9, '2017-05-14', '2017-04-27', 'varius ut blandit non interdm in ante vestibulum ante ipsum primis in faucibus orci luctus', 'mingguan', '8655528560');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (10, '2017-05-10', '2017-09-05', 'hac habitasse platea dictumum iaculis diam erat fermentum justo nec condimentum neque', 'bulanan', '6963282590');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (11, '2017-06-10', '2017-07-12', 'non velit donec diam neque es posuere cubilia curae donec pharetra magna vestibulum', 'harian', '6950685663');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (12, '2017-01-10', '2016-12-29', 'dolor sit amet consectetuerng elit proin risus praesent lectus vestibulum quam sapien varius', 'bulanan', '6963282590');

insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (14, '2017-02-17', '2017-05-27', 'molestie lorem quisque ut evida nisi at nibh in hac habitasse platea dictumst', 'bulanan', '6824071607');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (15, '2017-03-27', '2017-07-28', 'proin at turpis a pede posu integer non velit donec diam neque vestibulum eget', 'harian', '6824071607');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (16, '2017-08-15', '2017-08-30', 'varius nulla facilisi cras elit nec nisi vulputate nonummy maecenas', 'bulanan', '6824071607');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (17, '2017-05-13', '2017-02-22', 'arcu adipiscing molestie het vulputate vitae nisl aenean lectus pellentesque eget nunc', 'harian', '6824071607');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (18, '2017-05-24', '2017-02-20', 'odio cras mi pede malesuadaonsectetuer adipiscing elit proin interdum mauris', 'bulanan', '6824071607');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (19, '2017-02-28', '2016-10-04', 'leo pellentesque ultrices maugue a suscipit nulla elit ac nulla sed vel enim', 'mingguan', '9492643724');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (20, '2017-01-14', '2017-02-06', 'ut blandit non interdum in ante vestibulum ante ipsum primis in', 'mingguan', '6963282590');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (21, '2017-04-16', '2016-09-13', 'ligula suspendisse ornare cm fusce consequat nulla nisl nunc nisl duis bibendum', 'harian', '6950685663');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (22, '2016-12-24', '2017-09-02', 'diam vitae quam suspendissenon mauris morbi non lectus aliquam sit', 'bulanan', '6963282590');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (23, '2017-07-24', '2017-01-09', 'donec ut mauris eget massa et sapien dignissim vestibulum vestibulum ante', 'bulanan', '6824071607');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (24, '2017-04-17', '2017-10-09', 'felis fusce posuere felis svinar sed nisl nunc rhoncus dui vel', 'harian', '6950685663');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (25, '2017-07-17', '2017-10-12', 'aliquet massa id lobortis cris lacinia sapien quis libero nullam sit amet turpis elementum', 'mingguan', '6963282590');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (26, '2017-06-14', '2017-05-28', 'convallis eget eleifend luci luctus et ultrices posuere cubilia curae nulla dapibus', 'harian', '6824071607');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (27, '2017-01-11', '2016-12-26', 'orci nullam molestie nibh isse poteis parturient montes nascetur ridiculus', 'mingguan', '6963282590');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (28, '2016-12-22', '2017-07-06', 'eget eleifend lucbulum ante ipsum primis', 'bulanan', '6963282590');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (29, '2016-12-22', '2017-04-29', 'non lectus venenatis non sodales sed tincidunt eu felis fusce posuere felis', 'bulanan', '6824071607');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (30, '2017-01-03', '2017-04-24', 'tristique it mi in porttitor pede justo eu massa donec', 'mingguan', '6963282590');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (31, '2017-01-25', '2016-10-07', 'ut odio er adipiscing', 'bulanan', '6824071607');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (32, '2017-06-14', '2017-01-26', 'conv sit amet turpis elementum ligula vehicula consequat morbi a ipsum integer a nibh', 'bulanan', '6824071607');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (33, '2017-06-29', '2017-09-29', 'orci nullam molestie nibh in lectus pellentesque at nulla suspendisse potenti cras in', 'mingguan', '6824071607');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (34, '2017-03-15', '2016-09-04', 'aumentum ligula vehicula consequat morbi a ipsum integer a nibh in quis justo maecenas', 'bulanan', '6824071607');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (35, '2017-02-05', '2017-01-15', 'fusce po sagittis nam congue risus semper porta volutpat quam pede', 'harian', '6824071607');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (36, '2017-07-20', '2016-11-20', 'tristique esttus et ultrices posuere', 'mingguan', '6824071607');

insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (38, '2016-12-08', '2017-07-29', 'duis consequat dui nec nisi volutpat eleifend donec ut dolor morbi vel lectus in quam', 'bulanan', '6824071607');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (39, '2017-07-22', '2017-09-29', 'cursus id turpis integer aliquet massa id lobortis convallis tortor risus dapibus augue vel', 'harian', '6963282590');


insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (42, '2017-07-12', '2016-11-27', 'in faucibus orci luctus et ultrices posuere cubilia curae duis faucibus accumsan', 'mingguan', '8655528560');


insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (45, '2017-03-13', '2016-11-15', 'tortor dlla neque libero convallis eget eleifend luctus ultricies eu', 'mingguan', '6963282590');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (46, '2017-03-13', '2017-08-06', 'hendrbero', 'bulanan', '6950685663');

insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (48, '2017-04-11', '2017-04-25', 'maecenas rhoncortor id nulla ultrices aliquet', 'harian', '8655528560');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (49, '2016-12-29', '2016-11-20', 'faucibus cursus urna ut tellus nulla ut erat id mauris vulputate elementum nullam', 'harian', '6824071607');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (50, '2017-01-16', '2017-10-09', 'pulvinar nulla pede ullamcorper augue a suscipibitur at ipsum', 'mingguan', '6824071607');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (51, '2016-12-27', '2016-11-01', 'vel augue vestibulum racinia aenean sit amet justo morbi ut odio cras mi', 'harian', '8655528560');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (52, '2017-08-17', '2017-01-14', 'eu est congaculis diam erat fermentum justo', 'bulanan', '6824071607');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (53, '2017-05-19', '2017-05-25', 'at velit vivamus vel nullt quisque erat eros viverra eget congue eget', 'mingguan', '6824071607');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (54, '2017-04-29', '2017-04-25', 'turpis enim blandit  dapibus duis at velit eu est congue elementum in hac', 'bulanan', '8655528560');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (55, '2016-12-15', '2017-04-05', 'dignissim vestibulum ve et ultrices pin ut suscipit a feugiat et eros', 'harian', '6963282590');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (56, '2017-04-10', '2017-04-09', 'curabitur at ipsum ac tellus semper interdum maum ac', 'harian', '8655528560');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (57, '2017-04-18', '2017-07-04', 'id lu et tempus semper est quam pharetra magna ac consequat', 'bulanan', '6824071607');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (58, '2016-12-27', '2017-10-14', 'pulvinar nulla pede ullamcorper augue a suscipit nulla elit ac nulla sed vel enim sit amet', 'bulanan', '6963282590');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (59, '2017-07-17', '2017-05-09', 'imperdiet nullam orci pede venenatis non sodalmorbi sem', 'harian', '6824071607');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (60, '2017-04-06', '2017-07-30', 'a suscipit nulla elit ac nulla sed vel enim sit amet nuur at', 'mingguan', '6950685663');

insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (7, '2017-06-12', '2017-03-20', 'lorem ipsum dolor sit amet ces phasellus id sapien in sapienpiscing', 'bulanan', '8655528560');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (13, '2017-02-23', '2017-01-13', 'eget rutrum at lorem integat nunc', 'mingguan', '6824071607');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (37, '2017-06-06', '2016-09-14', 'nulla dapibus dolor vel est donem ac', 'harian', '6824071607');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (40, '2017-06-18', '2017-10-11', 'id nisl venenatis lacinipsum dolor sit amet', 'harian', '6963282590');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (41, '2017-03-24', '2017-07-29', 'interdum venepibus duis at velit', 'mingguan', '6824071607');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (43, '2017-04-21', '2016-12-22', 'lorem integer tincidunt ante de justo', 'bulanan', '6824071607');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (44, '2017-08-01', '2016-12-27', 'lacinia sapien quis libero nullam i a ipsum integer a', 'mingguan', '6963282590');
insert into jadwal_non_sidang (idjadwal, tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen) values (47, '2016-12-07', '2017-03-06', 'pede llla suspendisse potenti craus et magnis', 'bulanan', '6824071607');
-- END JADWAL_NON_SIDANG DATA --

-- RUANGAN DATA --
insert into ruangan (idruangan, namaruangan) values (21, 'Ruang 1150');
insert into ruangan (idruangan, namaruangan) values (22, 'Ruang 3122');
insert into ruangan (idruangan, namaruangan) values (23, 'Ruang 2112');
insert into ruangan (idruangan, namaruangan) values (24, 'Ruang 1123');
insert into ruangan (idruangan, namaruangan) values (25, 'Ruang 3321');
insert into ruangan (idruangan, namaruangan) values (26, 'Ruang 2111');
insert into ruangan (idruangan, namaruangan) values (27, 'Ruang 5121');
insert into ruangan (idruangan, namaruangan) values (28, 'Ruang 5012');
insert into ruangan (idruangan, namaruangan) values (29, 'Ruang 6124');
insert into ruangan (idruangan, namaruangan) values (30, 'Ruang 6227');
insert into ruangan (idruangan, namaruangan) values (31, 'Ruang 6118');
insert into ruangan (idruangan, namaruangan) values (32, 'Ruang 6231');
insert into ruangan (idruangan, namaruangan) values (33, 'Ruang 6157');
insert into ruangan (idruangan, namaruangan) values (34, 'Ruang 6129');
insert into ruangan (idruangan, namaruangan) values (35, 'Ruang 6239');

insert into ruangan (idruangan, namaruangan) values (1, 'Sage');
insert into ruangan (idruangan, namaruangan) values (2, 'Schlimgen');
insert into ruangan (idruangan, namaruangan) values (3, 'Artisan');
insert into ruangan (idruangan, namaruangan) values (4, 'Steensland');
insert into ruangan (idruangan, namaruangan) values (5, 'Marcy');
insert into ruangan (idruangan, namaruangan) values (6, 'Roxbury');
insert into ruangan (idruangan, namaruangan) values (7, 'Basil');
insert into ruangan (idruangan, namaruangan) values (8, 'Hoepker');
insert into ruangan (idruangan, namaruangan) values (9, 'Sachtjen');
insert into ruangan (idruangan, namaruangan) values (10, 'Butternut');
insert into ruangan (idruangan, namaruangan) values (11, 'Coolidge');
insert into ruangan (idruangan, namaruangan) values (12, 'Oak Valley');
insert into ruangan (idruangan, namaruangan) values (13, 'Village Green');
insert into ruangan (idruangan, namaruangan) values (14, 'Debra');
insert into ruangan (idruangan, namaruangan) values (15, 'Rowland');
insert into ruangan (idruangan, namaruangan) values (16, 'Parkside');
insert into ruangan (idruangan, namaruangan) values (17, 'Victoria');
insert into ruangan (idruangan, namaruangan) values (18, 'Talmadge');
insert into ruangan (idruangan, namaruangan) values (19, '1st');
insert into ruangan (idruangan, namaruangan) values (20, 'Gerald');

-- END RUANGAN DATA --

-- JADWAL_SIDANG DATA --
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (61, 1, '2016-10-11', '9:49 AM', '4:37 PM', 30);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (62, 20, '2016-11-27', '10:55 AM', '1:58 PM', 33);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (63, 20, '2016-10-30', '10:29 AM', '4:14 PM', 34);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (64, 10, '2016-11-18', '9:26 AM', '3:58 PM', 28);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (65, 22, '2016-10-15', '9:05 AM', '3:00 PM', 27);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (66, 25, '2016-11-16', '10:09 AM', '1:25 PM', 28);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (67, 25, '2016-12-12', '11:15 AM', '2:03 PM', 28);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (68, 11, '2016-12-22', '11:27 AM', '4:15 PM', 33);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (69, 13, '2016-11-30', '9:29 AM', '5:26 PM', 23);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (70, 16, '2016-11-09', '11:33 AM', '1:52 PM', 28);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (71, 19, '2016-10-25', '11:43 AM', '1:07 PM', 22);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (72, 28, '2016-10-22', '9:52 AM', '5:31 PM', 29);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (73, 28, '2016-12-15', '9:01 AM', '2:50 PM', 32);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (74, 21, '2016-10-31', '11:34 AM', '1:37 PM', 28);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (75, 21, '2016-12-02', '9:10 AM', '3:02 PM', 26);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (76, 20, '2016-12-11', '11:30 AM', '4:14 PM', 25);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (77, 22, '2016-10-31', '9:45 AM', '4:52 PM', 24);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (78, 19, '2016-11-08', '8:45 AM', '4:43 PM', 26);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (79, 19, '2016-11-30', '8:54 AM', '2:53 PM', 29);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (80, 10, '2016-10-15', '8:43 AM', '5:43 PM', 32);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (81, 20, '2016-11-08', '10:00 AM', '3:25 PM', 27);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (82, 20, '2016-10-29', '8:43 AM', '2:43 PM', 33);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (83, 30, '2016-11-29', '9:41 AM', '5:24 PM', 28);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (84, 20, '2016-11-27', '9:02 AM', '2:48 PM', 33);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (85, 20, '2016-12-16', '10:54 AM', '1:55 PM', 27);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (86, 30, '2016-10-24', '10:42 AM', '3:34 PM', 24);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (87, 11, '2016-11-13', '11:38 AM', '3:53 PM', 31);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (88, 11, '2016-10-30', '9:54 AM', '2:25 PM', 31);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (89, 11, '2016-11-09', '9:35 AM', '1:59 PM', 32);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (90, 11, '2016-12-28', '9:03 AM', '1:54 PM', 30);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (91, 11, '2016-12-07', '11:57 AM', '2:15 PM', 21);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (92, 11, '2016-12-13', '8:35 AM', '5:09 PM', 34);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (93, 5, '2016-12-08', '8:32 AM', '4:42 PM', 27);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (94, 11, '2016-11-28', '9:10 AM', '1:51 PM', 30);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (95, 20, '2016-10-27', '9:14 AM', '3:00 PM', 21);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (96, 20, '2016-12-10', '9:32 AM', '1:27 PM', 35);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (97, 20, '2016-12-15', '11:46 AM', '4:25 PM', 29);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (98, 20, '2016-10-29', '9:56 AM', '3:20 PM', 34);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (99, 20, '2016-12-11', '10:06 AM', '3:36 PM', 35);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (100, 10, '2016-12-27', '8:05 AM', '1:44 PM', 25);

	insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (1, 60, '2016-11-20', '10:05 AM', '1:44 PM', 10);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (2, 64, '2016-12-10', '8:40 AM', '3:21 PM', 17);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (3, 65, '2016-10-24', '10:46 AM', '2:47 PM', 10);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (4, 62, '2016-10-19', '11:05 AM', '1:57 PM', 6);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (5, 18, '2016-10-19', '8:19 AM', '5:03 PM', 19);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (6, 32, '2016-12-20', '9:13 AM', '5:29 PM', 11);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (7, 21, '2016-11-05', '9:28 AM', '4:12 PM', 4);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (8, 33, '2016-10-30', '10:00 AM', '2:36 PM', 14);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (9, 35, '2016-12-04', '9:00 AM', '3:02 PM', 3);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (10, 14, '2016-11-01', '8:06 AM', '1:00 PM', 4);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (11, 27, '2016-12-14', '8:46 AM', '4:00 PM', 15);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (12, 70, '2016-12-22', '11:24 AM', '2:10 PM', 11);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (13, 53, '2016-11-21', '8:57 AM', '1:37 PM', 19);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (14, 20, '2016-11-02', '11:11 AM', '3:15 PM', 3);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (15, 8, '2016-12-10', '9:17 AM', '4:18 PM', 14);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (16, 19, '2016-10-24', '11:50 AM', '1:20 PM', 11);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (17, 54, '2016-10-22', '11:33 AM', '4:26 PM', 8);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (18, 18, '2016-12-29', '10:16 AM', '5:12 PM', 8);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (19, 43, '2016-12-14', '8:17 AM', '1:01 PM', 14);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (20, 22, '2016-12-09', '8:27 AM', '1:28 PM', 19);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (21, 42, '2016-11-21', '8:41 AM', '1:23 PM', 16);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (22, 14, '2016-12-01', '11:57 AM', '3:58 PM', 12);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (23, 58, '2016-12-10', '11:14 AM', '5:22 PM', 10);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (24, 26, '2016-10-31', '9:59 AM', '5:10 PM', 8);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (25, 52, '2016-12-13', '9:51 AM', '2:59 PM', 1);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (26, 29, '2016-11-03', '8:46 AM', '4:29 PM', 9);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (27, 19, '2016-12-21', '8:29 AM', '3:06 PM', 6);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (28, 45, '2016-10-10', '8:33 AM', '3:56 PM', 18);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (29, 43, '2016-11-22', '8:14 AM', '3:16 PM', 3);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (30, 45, '2016-12-21', '11:43 AM', '3:10 PM', 6);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (31, 9, '2016-10-29', '9:11 AM', '3:25 PM', 8);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (32, 13, '2016-11-05', '10:09 AM', '4:13 PM', 5);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (33, 50, '2016-10-17', '9:35 AM', '4:46 PM', 20);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (34, 55, '2016-12-03', '8:24 AM', '3:23 PM', 19);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (35, 39, '2016-10-18', '10:52 AM', '2:57 PM', 17);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (36, 22, '2016-12-23', '11:48 AM', '4:24 PM', 20);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (37, 13, '2016-10-24', '9:29 AM', '4:05 PM', 19);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (38, 11, '2016-12-09', '9:22 AM', '2:55 PM', 18);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (39, 51, '2016-12-08', '11:57 AM', '1:18 PM', 7);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (40, 46, '2016-10-29', '11:05 AM', '4:44 PM', 13);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (41, 66, '2016-12-18', '11:40 AM', '2:20 PM', 20);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (42, 2, '2016-12-29', '9:11 AM', '4:08 PM', 7);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (43, 14, '2016-11-22', '11:17 AM', '3:53 PM', 18);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (44, 10, '2016-11-25', '11:20 AM', '5:00 PM', 2);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (45, 59, '2016-12-24', '10:45 AM', '2:59 PM', 1);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (46, 17, '2016-10-27', '11:11 AM', '5:08 PM', 11);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (47, 67, '2016-10-14', '10:11 AM', '1:57 PM', 8);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (48, 42, '2016-11-23', '9:55 AM', '5:32 PM', 18);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (49, 20, '2016-11-23', '8:41 AM', '3:55 PM', 7);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (50, 33, '2016-12-17', '10:18 AM', '2:40 PM', 10);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (51, 67, '2016-10-15', '9:17 AM', '5:27 PM', 7);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (52, 16, '2016-12-29', '8:58 AM', '5:21 PM', 7);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (53, 62, '2016-11-08', '9:59 AM', '3:00 PM', 6);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (54, 7, '2016-12-06', '8:48 AM', '4:14 PM', 3);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (55, 21, '2016-11-23', '10:51 AM', '2:14 PM', 19);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (56, 43, '2016-11-08', '9:40 AM', '1:00 PM', 2);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (57, 40, '2016-12-12', '8:03 AM', '5:04 PM', 7);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (58, 68, '2016-11-30', '9:03 AM', '4:44 PM', 20);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (59, 10, '2016-11-23', '9:12 AM', '5:42 PM', 6);
insert into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values (60, 47, '2016-10-12', '9:54 AM', '5:24 PM', 3);
-- END JADWAL_SIDANG DATA --

-- BERKAS DATA --
INSERT INTO BERKAS VALUES (50,1,'TurpisEnimBlandit.pdf','http://accuweather.com/amet/sapien/dignissim/vestibulum/vestibulum.html');
INSERT INTO BERKAS VALUES (51,2,'InLeoMaecenas.pdf','http://fda.gov/augue.html');
INSERT INTO BERKAS VALUES (52,3,'DonecVitaeNisi.pdf','http://google.it/ultrices/enim/lorem.html');
INSERT INTO BERKAS VALUES (53,4,'SitAmet.pdf','https://intel.com/molestie/lorem/quisque/ut.html');
INSERT INTO BERKAS VALUES (54,5,'Vestibulum.pdf','https://pen.io/eget/rutrum/at/lorem.xml');
INSERT INTO BERKAS VALUES (55,6,'EgetRutrumAt.pdf','http://un.org/eget/rutrum/at.html');
INSERT INTO BERKAS VALUES (56,7,'JustoSollicitudin.pdf','https://dmoz.org/mi/nulla.html');
INSERT INTO BERKAS VALUES (57,8,'SedJustoPellentesque.pdf','https://go.com/vulputate/elementum/nullam/varius/nulla/facilisi.html');
INSERT INTO BERKAS VALUES (58,9,'UltricesEratTortor.pdf','http://barnesandnoble.com/non/ligula/pellentesque/ultrices/phasellus/id/sapien.html');
INSERT INTO BERKAS VALUES (59,10,'OrciEgetOrci.pdf','http://cbc.ca/mauris/laoreet.html');
INSERT INTO BERKAS VALUES (50,11,'InFaucibusOrci.pdf','http://prlog.org/viverra/eget.html');
INSERT INTO BERKAS VALUES (61,12,'InCongue.pdf','https://va.gov/blandit/ultrices.html');
INSERT INTO BERKAS VALUES (52,13,'Suspendisse.pdf','https://lycos.com/et.html');
INSERT INTO BERKAS VALUES (63,14,'VestibulumQuam.pdf','http://nba.com/nisl/aenean.html');
INSERT INTO BERKAS VALUES (64,15,'TinciduntLacus.pdf','https://ovh.net/purus/eu/magna/vulputate/luctus.html');
INSERT INTO BERKAS VALUES (65,16,'SedTristiqueIn.pdf','https://dailymail.co.uk/luctus/et/ultrices/posuere/cubilia.html');
INSERT INTO BERKAS VALUES (55,17,'InFaucibusOrci.pdf','https://xing.com/sed.html');
INSERT INTO BERKAS VALUES (67,18,'Tempor.pdf','https://theatlantic.com/ac.html');
INSERT INTO BERKAS VALUES (68,19,'AcTellus.pdf','https://flavors.me/pretium.html');
INSERT INTO BERKAS VALUES (69,20,'AeneanLectusPellentesque.pdf','https://csmonitor.com/praesent/id/massa.html');
INSERT INTO BERKAS VALUES (70,21,'PedePosuere.pdf','https://accuweather.com/augue/vel.html');
INSERT INTO BERKAS VALUES (66,22,'VenenatisNonSodales.pdf','https://wikispaces.com/nulla/ac/enim/in/tempor.html');
INSERT INTO BERKAS VALUES (72,23,'Vitae.pdf','http://ifeng.com/lorem/id/ligula/suspendisse/ornare.html');
INSERT INTO BERKAS VALUES (73,24,'Integer.pdf','http://icq.com/augue/quam/sollicitudin/vitae/consectetuer/eget/rutrum.html');
INSERT INTO BERKAS VALUES (74,25,'CubiliaCuraeDonec.pdf','https://mail.ru/habitasse/platea.js');
INSERT INTO BERKAS VALUES (75,26,'MattisPulvinar.pdf','http://jimdo.com/eget/nunc/donec.html');
INSERT INTO BERKAS VALUES (76,27,'Enim.pdf','https://patch.com/imperdiet/et/commodo/vulputate/justo/in/blandit.html');
INSERT INTO BERKAS VALUES (68,28,'Platea.pdf','http://amazon.co.uk/nisi/venenatis/tristique/fusce/congue.xml');
INSERT INTO BERKAS VALUES (78,29,'InLacusCurabitur.pdf','http://freewebs.com/hendrerit.html');
INSERT INTO BERKAS VALUES (79,30,'Amet.pdf','http://marketwatch.com/et/tempus/semper/est.html');
INSERT INTO BERKAS VALUES (90,31,'EuMagna.pdf','http://comcast.net/interdum/in/ante/vestibulum.html');
INSERT INTO BERKAS VALUES (81,32,'UltricesPhasellusId.pdf','https://nytimes.com/nec/nisi/vulputate/nonummy/maecenas.xml');
INSERT INTO BERKAS VALUES (82,33,'AccumsanTortorQuis.pdf','https://ft.com/sapien.html');
INSERT INTO BERKAS VALUES (83,34,'LoremIntegerTincidunt.pdf','https://jalbum.net/libero/nullam/sit/amet/turpis.html');
INSERT INTO BERKAS VALUES (84,35,'Nisi.pdf','http://shareasale.com/massa/quis.html');
INSERT INTO BERKAS VALUES (85,36,'In.pdf','http://salon.com/integer/ac.html');
INSERT INTO BERKAS VALUES (94,37,'At.pdf','http://constantcontact.com/orci/luctus/et.html');
INSERT INTO BERKAS VALUES (87,38,'MorbiOdioOdio.pdf','https://squidoo.com/urna/pretium/nisl/ut/volutpat/sapien.html');
INSERT INTO BERKAS VALUES (88,39,'EuSapienCursus.pdf','http://goo.ne.jp/lacinia.html');
INSERT INTO BERKAS VALUES (89,40,'FaucibusOrciLuctus.pdf','https://wikimedia.org/eget/vulputate/ut/ultrices.js');
INSERT INTO BERKAS VALUES (90,41,'Lacinia.pdf','http://domainmarket.com/feugiat/et/eros/vestibulum/ac/est.xml');
INSERT INTO BERKAS VALUES (91,42,'AtVelit.pdf','http://pen.io/montes.html');

INSERT INTO BERKAS VALUES (93,44,'Ipsum.pdf','http://hibu.com/luctus/cum/sociis/natoque/penatibus.html');
INSERT INTO BERKAS VALUES (94,45,'VolutpatQuam.pdf','http://cnbc.com/quis.html');
INSERT INTO BERKAS VALUES (95,46,'FaucibusCursus.pdf','http://list-manage.com/pulvinar/nulla/pede.html');
INSERT INTO BERKAS VALUES (96,47,'NislNunc.pdf','http://biglobe.ne.jp/molestie/hendrerit.html');
INSERT INTO BERKAS VALUES (97,48,'MassaTempor.pdf','http://e-recht24.de/urna/ut/tellus/nulla/ut.js');
INSERT INTO BERKAS VALUES (98,49,'PosuereCubiliaCurae.pdf','http://comcast.net/nulla/mollis/molestie/lorem/quisque.html');
INSERT INTO BERKAS VALUES (99,50,'Fringilla.pdf','https://sakura.ne.jp/enim/leo/rhoncus/sed.html');

INSERT INTO BERKAS VALUES (92,50,'Ultrices.pdf','http://soup.io/nulla/ac/enim.html');

insert into berkas (idberkas, idmks, nama, alamat) values (1, '52', 'pramipexole dihydrochloride', '99698 Anhalt Crossing');
insert into berkas (idberkas, idmks, nama, alamat) values (2, '79', 'Wingscale', '47587 Becker Hill');
insert into berkas (idberkas, idmks, nama, alamat) values (3, '78', 'Loperamide HCl', '47956 Memorial Alley');
insert into berkas (idberkas, idmks, nama, alamat) values (4, '01', 'Octinoxate and Titanium Dioxide', '51940 Lakewood Gardens Plaza');
insert into berkas (idberkas, idmks, nama, alamat) values (5, '01', 'PYRITHIONE ZINC', '0994 Michigan Alley');
insert into berkas (idberkas, idmks, nama, alamat) values (6, '05', 'Octinoxate and Titanium Dioxide', '9887 Dunning Road');
insert into berkas (idberkas, idmks, nama, alamat) values (7, '05', 'ALCOHOL', '06 Meadow Vale Crossing');
insert into berkas (idberkas, idmks, nama, alamat) values (8, '67', 'Pramoxine Hydrochloride', '70 Hansons Drive');
insert into berkas (idberkas, idmks, nama, alamat) values (9, '06', 'Radish', '3 Loeprich Center');
insert into berkas (idberkas, idmks, nama, alamat) values (10, '62', 'Benazepril Hydrochloride', '63048 Bay Trail');
insert into berkas (idberkas, idmks, nama, alamat) values (11, '03', 'Sodium Fluoride', '3464 Thompson Way');
insert into berkas (idberkas, idmks, nama, alamat) values (12, '64', 'Pyridoxine, Folic Acid, Calcium, and Ginger', '98507 Claremont Drive');
insert into berkas (idberkas, idmks, nama, alamat) values (13, '09', 'allopurinol', '267 Tennessee Alley');
insert into berkas (idberkas, idmks, nama, alamat) values (14, '09', 'benztropine mesylate', '9554 Mayfield Drive');
insert into berkas (idberkas, idmks, nama, alamat) values (15, '01', 'OCTINOXATE, TITANIUM DIOXIDE, and ZINC OXIDE', '798 Northview Trail');
insert into berkas (idberkas, idmks, nama, alamat) values (16, '76', 'Naproxen Sodium', '61 Johnson Center');
insert into berkas (idberkas, idmks, nama, alamat) values (17, '58', 'Silver Sulfadiazine', '1767 Golden Leaf Trail');
insert into berkas (idberkas, idmks, nama, alamat) values (18, '03', 'Tizanidine', '224 High Crossing Parkway');
insert into berkas (idberkas, idmks, nama, alamat) values (19, '74', 'Avobenzone , Octinoxate and Octisalate', '116 Lien Lane');
insert into berkas (idberkas, idmks, nama, alamat) values (20, '03', 'Bacitracin', '096 Jana Court');
insert into berkas (idberkas, idmks, nama, alamat) values (21, '02', 'Avobenzone, Octisalate, Octocrylene', '1 Dennis Parkway');
insert into berkas (idberkas, idmks, nama, alamat) values (22, '01', 'propranolol hydrochloride', '32 Jay Crossing');
insert into berkas (idberkas, idmks, nama, alamat) values (23, '05', 'TITANIUM DIOXIDE', '0 Dunning Trail');
insert into berkas (idberkas, idmks, nama, alamat) values (24, '53', 'Sorrel Mixture', '735 Oakridge Circle');
insert into berkas (idberkas, idmks, nama, alamat) values (25, '04', 'Methimazole', '729 Sutteridge Pass');
insert into berkas (idberkas, idmks, nama, alamat) values (26, '06', 'Glipizide', '07272 Straubel Place');
insert into berkas (idberkas, idmks, nama, alamat) values (27, '13', 'Arnica montana, Rhus toxicodendron, Phytolacca decandra, Pulsatilla, Ruta graveolens,', '6781 Fallview Park');
insert into berkas (idberkas, idmks, nama, alamat) values (28, '05', 'ACTIVATED CHARCOAL', '5 Green Street');
insert into berkas (idberkas, idmks, nama, alamat) values (29, '08', 'ulipristal acetate', '268 Browning Place');
insert into berkas (idberkas, idmks, nama, alamat) values (30, '01', 'Simethicone', '0 Welch Parkway');
insert into berkas (idberkas, idmks, nama, alamat) values (31, '07', 'DOBUTAMINE HYDROCHLORIDE', '3091 Killdeer Place');
insert into berkas (idberkas, idmks, nama, alamat) values (32, '64', 'BENZALKONIUM CHLORIDE', '6 Kennedy Alley');
insert into berkas (idberkas, idmks, nama, alamat) values (33, '68', 'Dextromethorphan Hydrobromide and Promethazine Hydrochloride', '2883 Crownhardt Hill');

insert into berkas (idberkas, idmks, nama, alamat) values (35, '01', 'Glyburide', '35232 Sunnyside Way');
insert into berkas (idberkas, idmks, nama, alamat) values (36, '05', 'balanced salt solution enriched with bicarbonate, dextrose, and glutathione', '773 Butternut Court');
insert into berkas (idberkas, idmks, nama, alamat) values (37, '52', 'Carvedilol', '7 Southridge Street');
insert into berkas (idberkas, idmks, nama, alamat) values (38, '75', 'Octinoxate', '2866 Ruskin Center');
insert into berkas (idberkas, idmks, nama, alamat) values (39, '09', 'DOCUSATE SODIUM', '08 Delaware Street');
insert into berkas (idberkas, idmks, nama, alamat) values (40, '71', 'Acetaminophen, Doxylamine Succinate, Phenylephrine Hydrochloride', '0724 Sycamore Place');
insert into berkas (idberkas, idmks, nama, alamat) values (41, '05', 'baclofen', '219 Dahle Hill');
insert into berkas (idberkas, idmks, nama, alamat) values (42, '64', 'Petrolatum, Zinc Oxide', '6 Everett Place');
insert into berkas (idberkas, idmks, nama, alamat) values (43, '74', 'GELSEMIUM SEMPERVIRENS ROOT', '49 Glendale Park');
insert into berkas (idberkas, idmks, nama, alamat) values (44, '78', 'Colloidal silver,', '0 Northview Avenue');
insert into berkas (idberkas, idmks, nama, alamat) values (45, '04', 'Octinoxate and Avobenzone', '5257 Mayfield Avenue');
insert into berkas (idberkas, idmks, nama, alamat) values (46, '07', 'Torsemide', '7 Sage Street');
insert into berkas (idberkas, idmks, nama, alamat) values (47, '74', 'thioridazine hydrochloride', '3349 Loomis Park');
insert into berkas (idberkas, idmks, nama, alamat) values (48, '04', 'Naproxen sodium', '5 Northview Lane');
insert into berkas (idberkas, idmks, nama, alamat) values (49, '70', 'Dextran 70, Polyethylene Glycol 400, Povidone, Tetrahydrozoline Hydrochloride', '5602 Russell Road');
insert into berkas (idberkas, idmks, nama, alamat) values (50, '05', 'Mirtazapine', '72 Schlimgen Plaza');
insert into berkas (idberkas, idmks, nama, alamat) values (51, '01', 'Triclosan', '50204 Kenwood Park');
insert into berkas (idberkas, idmks, nama, alamat) values (52, '05', 'Desoximetasone', '00041 Corben Street');
insert into berkas (idberkas, idmks, nama, alamat) values (53, '74', 'BENZALKONIUM CHLORIDE', '666 Marquette Place');
insert into berkas (idberkas, idmks, nama, alamat) values (54, '06', 'capsaicin', '2 Mandrake Park');
insert into berkas (idberkas, idmks, nama, alamat) values (55, '02', 'TITANIUM DIOXIDE, ZINC OXIDE', '0774 Waubesa Drive');
insert into berkas (idberkas, idmks, nama, alamat) values (56, '01', 'Quetiapine fumarate', '037 Delladonna Plaza');
insert into berkas (idberkas, idmks, nama, alamat) values (57, '03', 'Midazolam Hydrochloride', '309 Washington Court');
insert into berkas (idberkas, idmks, nama, alamat) values (58, '70', 'Pyrithione Zinc', '0180 Coolidge Lane');
insert into berkas (idberkas, idmks, nama, alamat) values (59, '04', 'DIPHENHYDRAMINE HYDROCHLORIDE', '6 Westend Lane');
insert into berkas (idberkas, idmks, nama, alamat) values (60, '54', 'Epinephrine', '95 Forster Plaza');
insert into berkas (idberkas, idmks, nama, alamat) values (61, '01', 'Propafenone Hydrochloride', '9 Manufacturers Parkway');
insert into berkas (idberkas, idmks, nama, alamat) values (62, '52', 'octreotide acetate', '1 Green Ridge Terrace');
insert into berkas (idberkas, idmks, nama, alamat) values (63, '79', 'benzalkonium chloride', '025 Oak Valley Point');
insert into berkas (idberkas, idmks, nama, alamat) values (64, '05', 'Oxygen', '58 Bartelt Hill');
insert into berkas (idberkas, idmks, nama, alamat) values (65, '70', 'candida albicans', '71188 Petterle Park');
insert into berkas (idberkas, idmks, nama, alamat) values (66, '09', 'Diltiazem Hydrochloride', '244 Lillian Avenue');
insert into berkas (idberkas, idmks, nama, alamat) values (67, '07', 'Octinoxate and Titanium dioxide', '10784 Sheridan Road');
insert into berkas (idberkas, idmks, nama, alamat) values (68, '77', 'Miconazole nitrate', '14286 Mitchell Hill');
insert into berkas (idberkas, idmks, nama, alamat) values (69, '71', 'amlodipine valsartan and hydrochlorothiazide', '52 Autumn Leaf Plaza');
insert into berkas (idberkas, idmks, nama, alamat) values (70, '75', 'Bacitracin Zinc and Polymyxin B Sulfate', '016 Ramsey Plaza');
insert into berkas (idberkas, idmks, nama, alamat) values (71, '06', 'Butalbital, Aspirin and Caffeine', '3 Ronald Regan Street');
insert into berkas (idberkas, idmks, nama, alamat) values (72, '02', 'METOPROLOL TARTRATE', '78638 Dapin Park');
insert into berkas (idberkas, idmks, nama, alamat) values (73, '61', 'levofloxacin', '4 Farwell Park');
insert into berkas (idberkas, idmks, nama, alamat) values (74, '09', 'Furosemide', '7 Sage Circle');
insert into berkas (idberkas, idmks, nama, alamat) values (75, '03', 'loratadine and pseudoephedrine', '77226 Bunting Hill');
insert into berkas (idberkas, idmks, nama, alamat) values (76, '72', 'sumatriptan and naproxen sodium', '673 Chinook Parkway');
insert into berkas (idberkas, idmks, nama, alamat) values (77, '67', 'Fluconazole', '96 Westridge Lane');
insert into berkas (idberkas, idmks, nama, alamat) values (78, '06', 'SELENIUM SULFIDE', '3216 Kennedy Hill');
insert into berkas (idberkas, idmks, nama, alamat) values (79, '04', 'Hand Sanitizer Wipes', '29 Reinke Lane');
insert into berkas (idberkas, idmks, nama, alamat) values (80, '77', 'Oxygen', '6360 Del Sol Parkway');
insert into berkas (idberkas, idmks, nama, alamat) values (81, '01', 'gentamicin sulfate', '18 Northland Plaza');
insert into berkas (idberkas, idmks, nama, alamat) values (82, '01', 'Ceftriaxone Sodium', '695 Spenser Pass');
insert into berkas (idberkas, idmks, nama, alamat) values (83, '44', 'White Kidney Beans', '283 Kensington Park');
insert into berkas (idberkas, idmks, nama, alamat) values (84, '02', 'Zolpidem Tartrate', '0784 Hermina Lane');
insert into berkas (idberkas, idmks, nama, alamat) values (85, '03', 'propionibacterium acnes', '07449 Ruskin Junction');
insert into berkas (idberkas, idmks, nama, alamat) values (86, '78', 'Menthol', '3506 Westridge Street');
insert into berkas (idberkas, idmks, nama, alamat) values (87, '79', 'ALCOHOL', '011 Manufacturers Circle');
insert into berkas (idberkas, idmks, nama, alamat) values (88, '77', 'Topiramate', '55527 Westerfield Way');
insert into berkas (idberkas, idmks, nama, alamat) values (89, '04', 'SELENIUM SULFIDE', '54047 2nd Center');
insert into berkas (idberkas, idmks, nama, alamat) values (90, '05', 'Octinoxate and Oxybenzone', '40106 Cottonwood Avenue');
insert into berkas (idberkas, idmks, nama, alamat) values (91, '78', 'benzoyl peroxide', '52 Bunker Hill Way');
insert into berkas (idberkas, idmks, nama, alamat) values (92, '43', 'OCTINOXATE, ENSULIZOLE, OCTOCRYLENE, TITANIUM DIOXIDE', '1 Fairfield Terrace');
insert into berkas (idberkas, idmks, nama, alamat) values (93, '55', 'OTC SKIN PROTECTANT DRUG PRODUCTS', '540 Messerschmidt Park');
insert into berkas (idberkas, idmks, nama, alamat) values (94, '02', 'Metformin', '56525 Holmberg Court');
insert into berkas (idberkas, idmks, nama, alamat) values (95, '09', 'Octinoxate, Octisalate, Zinc Oxide, Titanium Dioxide', '2709 Haas Avenue');
insert into berkas (idberkas, idmks, nama, alamat) values (96, '60', 'Ethyl Alcohol', '0 Mendota Terrace');
insert into berkas (idberkas, idmks, nama, alamat) values (97, '07', 'AMANTADINE HYDROCHLORIDE', '138 Delaware Junction');
insert into berkas (idberkas, idmks, nama, alamat) values (98, '05', 'Etodolac', '62 Oak Valley Point');
insert into berkas (idberkas, idmks, nama, alamat) values (99, '03', 'phenytoin sodium', '530 Melvin Terrace');
insert into berkas (idberkas, idmks, nama, alamat) values (100, '05', 'MERCURIUS SOLUBILIS', '5 Hayes Plaza');

insert into berkas (idberkas, idmks, nama, alamat) values (34, '03', 'Hydrastis canadensis, Lac defloratum, Natrum muriaticum, Sepia', '4 Atwood Center');
-- END BERKAS DATA --

-- TRIGGER AND STORED PROCEDURE --
create or replace function izinMajuSidangCheck() returns TRIGGER
AS $$
DECLARE
	target_date date;
BEGIN
	select t.tanggal into target_date
		from timeline t, mata_kuliah_spesial mks
		where old.tahun = t.tahun and old.semester = t.semester
		and t.namaevent = 'Pemberian izin maju sidang oleh pembimbing';
	IF ((current_date > target_date) and (old.ijinmajusidang <> new.ijinmajusidang)) THEN
			BEGIN
				RAISE EXCEPTION 'submission is overdue';
				RETURN NULL;
			END;
    END IF;
  RETURN NEW;
END; $$ language 'plpgsql';

create TRIGGER izinMajuSidang_trigger
	before insert or update on mata_kuliah_spesial
	for each row execute procedure izinMajuSidangCheck();

create or replace function siapSidangCheck() returns TRIGGER
AS $$
DECLARE
	mks record;
BEGIN
	for mks in select *
		from mata_kuliah_spesial
		where issiapsidang = false
	LOOP
		IF ((mks.pengumpulanhardcopy = true) AND (mks.ijinmajusidang = true)) THEN
			BEGIN
				UPDATE mata_kuliah_spesial
				SET issiapsidang = true
				WHERE idmks = mks.idmks;
				RAISE NOTICE '%', mks.idmks;
			END;
    END IF;
	END LOOP;
	RETURN NULL;
END; $$ language 'plpgsql';

create TRIGGER siapSidang_trigger
	after insert or update on mata_kuliah_spesial
	for each row execute procedure siapSidangCheck();
