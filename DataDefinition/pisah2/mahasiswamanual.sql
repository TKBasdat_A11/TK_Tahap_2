/*Formatnya 

IDMKS			INT UNIQUE,
	NPM				CHAR(10),
	Tahun			INT,
	Semester		INT,
	Judul			VARCHAR(250)	NOT NULL,
	IsSiapSidang	Boolean			DEFAULT FALSE,
	PengumpulanHardCopy		Boolean		DEFAULT FALSE,
	IjinMajuSidang		Boolean		DEFAULT FALSE,
	IdJenisMKS		INT NOT NULL,
*/

INSERT INTO MATA_KULIAH_SPESIAL VALUES (1, '1506724505',2015,2, 'Nullam porttitor lacus at turpis', FALSE, FALSE, FALSE, 1);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (2, '1506688752',2016,2, 'Maecenas tristique, est et tempus semper', FALSE, FALSE, FALSE, 3);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (3, '1506688834',2016,2, 'Sed sagittis. Nam congue, risus semper porta volutpat', FALSE, FALSE, FALSE, 4);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (4, '1506688765',2016,2, 'Morbi porttitor lorem id ligula.', FALSE, FALSE, FALSE, 2);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (5, '1506688872',2016,2, 'Suspendisse ornare consequat lectus.', FALSE, FALSE, FALSE, 6);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (6, '1506688986',2016,2, 'Est risus, auctor sed, tristique in, tempus sit amet, sem.', FALSE, FALSE, FALSE, 7);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (7, '1506689194',2016,2, 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', FALSE, FALSE, FALSE, 2);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (8, '1506689345',2016,2, 'Duis aliquam convallis nunc.', FALSE, FALSE, FALSE, 6);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (9, '1506689396',2016,2, 'Proin at turpis a pede posuere nonummy.', FALSE, FALSE, FALSE, 7);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (10, '1506689452',2016,2, 'Aenean fermentum. Donec ut mauris eget massa tempor convallis.', FALSE, FALSE, FALSE, 3);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (11, '1506689490',2016,2, 'Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', FALSE, FALSE, FALSE, 4);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (12, '1506689585',2016,2, 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla.', FALSE, FALSE, FALSE, 5);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (13, '1506689641',2016,2, 'Sed accumsan felis.', FALSE, FALSE, FALSE, 1);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (14, '1506690391',2016,2, 'Ut at dolor quis odio consequat varius.', FALSE, FALSE, FALSE, 5);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (15, '1506690460',2016,2, 'Morbi non lectus.', FALSE, FALSE, FALSE, 2);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (16, '1506721794',2016,2, 'Nulla ut erat id mauris vulputate elementum.', FALSE, FALSE, FALSE, 1);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (17, '1506722746',2016,2, 'Phasellus in felis. Donec semper sapien a libero.', FALSE, FALSE, FALSE, 5);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (18, '1506722752',2016,2, 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim.', FALSE, FALSE, FALSE, 5);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (19, '1506722765',2016,2, 'Nullam porttitor lacus at turpis.', FALSE, FALSE, FALSE, 4);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (20, '1506722771',2016,2, 'Donec posuere metus vitae ipsum.', FALSE, FALSE, FALSE, 2);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (21, '1506722821',2016,2, 'Integer ac leo.', FALSE, FALSE, FALSE, 2);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (22, '1506722840',2016,2, 'In congue. Etiam justo. Etiam pretium iaculis justo.', FALSE, FALSE, FALSE, 6);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (23, '1506722853',2016,2, 'In hac habitasse platea dictumst.', FALSE, FALSE, FALSE, 6);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (24, '1506722866',2016,2, 'Morbi vestibulum, velit id pretium iaculis', FALSE, FALSE, FALSE, 6);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (25, '1506722872',2016,2, 'Nullam porttitor lacus at turpis.', FALSE, FALSE, FALSE, 5);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (26, '1506723572',2016,2, 'Etiam vel augue.', FALSE, FALSE, FALSE, 7);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (27, '1506724354',2016,2, 'Duis consequat dui nec nisi volutpat eleifend.', FALSE, FALSE, FALSE, 5);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (28, '1506724606',2016,2, 'Phasellus in felis.', FALSE, FALSE, FALSE, 3);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (29, '1506725003',2016,2, 'Cras in purus eu magna vulputate luctus.', FALSE, FALSE, FALSE, 3);
INSERT INTO MATA_KULIAH_SPESIAL VALUES (30, '1506725041',2016,2, 'Turpis enim blandit mi, in porttitor pede justo eu massa.', FALSE, FALSE, FALSE, 4);







INSERT INTO MAHASISWA VALUES ('1506724505', 'Fersandi Pratama', 'fersandi', 'tralalala', 'fersandi.pratama@ui.ac.id', 'fersandi_pratama@rocketmail.com', '081367173222', '0711410289');
INSERT INTO MAHASISWA VALUES ('1506688752', 'Aji Imawan Omi', 'aji52', 'aaabbcc', 'aji.imawan@ui.ac.id', NULL, '081323432431', '02132421231');
INSERT INTO MAHASISWA VALUES ('1506688765', 'Alif Fadila Safriyanto', 'alif65', 'dawqews', 'alif.fadila@ui.ac.id', NULL, '0854231321', '0213132131');
INSERT INTO MAHASISWA VALUES ('1506688834', 'Adityo Anggraito', 'adityo34', 'adsaswqe', 'adityo.anggraito@ui.ac.id', 'adityo@gmail.com', '08123646', '021437989');
INSERT INTO MAHASISWA VALUES ('1506688872', 'Vitosavero Avila Wibisono', 'Vitosavero72', 'dsaq3oiu', 'vitosavero.avila@ui.ac.id', 'vito_avila@yahoo.co.id', '08434234324', '0219382429');
INSERT INTO MAHASISWA VALUES ('1506688986', 'Andri Nur Rochman', 'andri86', 'skjhq4o3u23', 'andri.nur@ui.ac.id', 'andri86@live.net', '0832132341', '021243243');
INSERT INTO MAHASISWA VALUES ('1506689194', 'Nurhaya Kushadi Gitasari', 'nurhaya94', 'K*CSYsd', 'nurhaya.kushadi@ui.ac.id', 'nurhaya_kus@yahoo.com', '085364237', '0215430953');
INSERT INTO MAHASISWA VALUES ('1506689345', 'Bella Nadhifah Agustina', 'bella45', 'bbeellllaa', 'bella.nadhifah', '', '', '');
INSERT INTO MAHASISWA VALUES ('1506689396', 'Ivone Dona Khoirun Nisa', 'ivone96', 'qwerty', 'ivone.dona@ui.ac.id', NULL, '08934543222', NULL);
INSERT INTO MAHASISWA VALUES ('1506689452', 'Asyraf Ahmad Nadhil', 'asyraf52', 'asddfgh', 'asyraf.ahmad@ui.ac.id', 'asyrah@gmail.com', NULL, NULL);
INSERT INTO MAHASISWA VALUES ('1506689490', 'Enrico Jano Fiandi', 'enrico90', 'qwertyuio', 'enrico.jano@ui.ac.id', 'enrico.jano@gmail.com', '08984324212', NULL);
INSERT INTO MAHASISWA VALUES ('1506689585', 'Maktal Sakriadhi', 'maktal85', 'zxcvbfdsa', 'maktal51@ui.ac.id', 'maktal@yahoo.com', NULL, '0212312');
INSERT INTO MAHASISWA VALUES ('1506689641', 'Rizkah Shalihah', 'rizkah41', 'poiurerw', 'rizkah.shalihah@ui.ac.id', 'rizkahSha@gmail.com', '087312334', '021435432');
INSERT INTO MAHASISWA VALUES ('1506690391', 'Ibrahim', 'ibrahim91', 'AODsjdq203', 'ibrahim51@ui.ac.id', 'ibamGaring@gmail.com', '08213211', '021332423');
INSERT INTO MAHASISWA VALUES ('1506690460', 'Jiwo Nusantara Adji', 'jiwo60', 'llfjdkLQq3l', 'jiwo.nusantara@ui.ac.id', 'jiwo60@facebook.com', '0823123432', '02143521165');
INSERT INTO MAHASISWA VALUES ('1506721794', 'Ahmad Faqih', 'ahmad94', '213wzxc', 'ahmad.faqih@ui.ac.id', NULL, NULL, NULL);
INSERT INTO MAHASISWA VALUES ('1506722746', 'Muhammad Farhan', 'farham46', '31332rewds', 'muhammad.farhan@ui.ac.id', 'farhan51@yahoo.com', NULL, '0213343221');
INSERT INTO MAHASISWA VALUES ('1506722752', 'Ahmad Yazid', 'yazid52', 'pqwkd3q90', 'ahmad.yazid@ui.ac.id', NULL, '084132133231', '0213242341');
INSERT INTO MAHASISWA VALUES ('1506722765', 'Jordan Muhammad Andrianda', 'jordan65', 'ln31wOISA#', 'jordan.muhammad@ui.ac.id', 'jordan_andrianda@yahoo.com', '0832143223', '023212343');
INSERT INTO MAHASISWA VALUES ('1506722771', 'Muhammad Faiz', 'faiz71', 'fdsf343w', 'muhammad.faiz@ui.ac.id', 'faiz71@facebook.com', '08432123341', '02132415');
INSERT INTO MAHASISWA VALUES ('1506722821', 'Muhammad Givari Arnanda', 'gipaw21', 'sdofjoi32', 'muhammad.givari@ui.ac.id', 'givari51@gmail.com', '084312353232', '021323212');
INSERT INTO MAHASISWA VALUES ('1506722840', 'Muhammad Ramadhan', 'ramadhan40', 'qwe32ewIU', 'muhammad.ramadhan@ui.ac.id', NULL, '08312432213', NULL);
INSERT INTO MAHASISWA VALUES ('1506722853', 'Maula Faiz Adli', 'maula53', 'QwErTy', 'maula.faiz@ui.ac.id', 'molamoli@fakedomain.com', '0832134345', '021324312');
INSERT INTO MAHASISWA VALUES ('1506722866', 'Hizkia Nugroho', 'hizkia66', 'PAS93S3w', 'hizkia.nugroho@ui.ac.id', 'hizkia.nugroho@gmail.com', NULL, NULL);
INSERT INTO MAHASISWA VALUES ('1506722872', 'Arfi Renaldi', 'arfi72', 'AS32ASDw', 'arfi.renaldi@ui.ac.id', 'arfi_renaldi@rocketmail.com', '0812345312', '043212543');
INSERT INTO MAHASISWA VALUES ('1506723572', 'Muhammad Faisal Hardin Rangkuti', 'faisal72', 'ASQ2iwe3S', 'muhammad.faisal72@ui.ac.id', 'faisal_hardin@gmail.com', NULL, NULL);
INSERT INTO MAHASISWA VALUES ('1506724354', 'Valian Fil Ahli', 'valian54', ':K)#E:LD', 'valian.fil@ui.ac.id', 'valianahli@facebook.com', '0821324312', '02198443');
INSERT INTO MAHASISWA VALUES ('1506724606', 'Oksa Akbar Runa', 'oksa06', ')(asiU)', 'oksa.akbar@ui.ac.id', NULL, NULL, NULL);
INSERT INTO MAHASISWA VALUES ('1506725003', 'William Adjandra Hogan', 'william03', 'ASD309LDS', 'william.adjandra@ui.ac.id', 'williamhogan@yahoo.co.id', '0895495343', '0214366445');
INSERT INTO MAHASISWA VALUES ('1506725041', 'Jonathan Prasetya W.', 'jonathan41', 'ADS(#EWDSF', 'jonathan.Prasetya@ui.ac.id', 'jonathan.Prasetya@gmail.com', '0821343221', '021324235');
INSERT INTO MAHASISWA VALUES ('1506728453', 'Faqrul Anshurulloh', 'faqrul53', '#ewrw43324', 'faqrul.andhurulloh@ui.ac.id', 'faqrul51@facebook.com', NULL, '02132423');
INSERT INTO MAHASISWA VALUES ('1506730350', 'Ricky Putra Nursalim', 'ricky50', 'laskjdewq', 'ricky.putra@ui.ac.id', 'ricky.putra@gmail.com', '083124434', '0214353412');
INSERT INTO MAHASISWA VALUES ('1506731580', 'Farah Agia Ramadhina', 'farah80', '32rwfdsr23', 'farah.agia@ui.ac.id', 'farah.agia@gmail.com', '081234322', '021432143');
INSERT INTO MAHASISWA VALUES ('1506737666', 'Ismail Shalih Abdul Kholiq', 'ismail66', 'dslfJ#(ED3', 'ismail.shalih@ui.ac.id', NULL, '082314533', '02143243123');
INSERT INTO MAHASISWA VALUES ('1506738492', 'Muhammad Azri Renandi P. S', 'azri92', 'sai322', 'muhammad.azri@ui.ac.id', 'muhammad.azri@gmail.com', '0823143333', '02133543');
INSERT INTO MAHASISWA VALUES ('1506757440', 'Julio Jaya Handoyo', 'julio40', 'das231jk21', 'julio.jaya@gmail.com', 'julio.jaya@gmail.com', '0821343123', '0214323565');
INSERT INTO MAHASISWA VALUES ('1506757453', 'Joshua Valdamer Ghibran', 'joshua53', 'DSALJEOI', 'joshua.valdamer@ui.ac.id', NULL, '08213321343', '02164523');
INSERT INTO MAHASISWA VALUES ('1506757472', 'Kenny Reida Dharmawan', 'kenny72', 'SAIJ3o2ie', 'kenny.reida@ui.ac.id', 'kenny.reida@gmail.com', NULL, '082134312');
INSERT INTO MAHASISWA VALUES ('1506757522', 'Joachim Rachmad', 'joachim22', 'ASD@WIOE#', 'joachim.rachmad@ui.ac.id', 'joachim51@yahoo.com', NULL, '081234331');