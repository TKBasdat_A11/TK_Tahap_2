insert into timeline (idtimeline, namaevent, tanggal, tahun, semester) values (1, 'Pemberian izin maju sidang oleh pembimbing', '2015-12-10', '2015', '2');
insert into timeline (idtimeline, namaevent, tanggal, tahun, semester) values (2, 'Pengisian jadwal pribadi oleh dosen', '2015-11-20', '2015', '2');
insert into timeline (idtimeline, namaevent, tanggal, tahun, semester) values (3, 'Berkas sidang', '2015-10-29', '2015', '2');

insert into timeline (idtimeline, namaevent, tanggal, tahun, semester) values (4, 'Pemberian izin maju sidang oleh pembimbing', '2016-04-09', '2016', '1');
insert into timeline (idtimeline, namaevent, tanggal, tahun, semester) values (5, 'Pengisian jadwal pribadi oleh dosen', '2016-05-19', '2016', '1');
insert into timeline (idtimeline, namaevent, tanggal, tahun, semester) values (6, 'Berkas sidang', '2016-06-28', '2016', '1');

insert into timeline (idtimeline, namaevent, tanggal, tahun, semester) values (7, 'Pemberian izin maju sidang oleh pembimbing', '2016-12-8', '2016', '2');
insert into timeline (idtimeline, namaevent, tanggal, tahun, semester) values (8, 'Pengisian jadwal pribadi oleh dosen', '2016-11-18', '2016', '2');
insert into timeline (idtimeline, namaevent, tanggal, tahun, semester) values (9, 'Berkas sidang', '2016-10-27', '2016', '2');