<?php
	session_start();
	if(count($_POST) == 0){
		header('Location: login.php');
	}
	extract($_POST);
	include 'config.php';

	//Check if Admin
	if ($username == 'admin' && $password == 'password'){
		$_SESSION['auth'] = true;
		$_SESSION['role'] = 'admin';
		$_SESSION['name'] = 'admin';
		header('Location: index.php');
		exit();
	}
	//Check if Dosen
	$dosen = pg_query_params($db, "SELECT nip,nama,username,password FROM DOSEN where username=$1", array($username));
	if ($dosen && pg_num_rows($dosen) > 0){
		$row = pg_fetch_assoc($dosen);
		if ($row['password'] == $password){
			$_SESSION['auth'] = true;
			$_SESSION['role'] = 'dosen';
			$_SESSION['name'] = $row['nama'];
			$_SESSION['number_id'] = $row['nip'];
			header('Location: index.php');
			exit();
		}
	}
	//Check if Mahasiswa
	$mahasiswa = pg_query_params($db, "SELECT npm,nama,username,password FROM MAHASISWA where username=$1", array($username));
	if ($mahasiswa && pg_num_rows($mahasiswa) > 0){
		$row = pg_fetch_assoc($mahasiswa);
		if ($row['password'] == $password){
			$_SESSION['auth'] = true;
			$_SESSION['role'] = 'mahasiswa';
			$_SESSION['name'] = $row['nama'];
			$_SESSION['number_id'] = $row['npm'];
			header('Location: index.php');
			exit();
		}
	}
	$_SESSION['error'] = 'Username atau password salah';
	header('Location: login.php');
