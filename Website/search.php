<?php
	session_start();
	if (!isset($_SESSION['auth'])){
		header('Location: login.php');
		exit();
	}
	if ($_SESSION['role'] != 'admin'){
		header('Location: index.php');
		exit();
	}

	include "config.php";
	include "models/jadwalSidang.php";

	if(count($_GET) < 2){
		header("Location: index");
		exit();
	}
	if ($_GET['type'] == "jadwal"){
		$semester = 0;
		$tahun = 0;
		if ($_GET['term'] == '1'){
			$semester = 2;
			$tahun = 2015;
		} else if ($_GET['term'] == '2'){
			$semester = 1;
			$tahun = 2016;
		} else if ($_GET['term'] == '3'){
			$semester = 2;
			$tahun = 2016;
		} 
		$jadwals = searchByJenis($db, $_GET['jenis'], $semester, $tahun);
	} else if ($_GET['type'] == "mahasiswa"){
		$jadwals = searchByNama($db, $_GET['name']);
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Search | SISIDANG</title>
	<!--Import Google Icon Font-->
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="src/css/materialize.min.css"  media="screen,projection"/>
	<!--Import Google Slabo Font-->
	<link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="src/css/style.css"  media="screen,projection"/>
	<script type="text/javascript" src="src/js/jquery-3.1.1.js"></script>
	<script type="text/javascript" src="src/js/materialize.js"></script>        
	<script type="text/javascript" src="src/js/jquery.js"></script>

	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body class= "grey lighten-1">
		<?php include "views/navbar.php"; ?>
		<div id ="aboutus" class="section grey lighten-1">
			<div class="row container">
			<h2 class="white-text header" style="border-bottom: 2px solid black; padding-bottom: 5px; width: auto">Hasil Pencarian</h2>
			</div>
			<div class="container">
				<a class="waves-effect waves-teal btn-flat" href="beranda.php">Kembali</a>
			</div>
		</div>
		<div id ="aboutus" class="section grey lighten-1">
			<div class="row container highlight">
				<table class="striped centered">
					<thead>
						<tr>
							<th data-field="id">Jenis Sidang</th>
							<th data-field="name">Mahasiswa</th>
							<th data-field="price">Judul</th>

							<th data-field="id">Dosen Pembimbing</th>
							<th data-field="name">Dosen Penguji</th>
							<th data-field="price">Waktu dan Lokasi</th>

							<th data-field="price">Action</th>
						</tr>
					</thead>

					<tbody>
						<?php while($row = pg_fetch_assoc($jadwals)): ?>
							<tr>
								<td><?= $row['namamks']?></td>
								<td><?= $row['nama']?></td>
								<td><?= $row['judul']?></td>
								<td>
									<?php 
									$dosBing = retrieveDosenPembimbing($db, $row['idmks']);
									$lstDosen = '';
									while($dosen = pg_fetch_assoc($dosBing)){
										$lstDosen = $lstDosen.$dosen['nama'].', ';
									}
									if ($lstDosen != ''){
										$lstDosen = substr($lstDosen, 0, strlen($lstDosen)-2);
									}
									echo $lstDosen;
									?>
								</td>
								<td>
									<?php 
									$dosPeng = retrieveDosenPenguji($db, $row['idmks']);
									$lstDosen = '';
									while($dosen = pg_fetch_assoc($dosPeng)){
										$lstDosen = $lstDosen.$dosen['nama'].', ';
									}
									if ($lstDosen != ''){
										$lstDosen = substr($lstDosen, 0, strlen($lstDosen)-2);
									}
									echo $lstDosen;
									?>
								</td>
								<td>
									<?= $row['tanggal'].' '.$row['jammulai'].'-'.$row['jamselesai']?>
								</td>
								<td><button class="btn black">Edit</button></td>
							</tr>
						<?php endwhile; ?>
					</tbody>
				</table>
			</div>
		</div>

		<footer class="page-footer grey darken-3">
			<div class="container">
				<div class="row">
					<div class="col l6 s12">
						<h5 class="white-text">SISIDANG</h5>
						<p class="grey-text text-lighten-4">Website ini dibuat untuk memenuhi tugas akhir mata kuliah Basis Data Gasal 2016/2017</p>
					</div>
					<div class="col l4 offset-l2 s12">
						<h5 class="white-text">Links</h5>
						<ul>
							<li><a class="grey-text text-lighten-3" href="#!">Sumber</a></li>  
						</ul>
					</div>
				</div>
			</div>
			<div class="footer-copyright" >
				<div class="container">
					© 2016 Created by Kelompok T11 Basdat A
				</div>
			</div>
		</footer>
</body>
</html>