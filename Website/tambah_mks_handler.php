<?php
	include("config.php");
	$response = ['data' => null];

	if($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['action'])) {
		switch($_GET['action']) {
			case 'get_term':
				$termList = getTerm($db);
				$data = pg_fetch_all($termList);
				$response['data'] = $data;
				break;
			case 'get_jenis':
				$jenisList = getJenis($db);
				$data = pg_fetch_all($jenisList);
				$response['data'] = $data;
				break;
			case 'get_mahasiswa':
				$mahasiswaList = getMahasiswa($db);
				$data = pg_fetch_all($mahasiswaList);
				$response['data'] = $data;
				break;
			case 'get_pb':
				$pembimbingList = getPb($db);
				$data = pg_fetch_all($pembimbingList);
				$response['data'] = $data;
				break;
			case 'get_pj':
				$pengujiList = getPj($db);
				$data = pg_fetch_all($pengujiList);
				$response['data'] = $data;
				break;
		}
	}
	else if($_SERVER['REQUEST_METHOD'] === 'POST') {
			$npm = $_POST['select_mahasiswa'];
			$splitted = explode("n",$_POST['select_term']);
			$tahun = $splitted[0];
			$semester = $splitted[1];
			$jenis = $_POST['select_jenis'];
			$judul = pg_escape_string($_POST['judul']);
			$id = simpan($db,$npm,$tahun,$semester,$judul,$jenis);
			$pb1 = $_POST['select_pb1'];
			$checker = array();
			array_push($checker,$pb1);
			echo $pb1;
			if($_POST['select_pb2'] != "" && !in_array($_POST['select_pb2'],$checker)) {
				array_push($checker,$_POST['select_pb2']);
			}
			if($_POST['select_pb3'] != "" && !in_array($_POST['select_pb3'],$checker)) {
				array_push($checker,$_POST['select_pb3']);
			}
			for($i = 0; $i < count($checker); $i++){
				echo $checker[$i];
				setPb($db,$id,$checker[$i]);
			}

			$checker = array();
			$pj1 = $_POST['select_pj1'];
			array_push($checker,$pj1);
			if(isset($_POST['select_pj2']) && $_POST['select_pj2'] != "" && !in_array($_POST['select_pj2'],$checker)) {
				array_push($checker,$_POST['select_pj2']);
			}
			if(isset($_POST['select_pj3']) && $_POST['select_pj3'] != "" && !in_array($_POST['select_pj3'],$checker)) {
				array_push($checker,$_POST['select_pj3']);
			}
			for($i = 0; $i < count($checker); $i++){
				setPj($db,$id,$checker[$i]);
			}
			header('Location: tambah_mks.php');
	}

	function getTerm($db) {
		$list = pg_query($db, "SELECT * FROM TERM");
		return $list;
	}

	function getJenis($db) {
	 	$list = pg_query($db, "SELECT * FROM JENISMKS");
	 	return $list;
	}

	function getMahasiswa($db) {
	 	$list = pg_query($db, "SELECT npm,nama FROM MAHASISWA");
	 	return $list;
	}

	function getPb($db) {
		$list = pg_query($db, "SELECT nip,nama FROM DOSEN");
		return $list;
	}

	function getPj($db) {
		$list = pg_query($db, "SELECT DISTINCT nipsaranpenguji,nama FROM DOSEN,SARAN_DOSEN_PENGUJI WHERE nip=nipsaranpenguji");
		return $list;
	}

	function simpan($db,$npm,$tahun,$semester,$judul,$jenis) {
		$idmks = pg_query($db, "SELECT MAX(idmks) AS jumlah FROM MATA_KULIAH_SPESIAL");
		$temp = pg_fetch_result($idmks,"jumlah");
		$temp = ((int)$temp)+1;
		pg_query($db,"INSERT INTO Mata_Kuliah_Spesial VALUES ($temp,$npm,$tahun,$semester,'$judul',FALSE,FALSE,FALSE,$jenis)");
		return $temp;
	}

	function setPb($db,$idmks,$nipdosenpembimbing) {
		pg_query($db,"INSERT INTO DOSEN_PEMBIMBING(idmks,nipdosenpembimbing) VALUES ($idmks,$nipdosenpembimbing)");
	}

	function setPj($db,$idmks,$nipdosenpenguji) {
		pg_query($db,"INSERT INTO DOSEN_PENGUJI(idmks,nipdosenpenguji) VALUES ($idmks,$nipdosenpenguji)");
	}

	echo json_encode($response);
?>
