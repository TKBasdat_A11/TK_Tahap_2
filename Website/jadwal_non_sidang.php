<?php
session_start();

if (!isset($_SESSION['auth']) || ($_SESSION['auth'] = false)){
    header('Location: login.php');
}

include 'config.php';

if ($_SESSION['role'] == 'mahasiswa') {
    header('Location: index.php');
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Jadwal non Sidang | SISIDANG</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="src/css/materialize.min.css"  media="screen,projection"/>
    <!--Import Google Slabo Font-->
    <link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="src/css/style.css"  media="screen,projection"/>
    <script type="text/javascript" src="src/js/jquery-3.1.1.js"></script>
    <script type="text/javascript" src="src/js/materialize.js"></script>
    <script type="text/javascript" src="src/js/jquery.js"></script>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body class= "grey lighten-1">
<?php include "views/navbar.php"; ?>
<div id ="aboutus" class="section grey lighten-1">
    <div class="row container highlight">
        <h2>Jadwal Non-Sidang</h2>
        <div class="sort col 6">
            <form action = "tambah_jadwal_non_sidang.php" method = "post" class="col 2">
                <button name="tambah" class="btn black">TAMBAH</button>
            </form>
        </div>
        <table class="stripped centered">
            <thead>
            <tr>
                <th>Tanggal Mulai</th>
                <th>Tanggal Selesai</th>
                <th>Keterangan</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if ($_SESSION['role'] == 'admin') {
                $jadwal_non_sidang = pg_query($db, "select tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen from jadwal_non_sidang");
                while ($row = pg_fetch_row($jadwal_non_sidang)) {
                    echo '<tr>';
                    echo '<td>' . $row[0] . '</td>';
                    echo '<td>' . $row[1] . '</td>';
                    echo '<td>' . $row[2] . '</td>';
                    echo '<td><a href="#">EDIT</a></td>';
                    echo '</tr>';
                }
            }
            if ($_SESSION['role'] == 'dosen'){
                $id = $_SESSION['number_id'];
                $jadwal_non_sidang = pg_query($db, "select tanggalmulai, tanggalselesai, alasan, repetisi, nipdosen from jadwal_non_sidang where nipdosen = '$id'");
                while ($row = pg_fetch_row($jadwal_non_sidang)) {
                    echo '<tr>';
                    echo '<td>' . $row[0] . '</td>';
                    echo '<td>' . $row[1] . '</td>';
                    echo '<td>' . $row[2] . '</td>';
                    echo '<td><a href="#">EDIT</a></td>';
                    echo '</tr>';
                }
            }
            ?>
            </tbody>
        </table>
    </div>
</div>
<center><ul class="pagination container">
        <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
        <li class="active"><a href="#!">1</a></li>
        <li class="waves-effect"><a href="#!">2</a></li>
        <li class="waves-effect"><a href="#!">3</a></li>
        <li class="waves-effect"><a href="#!">4</a></li>
        <li class="waves-effect"><a href="#!">5</a></li>
        <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
    </ul></center>

<footer class="page-footer grey darken-3">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5 class="white-text">SISIDANG</h5>
                <p class="grey-text text-lighten-4">Website ini dibuat untuk memenuhi tugas akhir mata kuliah Basis Data Gasal 2016/2017</p>
            </div>
            <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Links</h5>
                <ul>
                    <li><a class="grey-text text-lighten-3" href="#!">Sumber</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright" >
        <div class="container">
            © 2016 Created by Kelompok T11 Basdat A
        </div>
    </div>
</footer>
</body>
</html>