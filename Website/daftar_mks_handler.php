<?php
  include "config.php";

  if($_SERVER['REQUEST_METHOD'] == "GET") {
    if($_GET['role']=='dosen') {
      if(isset($_GET['order'])) {
        $mksList = get($db,$_GET['order'],$_GET['nip']);
        $data = pg_fetch_all($mksList);
      } else {
        $mksList = get($db,"none",$_GET['nip']);
        $data = pg_fetch_all($mksList);
      }
    } else {
      if(isset($_GET['order'])) {
        $mksList = get($db,$_GET['order']);
        $data = pg_fetch_all($mksList);
      } else {
        $mksList = get($db);
        $data = pg_fetch_all($mksList);
      }
    }
    echo json_encode($data);
  }

  function get($db, $order = null, $nip = null) {
    if(null == $order || "none" == $order)
      $sort = "Mata_Kuliah_Spesial.idmks";
    elseif($order == 'mahasiswa')
      $sort = "MAHASISWA.nama";
    elseif($order == 'jenis')
      $sort = "JENISMKS.namamks";
    else
      $sort = "Mata_Kuliah_Spesial.tahun, Mata_Kuliah_Spesial.semester";

    if(null == $nip) {
      $list = pg_query($db, "SELECT Mata_Kuliah_Spesial.*,Mahasiswa.nama,JENISMKS.namamks
                             FROM Mata_Kuliah_Spesial,MAHASISWA,JENISMKS
                             WHERE Mata_Kuliah_Spesial.idjenismks = JENISMKS.id AND Mata_Kuliah_Spesial.npm = MAHASISWA.npm
                             ORDER BY ".$sort."");
    } else {
      $list = pg_query_params($db, "SELECT * FROM (
                               SELECT Mata_Kuliah_Spesial.*,Mahasiswa.nama,JENISMKS.namamks
                               FROM Mata_Kuliah_Spesial,MAHASISWA,JENISMKS,DOSEN_PEMBIMBING
                               WHERE Mata_Kuliah_Spesial.idjenismks = JENISMKS.id AND Mata_Kuliah_Spesial.npm = MAHASISWA.npm
                               AND Mata_Kuliah_Spesial.idmks=DOSEN_PEMBIMBING.idmks AND DOSEN_PEMBIMBING.nipdosenpembimbing = $1
                             UNION
                               SELECT Mata_Kuliah_Spesial.*,Mahasiswa.nama,JENISMKS.namamks
                               FROM Mata_Kuliah_Spesial,MAHASISWA,JENISMKS,DOSEN_PENGUJI
                               WHERE Mata_Kuliah_Spesial.idjenismks = JENISMKS.id AND Mata_Kuliah_Spesial.npm = MAHASISWA.npm
                               AND Mata_Kuliah_Spesial.idmks=DOSEN_PENGUJI.idmks AND DOSEN_PENGUJI.nipdosenpenguji = $1)
                             ORDER BY ".$sort."",array($nip));
    }
    return $list;
  }
?>
