<?php
session_start();

if (!isset($_SESSION['auth']) || ($_SESSION['auth'] = false)){
	header('Location: login.php');
}

if ($_SESSION['role'] == 'mahasiswa'){
	header('Location: index.php');
	exit();
}

$sortby = 'm.nama';

include 'config.php';

extract($_POST);
if (isset($izinkan)) {
	pg_query($db, "update mata_kuliah_spesial set ijinmajusidang = true where idmks = $izinkan");
}


?>
<!DOCTYPE html>
<html>
<head>
	<title>Izin Jadwal Sidang | SISIDANG</title>
	<!--Import Google Icon Font-->
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="src/css/materialize.min.css"  media="screen,projection"/>
	<!--Import Google Slabo Font-->
	<link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="src/css/style.css"  media="screen,projection"/>
	<script type="text/javascript" src="src/js/jquery-3.1.1.js"></script>
	<script type="text/javascript" src="src/js/materialize.js"></script>
	<script type="text/javascript" src="src/js/jquery.js"></script>

	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body class= "grey lighten-1">
	<?php include "views/navbar.php"; ?>

	<div id ="aboutus" class="section grey lighten-1">
		<div class="row container highlight">
			<h2>Izin Jadwal Sidang</h2>
			<div class="sort col 6">
				<span class="col 1">Sort By:</span>
				<form action = "izin_jadwal_sidang.php" method = "post" class="col 2">
					<button name="sortby" value="m.nama" class="btn black">Mahasiswa</button>
				</form>
				<form action = "izin_jadwal_sidang.php" method = "post" class="col 2">
					<button name="sortby" value="jmks.namamks" class="btn black">Sidang</button>
				</form>
				<form action = "izin_jadwal_sidang.php" method = "post" class="col 2">
					<button name="sortby" value="js.tanggal, js.jammulai, js.jamselesai" class="btn black">Waktu</button>
				</form>
			</div>
			<table class="stripped centered">
				<thead>
					<tr>
						<th>Mahasiswa</th>
						<th>Jenis Sidang</th>
						<th>Judul</th>
						<th>Waktu dan Lokasi</th>
						<th>
							<?php
							if ($_SESSION['role'] == 'admin') {
								echo 'Dosen Pembimbing';
							} else {
								echo 'Pembimbing Lain';
							}
							?>
						</th>
						<th>Izinkan Maju Sidang</th>
					</tr>
				</thead>
				<tbody>
					<?php
					if ($_SESSION['role'] == 'admin') {
						$sidang = pg_query($db, "select m.nama, jmks.namamks, mks.judul, js.tanggal, js.jammulai, js.jamselesai, r.namaruangan, d.nama, mks.ijinmajusidang, mks.idmks
							from mahasiswa m, jenismks jmks, mata_kuliah_spesial mks, jadwal_sidang js, ruangan r, dosen_pembimbing dp, dosen d
							where m.npm = mks.npm and jmks.id = mks.idjenismks and js.idmks = mks.idmks and js.idruangan = r.idruangan and dp.idmks = mks.idmks and dp.nipdosenpembimbing = d.nip
							order by $sortby asc");
						$temp = array('');
						$flag = false;
						$size = pg_num_rows($sidang);
						if ($size > 0) {
							while ($row = pg_fetch_row($sidang)) {
								if ($temp[0] != $row[0] && $flag == true) {
									$flag = false;
									if ($temp[8] == 't') {
										echo '<td>dizinkan</td>';
									} else {
										echo '<td>
										<form action = "izin_jadwal_sidang.php" method = "post">
											<button name="izinkan" value="' . $temp[9] . '">Izinkan</button>
										</form>
									</td>';
									echo '</tr>';
								}
							}
							if ($temp[0] != $row[0] && $flag == false) {
								$flag = true;
								$temp = $row;
								echo '<tr>';
								echo '<td>' . $row[0] . '</td>';
								echo '<td>' . $row[1] . '<br>Sebagai: Pembimbing</td>';
								echo '<td>' . $row[2] . '</td>';
								echo '<td>' . $row[3] . '/' . $row[4] . '-' . $row[5] . '/' . $row[6] . '</td>';
								echo '<td><ul><li>' . $row[7] . '</li>';
							} else {
								echo '<li>' . $row[7] . '<li>';
							}
						}
						if ($temp[8] == 't') {
							echo '<td>dizinkan</td>';
						} else {
							echo '<td>
							<form action = "izin_jadwal_sidang.php" method = "post">
								<button name="izinkan" value="' . $temp[9] . '">Izinkan</button>
							</form>
						</td>';
						echo '</tr>';
					}
				}
			}
			if ($_SESSION['role'] == 'dosen'){
				$number_id = $_SESSION['number_id'];
				$sidang = pg_query($db, "select m.nama, jmks.namamks, mks.judul, js.tanggal, js.jammulai, js.jamselesai, r.namaruangan, d.nama, mks.ijinmajusidang, mks.idmks
					from mahasiswa m, jenismks jmks, mata_kuliah_spesial mks, jadwal_sidang js, ruangan r, dosen_pembimbing dp, dosen d
					where m.npm = mks.npm and jmks.id = mks.idjenismks and js.idmks = mks.idmks and js.idruangan = r.idruangan and dp.idmks = mks.idmks and dp.nipdosenpembimbing = d.nip
					and m.npm in (select mks.npm from dosen_pembimbing dp, mata_kuliah_spesial mks where mks.idmks = dp.idmks and dp.nipdosenpembimbing = '$number_id')
					order by $sortby asc");
				$temp = array('');
				$flag = false;
				$size = pg_num_rows($sidang);
				if ($size > 0) {
					while ($row = pg_fetch_row($sidang)) {
						if ($temp[0] != $row[0] && $flag == true) {
							$flag = false;
							if ($temp[8] == 't') {
								echo '<td>dizinkan</td>';
							} else {
								echo '<td>
								<form action = "izin_jadwal_sidang.php" method = "post">
									<button name="izinkan" value="' . $temp[9] . '">Izinkan</button>
								</form>
							</td>';
							echo '</tr>';
						}
					}
					if ($temp[0] != $row[0] && $flag == false) {
						$flag = true;
						$temp = $row;
						echo '<tr>';
						echo '<td>' . $row[0] . '</td>';
						echo '<td>' . $row[1] . '<br>Sebagai: Pembimbing</td>';
						echo '<td>' . $row[2] . '</td>';
						echo '<td>' . $row[3] . '/' . $row[4] . '-' . $row[5] . '/' . $row[6] . '</td>';
						echo '<td><ul>';
						if ($row[7] != $_SESSION['name']) {
							echo '<li>' . $row[7] . '</li>';
						}
					} else {
						if ($row[7] != $_SESSION['name']) {
							echo '<li>' . $row[7] . '</li>';
						}
					}
				}
				if ($temp[8] == 't') {
					echo '<td>dizinkan</td>';
				} else {
					echo '<td>
					<form action = "izin_jadwal_sidang.php" method = "post">
						<button name="izinkan" value="' . $temp[9] . '">Izinkan</button>
					</form>
				</td>';
				echo '</tr>';
			}
		}
	}
	?>
</tbody>
</table>
</div>
</div>
<center><ul class="pagination container">
	<li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
	<li class="active"><a href="#!">1</a></li>
	<li class="waves-effect"><a href="#!">2</a></li>
	<li class="waves-effect"><a href="#!">3</a></li>
	<li class="waves-effect"><a href="#!">4</a></li>
	<li class="waves-effect"><a href="#!">5</a></li>
	<li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
</ul></center>

<footer class="page-footer grey darken-3">
	<div class="container">
		<div class="row">
			<div class="col l6 s12">
				<h5 class="white-text">SISIDANG</h5>
				<p class="grey-text text-lighten-4">Website ini dibuat untuk memenuhi tugas akhir mata kuliah Basis Data Gasal 2016/2017</p>
			</div>
			<div class="col l4 offset-l2 s12">
				<h5 class="white-text">Links</h5>
				<ul>
					<li><a class="grey-text text-lighten-3" href="#!">Sumber</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="footer-copyright" >
		<div class="container">
			© 2016 Created by Kelompok T11 Basdat A
		</div>
	</div>
</footer>
</body>
</html>