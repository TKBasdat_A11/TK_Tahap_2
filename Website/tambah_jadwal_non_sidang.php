<?php 
	session_start();
	if (!isset($_SESSION['auth'])){
		header('Location: login.php');
		exit();
	}
	if ($_SESSION['role'] == 'mahasiswa'){
		header('Locatiion: index.php');
		exit();
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Tambah Jadwal Non-Sidang | SISIDANG</title>
	<!--Import Google Icon Font-->
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="src/css/materialize.min.css"  media="screen,projection"/>
	<!--Import Google Slabo Font-->
	<link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="src/css/style.css"  media="screen,projection"/>
	<script type="text/javascript" src="src/js/jquery-3.1.1.js"></script>
	<script type="text/javascript" src="src/js/jquery.js"></script>
	<script type="text/javascript" src="src/js/tambah_jadwal_non_sidang.js"></script>
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body class= "grey lighten-1">
		<?php include "views/navbar.php"; ?>
	  	<div id ="aboutus" class="section grey lighten-1">
		    <div class="row container">
		        <h2 class="white-text header" style="border-bottom: 2px solid black; padding-bottom: 5px; width: auto">Tambah Jadwal Non-Sidang</h2>
		    </div>
		</div>
		<div class="row container">
		    <div id="test1" class="col s12 grey darken-2">
		<form action="tambah_jadwal_non_sidang_handler.php" method="post">
			<label for='dosen'>Dosen</label>
			<select name="select_dosen" id="select_dosen"></select><br>
			<label for='tanggal_mulai'>Tanggal Mulai</label>
			<input type="date" name="tanggal_mulai" id="tanggal_mulai"><br>
			<label for='tanggal_selesai'>Tanggal Selesai</label>
			<input type="date" name="tanggal_selesai" id="tanggal_selesai"><br>
			<label for='tanggal_selesai'>Jam Mulai</label>
			<input type="time" name="jam_mulai" id="jam_mulai"><br>
			<label for='jam_selesai'>Jam Selesai</label>
			<input type="time" name="jam_selesai" id="jam_selesai"><br>
			<label for='kegiatan_berulang'>Kegiatan Berulang</label><br>
			<input type="radio" name="kegiatan_berulang" id="kegiatan_berulang1" value="harian">
			<label for="kegiatan_berulang1">Harian</label>
			<input type="radio" name="kegiatan_berulang" id="kegiatan_berulang2" value="mingguan">
			<label for="kegiatan_berulang2">Mingguan</label>
			<input type="radio" name="kegiatan_berulang" id="kegiatan_berulang3" value="bulanan">
			<label for="kegiatan_berulang3">Bulanan</label><br><br>
			<label for='alasan'>Alasan</label>
			<textarea id="alasan" class="text" cols="86" rows ="20" name="alasan"></textarea>
			<br><br><input class="btn black" style="margin-bottom: 10px;" type="submit">
		</form>
		<button class="btn black">Cancel</button>
		    </div>
		</div>

	  	<footer class="page-footer grey darken-3">
         	<div class="container">
            	<div class="row">
              		<div class="col l6 s12">
                		<h5 class="white-text">SISIDANG</h5>
                		<p class="grey-text text-lighten-4">Website ini dibuat untuk memenuhi tugas akhir mata kuliah Basis Data Gasal 2016/2017</p>
              		</div>
              		<div class="col l4 offset-l2 s12">
                		<h5 class="white-text">Links</h5>
                		<ul>
                 			<li><a class="grey-text text-lighten-3" href="#!">Sumber</a></li>
                		</ul>
             		</div>
            	</div>
          	</div>
          	<div class="footer-copyright" >
            	<div class="container">
            		© 2016 Created by Kelompok T11 Basdat A
            	</div>
          	</div>
      	</footer>
</body>
<script type="text/javascript" src="src/js/materialize.js"></script>
</html>
