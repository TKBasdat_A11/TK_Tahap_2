<?php
	session_start();
	if (!isset($_SESSION['auth'])){
	 header('Location: login.php');
	}
	require_once('config.php');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Jadwal Sidang | SISIDANG</title>
	<!--Import Google Icon Font-->
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="src/css/materialize.min.css"  media="screen,projection"/>
	<!--Import Google Slabo Font-->
	<link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="src/css/style.css"  media="screen,projection"/>
	<script type="text/javascript" src="src/js/jquery-3.1.1.js"></script>
	<script type="text/javascript" src="src/js/materialize.js"></script>
		<script type="text/javascript" src="src/js/jquery.js"></script>

	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body class= "grey lighten-1">
	<?php include "views/navbar.php" ?>
	<?php 
		if ($_SESSION['role'] == 'admin'){
			include "views/lihat-sidang-admin.php";
		} else if ($_SESSION['role'] == 'dosen'){
			include "views/lihat-sidang-dosen.php";
		} else if ($_SESSION['role'] == 'mahasiswa'){
			include "views/lihat-sidang-mahasiswa.php";
		}
	?>
	<footer class="page-footer grey darken-3">
		<div class="container">
			<div class="row">
				<div class="col l6 s12">
					<h5 class="white-text">SISIDANG</h5>
					<p class="grey-text text-lighten-4">Website ini dibuat untuk memenuhi tugas akhir mata kuliah Basis Data Gasal 2016/2017</p>
				</div>
				<div class="col l4 offset-l2 s12">
					<h5 class="white-text">Links</h5>
					<ul>
						<li><a class="grey-text text-lighten-3" href="#!">Sumber</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="footer-copyright" >
			<div class="container">
				© 2016 Created by Kelompok T11 Basdat A
			</div>
		</div>
	</footer>
</body>
</html>
