<?php
session_start();
if (!isset($_SESSION['auth'])){
	header('Location: login.php');
}

if ($_SESSION['role'] != 'admin'){
	header('Location: index.php');
}
require_once('config.php');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Tambah MKS | SISIDANG</title>
	<!--Import Google Icon Font-->
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="src/css/materialize.min.css"  media="screen,projection"/>
	<!--Import Google Slabo Font-->
	<link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="src/css/style.css"  media="screen,projection"/>
	<script type="text/javascript" src="src/js/jquery-3.1.1.js"></script>
	<script type="text/javascript" src="src/js/jquery.js"></script>
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body class= "grey lighten-1">
	<?php include "views/navbar.php"; ?>
	<div class="row container">
		<div id="test1" class="col s12 grey darken-2">
			<h2>Tambah Data MKS</h2>
			<form action="tambah_mks_handler" method="post">
				<label for='term'>Term</label>
				<select name="select_term" id="select_term">
				</select><br>
				<label for='jenis'>Jenis</label>
				<select name="select_jenis" id="select_jenis">
				</select><br>
				<label for='mahasiswa'>Mahasiswa</label>
				<select name="select_mahasiswa" id="select_mahasiswa">
				</select><br>
				<label for='judul'>Judul MKS</label>
				<textarea name="judul" rows="4" cols="50" value='' required></textarea><br>
				<label for="pb1">Pembimbing 1</label>
				<select name="select_pb1" id="select_pb1">
				</select><br>
				<label for="pb2">Pembimbing 2</label>
				<select name="select_pb2" id="select_pb2">
					<option value=""></option>
				</select><br>
				<label for="pb3">Pembimbing 3</label>
				<select name="select_pb3" id="select_pb3">
					<option value=""></option>
				</select><br>
				<div class="pjwrapper">
					<label for="pj1">Penguji 1</label>
					<select name="select_pj1" id="select_pj1">
					</select><br>
				</div>
				<input class="btn black" style="margin-bottom: 10px;" type="submit">
			</form>
			<button class="btn black" id="tambah_pj">Tambah Penguji</button>
			<button class="btn black"><a href="index">Cancel</a></button>
		</div>
	</div>

	<footer class="page-footer grey darken-3">
		<div class="container">
			<div class="row">
				<div class="col l6 s12">
					<h5 class="white-text">SISIDANG</h5>
					<p class="grey-text text-lighten-4">Website ini dibuat untuk memenuhi tugas akhir mata kuliah Basis Data Gasal 2016/2017</p>
				</div>
				<div class="col l4 offset-l2 s12">
					<h5 class="white-text">Links</h5>
					<ul>
						<li><a class="grey-text text-lighten-3" href="#!">Sumber</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="footer-copyright" >
			<div class="container">
				© 2016 Created by Kelompok T11 Basdat A
			</div>
		</div>
	</footer>
</body>
<script type="text/javascript" src="src/js/materialize.js"></script>
<script type="text/javascript" src="src/js/tambah_mks.js"></script>
</html>
