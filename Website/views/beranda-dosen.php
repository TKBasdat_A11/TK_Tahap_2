<?php 
	//Beranda Dosen
?>
<link rel="stylesheet" type="text/css" href="src/fullcalendar-3.0.1/fullcalendar.min.css">
<script type="text/javascript" src="src/js/moment.min.js"></script>
<script type="text/javascript" src="src/fullcalendar-3.0.1/fullcalendar.min.js"></script>
<script type="text/javascript" src="src/fullcalendar-3.0.1/locale/id.js"></script>
<script type="text/javascript" src="src/js/kalendar-dosen.js"></script>

<input type="hidden" id="nipDosen" value="<?= $_SESSION['number_id']?>">
<div id ="aboutus" class="section grey lighten-1">
	<div class="row container">
		<h2 class="white-text header" style="border-bottom: 2px solid black; padding-bottom: 5px; width: auto">Jadwal Sidang Dosen</h2>
	</div>
</div>
<div class="container">
	<div class="section">
		<div id="calendar" style="margin:auto;"></div>
	</div>
</div>