<?php //Jadwal sidang
	require_once("models/jadwalSidang.php");
	$page = (isset($_GET['page']) ? $_GET['page'] : 1);
	$sortMode = (isset($_GET['sort']) ? $_GET['sort'] : '0');
	$jadwals = retrieveJadwal($db, $page, $sortMode);
	$num = getPaginationAmount($db);
?>
<div id ="aboutus" class="section grey lighten-1">
	<div class="row container">
		<h2 class="white-text header" style="border-bottom: 2px solid black; padding-bottom: 5px; width: auto">Jadwal Sidang (Admin)</h2>
	</div>
</div>

<div id ="aboutus" class="section grey lighten-1">
	<div class="row container highlight">
		<h2>Daftar Jadwal Sidang</h2>
		<a href="tambah_mks.php"><button class="btn black" style="margin-bottom: 20px;">Tambah</button><br></a>
		<span>Sort By:
			<a href="?sort=1"><button class="btn black" style="margin-left: 10px; margin-right: 10px;">Mahasiswa</button></a>
			<a href="?sort=2"><button class="btn black" style="margin-left: 10px; margin-right: 10px;">Jenis MKS</button></a>
			<a href="?sort=0"><button class="btn black" style="margin-left: 10px; margin-right: 10px;">Waktu</button></a>
		</span>
		<table class="striped centered">
			<thead>
				<tr>
					<th data-field="id">Jenis Sidang</th>
					<th data-field="name">Mahasiswa</th>
					<th data-field="price">Judul</th>

					<th data-field="id">Dosen Pembimbing</th>
					<th data-field="name">Dosen Penguji</th>
					<th data-field="price">Waktu dan Lokasi</th>

					<th data-field="price">Action</th>
				</tr>
			</thead>

			<tbody>
				<?php while($row = pg_fetch_assoc($jadwals)):?>
					<tr>
						<td><?= $row['namamks']?></td>
						<td><?= $row['nama']?></td>
						<td><?= $row['judul']?></td>
						<td>
							<?php 
								$dosBing = retrieveDosenPembimbing($db, $row['idmks']);
								$lstDosen = '';
								while($dosen = pg_fetch_assoc($dosBing)){
									$lstDosen = $lstDosen.$dosen['nama'].', ';
								}
								if ($lstDosen != ''){
									$lstDosen = substr($lstDosen, 0, strlen($lstDosen)-2);
								}
								echo $lstDosen;
							?>
						</td>
						<td>
							<?php 
								$dosPeng = retrieveDosenPenguji($db, $row['idmks']);
								$lstDosen = '';
								while($dosen = pg_fetch_assoc($dosPeng)){
									$lstDosen = $lstDosen.$dosen['nama'].', ';
								}
								if ($lstDosen != ''){
									$lstDosen = substr($lstDosen, 0, strlen($lstDosen)-2);
								}
								echo $lstDosen;
							?>
						</td>
						<td>
							<?= date("d F o",strtotime($row['tanggal'])).' '.$row['jammulai'].'-'.$row['jamselesai'].' '.$row['namaruangan']?>
						</td>
						<td><a href="edit_jadwal_sidang.php?idjadwal=<?= $row['idjadwal']?>"><button class="btn black">Edit</button></td></a>
					</tr>
				<?php endwhile; ?>
			</tbody>
		</table>
	</div>
</div>
<center><ul class="pagination container">
	<?php if ($page > 1): ?>
			<li class="disabled"><a href="?page=<?= ($page-1)?>&sort=<?= $sortMode?>"><i class="material-icons">chevron_left</i></a></li>
	<?php 
		endif;
		$pageAmount = pg_fetch_assoc($num);
		$pageAmount = ceil($pageAmount['count']/10);
		for($i = 1; $i <= $pageAmount; $i++):
			$sort = (isset($_GET['sort']) ? '&sort='.$_GET['sort'] : '');
			if ($page == $i):
	 ?>
			<li class="active"><a href="?page=<?= $i.$sort?>"><?= $i?></a></li>
			<?php else: ?>
			<li class="waves-effect"><a href="?page=<?= $i.$sort?>"><?= $i?></a></li>
			<?php endif; ?>
	<?php endfor; ?>
	<?php if ($page != $pageAmount): ?>
		<li class="waves-effect"><a href="?page=<?= ($page+1)?>&sort=<?= $sortMode?>"><i class="material-icons">chevron_right</i></a></li>
	<?php endif; ?>
</ul></center>