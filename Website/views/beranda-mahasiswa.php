<?php 
	//Beranda Mahasiswa Page
	require_once("models/jadwalSidang.php");
	$result = retrieveDataMahasiswa($db, $_SESSION['number_id']);
	$row = pg_fetch_assoc($result);
?>
<div id ="aboutus" class="section grey lighten-1">
	<div class="row container">
		<h2 class="white-text header" style="border-bottom: 2px solid black; padding-bottom: 5px; width: auto">Data Mahasiswa</h2>
	</div>
	<div class="container">
		
		<table class="bordered highlighted">
			<tbody>
				<tr>
					<th>Nama</th>
					<td><?= $row['nama']?></td>
				</tr>
				<tr>
					<th>NPM</th>
					<td><?= $row['npm']?></td>
				</tr>
				<tr>
					<th>Email</th>
					<td><?= $row['email']?></td>
				</tr>
				<tr>
					<th>Email-alternatif</th>
					<td><?= $row['email_alternatif']?></td>
				</tr>
				<tr>
					<th>Telepon</th>
					<td><?= $row['telepon']?></td>
				</tr>
				<tr>
					<th>No. Telp</th>
					<td><?= $row['notelp']?></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>