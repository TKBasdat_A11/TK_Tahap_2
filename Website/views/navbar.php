<?php //Navbar templte ?>

<script type="text/javascript" src="src/js/navbar.js"></script>
<div class="navbar-fixed">
	<nav>
		<div class="nav-wrapper grey darken-3">
			<div class="container">
				<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
				<ul href="#" class="hide-on-med-and-down">
					<li><a href="index.php"><i class="material-icons">schedule</i></a></li>
					<li style="font-size: 20px;">SISTEM PENJADWALAN SISIDANG</li>
				</ul>
				<ul id="dropdown1" class="dropdown-content">
					<li><a href="lihat-jadwal.php">Lihat Jadwal Sidang</a></li>
					<li><a href="tambah_mks.php">Tambah Peserta MKS</a></li>

					<?php if ($_SESSION['role'] == 'admin' || $_SESSION['role'] =='dosen'): ?>
						<li><a href="daftar_mks.php">Lihat Daftar MKS</a></li>
						<li><a href="jadwal_non_sidang.php">Buat Jadwal Non Sidang Dosen</a></li>
						<li><a href="izin_jadwal_sidang.php">Izin Jadwal Sidang</a></li>
						<?php endif; ?>
					<?php if ($_SESSION['role'] =='admin'): ?>
						<li><a href="buat_jadwal_sidang_mks.php">Buat Jadwal Sidang MKS</a></li>
					<?php endif; ?>
				</ul>
				<ul id="nav-mobile" href="#" class="left hide-on-med-and-down">
					<li><a class="dropdown-button" href="#!" data-activates="dropdown1">Menu<i class="material-icons right">arrow_drop_down</i></a></li>
				</ul>
				<ul id="nav-mobile" href="#" class="right hide-on-med-and-down">
					<?php if ($_SESSION['role'] == 'admin'): ?>
						<li>Admin</li>
					<?php else: ?>
						<li><?= $_SESSION['name'].' - '.$_SESSION['number_id']?></li>
					<?php endif; ?>
					<li><a href="logout.php">Logout</a></li>
				</ul>

				<ul class="side-nav" id="mobile-demo">
					<?php if ($_SESSION['role'] == 'admin'): ?>
						<li>Admin</li>
					<?php else: ?>
						<li><?= $_SESSION['name'].' - '.$_SESSION['number_id']?></li>
					<?php endif; ?>					
					<li><a href="lihat-jadwal.php">Lihat Jadwal Sidang</a></li>
					<li><a href="tambah_mks.php">Tambah Peserta MKS</a></li>

					<?php if ($_SESSION['role'] == 'admin' || $_SESSION['role'] =='dosen'): ?>
						<li><a href="daftar_mks.php">Lihat Daftar MKS</a></li>
						<li><a href="#">Buat Jadwal Non Sidang Dosen</a></li>
					<?php endif; ?>

					<?php if ($_SESSION['role'] =='admin'): ?>
						<li><a href="buat_jadwal_sidang_mks.php">Buat Jadwal Sidang MKS</a></li>
					<?php endif; ?>
					<li><a href="logout.php">Logout</a></li>
				</ul>
			</div>
		</div>
	</nav>
</div>
