<?php
	//daftar sidang template
	require_once("models/jadwalSidang.php");
	$page = (isset($_GET['page']) ? $_GET['page'] : 1);
	$jadwals = retrieveJadwal($db, $page);
	$num = getPaginationAmount($db);
 ?>
<div id ="aboutus" class="section grey lighten-1">
	<div class="row container">
		<h2 class="white-text header" style="border-bottom: 2px solid black; padding-bottom: 5px; width: auto">Daftar Jadwal Sidang</h2>
	</div>
</div>
<div class="row container">
	<div class="col s6">
		<ul class="tabs">
			<li class="tab col s4 grey darken-3"><a class="active white-text" href="#test1">Jadwal Sidang</a></li>
			<li class="tab col s8 grey darken-3"><a class="white-text" href="#test2">Mahasiswa</a></li>
		</ul>
		<div id="test1" class="col s12 grey darken-2">
			<form action="search.php" method="GET">
				<input type="hidden" name="type" value="jadwal">
				<label>Term</label>
				<select name="term">
					<option value="0" disabled selected>Term</option>			      
					<option value="1">Genap 2015/2016</option>
					<option value="2">Gasal 2016/2017</option>
					<option value="3">Genap 2016/2017</option>
				</select>
				<br>
				<label>Jenis Sidang</label>
				<select name="jenis">
					<option value="" disabled selected>Jenis Sidang</option>
					<option value="1">Skripsi</option>
					<option value="2">Karya Akhir</option>
					<option value="3">Proposal Thesis</option>
					<option value="1">Usulan Penelitian</option>
					<option value="2">Seminar Hasil Penelitian S3</option>
					<option value="3">Pra Promosi</option>
					<option value="1">Promosi</option>
					<option value="2">Tesis</option>
					<option value="3">Lain-lain</option>
				</select>
				<center><button class="btn black" type="submit">Pilih</button></center>
			</form>
		</div>
		<div id="test2" class="col s12 grey darken-2">
			<form action="search.php" method="GET">
				<br>
				<input type="hidden" name="type" value="mahasiswa">
				<label>Nama</label>
				<input type="text" name="name">
				<center><button class="btn black" type="submit">Pilih</button></center>
			</form>
		</div>
	</div>
</div>
<div id ="aboutus" class="section grey lighten-1">
	<div class="row container highlight">
		<table class="striped centered">
			<thead>
				<tr>
					<th data-field="id">Jenis Sidang</th>
					<th data-field="name">Mahasiswa</th>
					<th data-field="price">Judul</th>

					<th data-field="id">Dosen Pembimbing</th>
					<th data-field="name">Dosen Penguji</th>
					<th data-field="price">Waktu dan Lokasi</th>

					<th data-field="price">Action</th>
				</tr>
			</thead>

			<tbody>
				<?php while($row = pg_fetch_assoc($jadwals)):?>
					<tr>
						<td><?= $row['namamks']?></td>
						<td><?= $row['nama']?></td>
						<td><?= $row['judul']?></td>
						<td>
							<?php 
								$dosBing = retrieveDosenPembimbing($db, $row['idmks']);
								$lstDosen = '';
								while($dosen = pg_fetch_assoc($dosBing)){
									$lstDosen = $lstDosen.$dosen['nama'].', ';
								}
								if ($lstDosen != ''){
									$lstDosen = substr($lstDosen, 0, strlen($lstDosen)-2);
								}
								echo $lstDosen;
							?>
						</td>
						<td>
							<?php 
								$dosPeng = retrieveDosenPenguji($db, $row['idmks']);
								$lstDosen = '';
								while($dosen = pg_fetch_assoc($dosPeng)){
									$lstDosen = $lstDosen.$dosen['nama'].', ';
								}
								if ($lstDosen != ''){
									$lstDosen = substr($lstDosen, 0, strlen($lstDosen)-2);
								}
								echo $lstDosen;
							?>
						</td>
						<td>
							<?= date("d F o",strtotime($row['tanggal'])).' '.$row['jammulai'].'-'.$row['jamselesai'].' '.$row['namaruangan']?>
						</td>
						<td><a href="edit_jadwal_sidang.php?idjadwal=<?= $row['idjadwal']?>"><button class="btn black">Edit</button></td></a>
					</tr>
				<?php endwhile; ?>
			</tbody>
		</table>
	</div>
</div>
<center>
	<ul class="pagination container">
	<?php if ($page != 1): ?>
		<li class="disabled"><a href="?page<?= ($page-1)?>"><i class="material-icons">chevron_left</i></a></li>
	<?php endif; ?>
	<?php 
		$pageAmount = pg_fetch_assoc($num);
		$pageAmount = ceil($pageAmount['count']/10);
		for($i = 1; $i <= $pageAmount; $i++):
			if ($page == $i):
	 ?>
			<li class="active"><a href="?page=<?= $i?>"><?= $i?></a></li>
			<?php else: ?>
			<li class="waves-effect"><a href="?page=<?= $i?>"><?= $i?></a></li>
			<?php endif; ?>
	<?php endfor; ?>
	<?php if ($page != $pageAmount): ?>
		<li class="waves-effect"><a href="?page=<?= ($page+1)?>"><i class="material-icons">chevron_right</i></a></li>
	<?php endif; ?>
	</ul>
</center>