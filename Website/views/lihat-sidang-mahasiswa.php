<?php 
	//Lihat sidang mahasiswa
	require_once("models/jadwalSidang.php");
	$result = retrieveSidangMahasiswa($db, $_SESSION['number_id']);
?>
<div id ="aboutus" class="section grey lighten-1">
	<div class="row container">
		<h2 class="white-text header" style="border-bottom: 2px solid black; padding-bottom: 5px; width: auto">Jadwal Sidang Mahasiswa</h2>
	</div>
</div>

<div class="container">
	<?php 
		if (pg_num_rows($result) > 0): 
			$row = pg_fetch_assoc($result);
	?>
	<table class="bordered highlighted">
		<tbody>
			<tr>
				<th>Judul Tugas Akhir</th>
				<td><?= $row['judul']?></td>
			</tr>
			<tr>
				<th>Jadwal Sidang</th>
				<td><?= date("d F o",strtotime($row['tanggal']))?></td>
			</tr>
			<tr>
				<th>Waktu Sidang</th>
				<td><?= $row['jammulai'].'-'.$row['jamselesai'].' @ '.$row['namaruangan']?></td>
			</tr>
			<tr>
				<th>Dosen Pembimbing</th>
				<td>
					<ul>
						<?php 
							$result = retrieveDosenPembimbing($db, $row['idmks']);
							if (pg_num_rows($result) > 0):
								while ($dosBing = pg_fetch_assoc($result)):
						?>
							<li><?= $dosBing['nama']?></li>
						<?php 
								endwhile;
							endif;
						 ?>
					</ul>
				</td>
			</tr>
			<tr>
				<th>Dosen Penguji</th>
				<td>
					<ul>
						<?php 
							$result = retrieveDosenPenguji($db, $row['idmks']);
							if (pg_num_rows($result) > 0):
								while ($dosPeng = pg_fetch_assoc($result)):
						?>
							<li><?= $dosPeng['nama']?></li>
						<?php 
								endwhile;
							endif;
						 ?>
					</ul>
				</td>
			</tr>
		</tbody>
	</table>
	<?php else : ?>
		<h3>Jadwal Sidang Anda Belum Ada</h3>
	<?php endif; ?>
</div>
