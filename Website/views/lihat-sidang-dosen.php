<?php //Jadwal sidang
require_once("models/jadwalSidang.php");
$page = (isset($_GET['page']) ? $_GET['page'] : 1);
$sortMode = (isset($_GET['sort']) ? $_GET['sort'] : '0');

$jadwals = retrieveJadwalDosen($db, $page, $sortMode, $_SESSION['number_id']);
$num = getPaginationDosen($db, $_SESSION['number_id']);
?>
<div id ="aboutus" class="section grey lighten-1">
	<div class="row container">
		<h2 class="white-text header" style="border-bottom: 2px solid black; padding-bottom: 5px; width: auto">Jadwal Sidang (Dosen)</h2>
	</div>
</div>

<div id ="aboutus" class="section grey lighten-1">
	<div class="row container highlight">
		<span>Sort By:
			<a href="?sort=1"><button class="btn black" style="margin-left: 10px; margin-right: 10px;">Mahasiswa</button></a>
			<a href="?sort=2"><button class="btn black" style="margin-left: 10px; margin-right: 10px;">Jenis MKS</button></a>
			<a href="?sort=0"><button class="btn black" style="margin-left: 10px; margin-right: 10px;">Waktu</button></a>
		</span>
		<table class="stripped centered">
			<thead>
				<tr>
					<th>Mahasiswa</th>
					<th>Jenis Sidang</th>
					<th>Judul</th>
					<th>Waktu dan Lokasi</th>
					<th>Pembimbing Lain</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
				<?php  while($row = pg_fetch_assoc($jadwals)):?>
					<tr>
						<td><?= $row['nama']?></td>
						<td>
							<ul style="list-style-type: none;">
								<li><?= $row['namamks']?></li>
								<li>Sebagai :</li>
								<li><?= $row['role']?></li>
							</ul>

						</td>
						<td><?= $row['judul']?></td>
						<td><?= date("d F o",strtotime($row['tanggal'])).' '.$row['jammulai'].'-'.$row['jamselesai'].' '.$row['namaruangan']?></td>
						<td>
							<ul>					
								<?php  
									$result = retrieveDosenPembimbingLain($db, $row['idmks'], $_SESSION['number_id']);
									if (pg_num_rows($result) == 0){
								?>
									<li>Tidak ada</li>
								<?php } else {  ?>
									<?php while($dosbing = pg_fetch_assoc($result)): ?>
										<li><?= $dosbing['nama']?></li>
									<?php endwhile; ?>
								<?php } ?>
							</ul>
						</td>
						<td>
							<ul>
								<?php if ($row['pengumpulanhardcopy'] === 't'): ?>
									<li>Mengumpulkan Hard Copy</li>
								<?php endif; ?>
								<?php if ($row['ijinmajusidang'] === 't'): ?>
									<li>Izin maju sidang</li>
								<?php endif; ?>
							</ul>
						</td>
					</tr>
				<?php endwhile; ?>
			</tbody>
		</table>
	</div>
</div>
<center>
	<ul class="pagination container">
		<?php if ($page > 1): ?>
			<li class="disabled"><a href="?page=<?= ($page-1)?>&sort=<?= $sortMode?>"><i class="material-icons">chevron_left</i></a></li>
			<?php 
			endif;
			$pageAmount = pg_fetch_assoc($num);
			$pageAmount = ceil($pageAmount['count']/10);
			for($i = 1; $i <= $pageAmount; $i++):
				$sort = (isset($_GET['sort']) ? '&sort='.$_GET['sort'] : '');
			if ($page == $i):
				?>
			<li class="active"><a href="?page=<?= $i.$sort?>"><?= $i?></a></li>
		<?php else: ?>
			<li class="waves-effect"><a href="?page=<?= $i.$sort?>"><?= $i?></a></li>
		<?php endif; ?>
	<?php endfor; ?>
	<?php if ($page != $pageAmount): ?>
		<li class="waves-effect"><a href="?page=<?= ($page+1)?>&sort=<?= $sortMode?>"><i class="material-icons">chevron_right</i></a></li>
	<?php endif; ?>
</ul>
</center>