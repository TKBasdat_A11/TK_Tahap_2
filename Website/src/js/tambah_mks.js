$(document).ready(function(){
	$.ajax({
		url : "tambah_mks_handler.php",
		method : "GET",
		dataType : "JSON",
		data : {"action" : "get_term"},
		success : function(response) {
			$('select').material_select();
			var data = response.data;
			var selectTerm = $("#select_term");
			for(var i = 0; i < data.length; i++) {
				var temp = ''
				if(data[i].semester % 2 == 1) {
					temp = 'Gasal';
				} else {
					temp = 'Genap';
				}
				tempA = data[i].tahun;
				tempB = data[i].semester;
				selectTerm.append('<option value='+tempA + 'n' + tempB+'>' + temp + ' ' + data[i].tahun + '/' + (parseInt(data[i].tahun)+1) + '</option>');
			}
			$('select').material_select('update');
		}
	});

	$.ajax({
		url : "tambah_mks_handler.php",
		method : "GET",
		dataType : "JSON",
		data : {"action" : "get_jenis"},
		success : function(response) {
			$('select').material_select();
			var data = response.data;
			var selectJenis = $("#select_jenis");
			for(var i = 0; i < data.length; i++) {
				selectJenis.append('<option value='+data[i].id+'>'+ data[i].namamks +'</option>');
			}
			$('select').material_select('update');
		}
	});

	$.ajax({
		url : "tambah_mks_handler.php",
		method : "GET",
		dataType : "JSON",
		data : {"action" : "get_mahasiswa"},
		success : function(response) {
			$('select').material_select();
			var data = response.data;
			var selectMahasiswa = $("#select_mahasiswa");
			for(var i = 0; i < data.length; i++) {
				selectMahasiswa.append('<option value='+data[i].npm+'>'+ data[i].nama +'</option>');
			}
			$('select').material_select('update');
		}
	});

	$.ajax({
		url : "tambah_mks_handler.php",
		method : "GET",
		dataType : "JSON",
		data : {"action" : "get_pb"},
		success : function(response) {
			$('select').material_select();
			var data = response.data;
			var selectPb1 = $("#select_pb1");
			for(var i = 0; i < data.length; i++) {
				selectPb1.append('<option value='+data[i].nip+'>'+ data[i].nama +'</option>');
			}

			var selectPb2 = $("#select_pb2");
			for(var i = 0; i < data.length; i++) {
				selectPb2.append('<option value='+data[i].nip+'>'+ data[i].nama +'</option>');
			}

			var selectPb3 = $("#select_pb3");
			for(var i = 0; i < data.length; i++) {
				selectPb3.append('<option value='+data[i].nip+'>'+ data[i].nama +'</option>');
			}
			$('select').material_select('update');
		}
	});

	$.ajax({
		url : "tambah_mks_handler.php",
		method : "GET",
		dataType : "JSON",
		data : {"action" : "get_pj"},
		success : function(response) {
			$('select').material_select();
			var data = response.data;
			var selectPj1 = $("#select_pj1");
			for(var i = 0; i < data.length; i++) {
				selectPj1.append('<option value='+data[i].nipsaranpenguji+'>'+ data[i].nama +'</option>');
			}
			$('select').material_select('update');
		}
	});

	var max_fields = 3;
	var wrapper = $('.pjwrapper');
	var add_button = $("#tambah_pj");
	var x = 1;
	$(add_button).click(function(e){
		e.preventDefault();
		if(x < max_fields) {
			x++;
			var temp = '<label for=pj' + x + '>Penguji ' + x + '</label> <select name=select_pj' + x + ' id=select_pj' + x + '> <option value=""></option> </select><br>';
			$(wrapper).append(temp);
			$('#select_pj1 option').clone().appendTo('#select_pj'+x);
		}
		$('select').material_select('update');
	});
});
