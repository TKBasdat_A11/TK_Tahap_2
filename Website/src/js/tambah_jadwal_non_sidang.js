$(document).ready(function(){

	var role;
	$.ajax({
		url : "tambah_jadwal_non_sidang_handler.php",
		method : "GET",
		dataType : "JSON",
		data : {"action" : "get_session_role", "requested" : "role"},
		success : function(response) {
			role = response.data;
		}
	});

	$.ajax({
		url : "tambah_jadwal_non_sidang_handler.php",
		method : "GET",
		dataType : "JSON",
		data : {"action" : "get_dosen"},
		success : function(response) {
			$('select').material_select();
			var data = response.data;
			var selectDosen = $("#select_dosen");
			for(var i = 0; i < data.length; i++) {
				selectDosen.append('<option value='+data[i].nip+'>'+ data[i].nama +'</option>');
			}
			$('select').material_select('update');
		}
	});
});
