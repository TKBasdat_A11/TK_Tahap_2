$(document).ready(function(){
	var nipDos = $("#nipDosen").val();

	$.post('models/jadwalSidangDosen', {nip: nipDos}, function(data) {
		data = JSON.parse(data);
		$('#calendar').fullCalendar({
				header: {
					left: 'prev,next today',
					center: 'title',
					right: 'month,basicWeek,basicDay'
				},
				defaultDate: moment().format(),
				navLinks: true, // can click day/week names to navigate views
				eventLimit: true, // allow "more" link when too many events
				events: data
			}, "json");
	});
	
});