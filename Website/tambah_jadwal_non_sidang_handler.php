<?php
	session_start();
	include("config.php");
	$response = ['data' => null];

	if($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['action'])) {
		switch($_GET['action']) {
			case 'get_dosen':
				if ($_SESSION['role'] == 'admin') {
					$dosenList = getAdmin($db);
				} else {
					$dosenList = getDosen($db);
				}
				$data = pg_fetch_all($dosenList);
				$response['data'] = $data;
				break;
		}
	}
	else if($_SERVER['REQUEST_METHOD'] === 'POST') {
		$nip = $_POST['select_dosen'];
		$tanggal_mulai = $_POST['tanggal_mulai'];
		$tanggal_selesai = $_POST['tanggal_selesai'];
		$jam_mulai = $_POST['jam_mulai'];
		$jam_selesai = $_POST['jam_selesai'];
		$repetisi = $_POST['kegiatan_berulang'];
		$alasan = $_POST['alasan'];
		$id = pg_query($db, "SELECT MAX(idjadwal) AS jumlah FROM jadwal_non_sidang");
		$temp = pg_fetch_result($id,"jumlah");
		$temp = ((int)$temp)+1;
		pg_query($db,"INSERT INTO jadwal_non_sidang VALUES ($temp,'$tanggal_mulai','$tanggal_selesai','$alasan','$repetisi','$nip')");
		header("Location:jadwal_non_sidang.php");
	}

	function getAdmin($db) {
		$list = pg_query($db, "SELECT nip,nama FROM DOSEN");
	 	return $list;
	}

	function getDosen($db) {
		$id =  $_SESSION['number_id'];
		$list = pg_query($db, "SELECT nip,nama FROM DOSEN WHERE DOSEN.nip = '$id'");
		return $list;
	}

	echo json_encode($response);
?>
