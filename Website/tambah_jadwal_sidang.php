<?php
  session_start();
  if (!isset($_SESSION['auth'])){
    header('Location: login.php');
  }
  include "config.php";

  $mahasiswa = $_POST['mahasiswa'];
  $tanggal = $_POST['date'];
  $mulai = $_POST['timestart'];
  $selesai = $_POST['timeend'];
  $ruangan = $_POST['select_ruangan'];
  $penguji = $_POST['select_pb1'];

  $idmks = pg_query("SELECT mks.idmks FROM mahasiswa m, mata_kuliah_spesial mks WHERE m.npm = mks.npm AND m.nama ='$mahasiswa'");
  $rowidmks = pg_fetch_assoc($idmks);
  $idmksfix = $rowidmks['idmks'];


  if (substr($mulai,-2,0) == "PM") {
    $mulaisidangjam = (int)(substr($mulai, 0,2) +12);
    $mulaisidangmenit = (int)(substr($mulai, 3, 5));
  }
  else {
    $mulaisidangjam = (int)(substr($mulai, 0,2));
    $mulaisidangmenit = (int)(substr($mulai, 3, 5));
  }

  if (substr($mulai,-2,0) == "PM") {
    $selesaisidangjam = (int)(substr($selesai, 0,2) +12);
    $selesaisidangmenit = (int)(substr($selesai, 3, 5));
  }
  else {
    $selesaisidangjam = (int)(substr($selesai, 0,2));
    $selesaisidangmenit = (int)(substr($selesai, 3, 5));
  }

  $jammulaisidang = $mulaisidangjam.":".$mulaisidangmenit.":00";
  $jamselesaisidang = $selesaisidangjam.":".$selesaisidangmenit.":00";

  $idruang = pg_query("SELECT idruangan FROM ruangan WHERE namaruangan='$ruangan'");
  $rowruang = pg_fetch_assoc($idruang);
  $id = $rowruang['idruangan'];
  $list_ruangan_terpakai = pg_query("SELECT tanggal, EXTRACT(HOUR FROM jammulai) as mulaijam, EXTRACT(MINUTE FROM jammulai) mulaimenit, EXTRACT(HOUR FROM jamselesai) as selesaijam, EXTRACT(MINUTE FROM jamselesai) as selesaimenit FROM jadwal_sidang WHERE idruangan='$id'");
  $list_tanggal_dosen_terkait = pg_query("SELECT jns.tanggalmulai, jns.tanggalselesai FROM jadwal_non_sidang jns, dosen d WHERE d.nip = jns.nipdosen");
  $boolean = True;
  while ($row = pg_fetch_assoc($list_ruangan_terpakai)) {

    if ($tanggal == $row['tanggal']) {
      if(($mulaisidangjam >= $row['mulaijam'] AND $mulaisidangjam <= $row['selesaijam']) OR ($selesaisidangjam >= $row['mulaijam'] AND $selesaisidangjam <= $row['selesaijam'])) {
        if(($mulaisidangmenit >= $row['mulaimenit'] AND $mulaisidangmenit <= $row['selesaimenit']) OR ($selesaisidangmenit >= $row['mulaimenit'] AND $selesaisidangmenit <= $row['selesaimenit'])) {
           $msg = "Jadwal bentrok dengan sidang lain!";
           $boolean = False;
           header("location:buat_jadwal_sidang_mks.php?msg=$msg"); 
        }  
      }
    }
  }

  if($boolean) {
    $lastid = pg_query("SELECT idjadwal FROM jadwal_sidang ORDER BY idjadwal DESC LIMIT 1");
    $lastestid = pg_fetch_assoc($lastid);
    $idjadwals = $lastestid['idjadwal'] + 1;
  //INSERT into paket (nama_paket, tujuan, fitur, harga) values('$paket','$tujuan','$fitur','$harga')";
   $result = pg_query("INSERT into jadwal_sidang (idjadwal, idmks, tanggal, jammulai, jamselesai, idruangan) values ('$idjadwals','$idmksfix','$tanggal','$jammulaisidang', '$jamselesaisidang', '$id')"); 

    header("Location: index.php");
  }
?>