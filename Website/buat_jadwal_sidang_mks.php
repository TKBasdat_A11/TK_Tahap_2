<?php
  session_start();
  if (!isset($_SESSION['auth'])){
    header('Location: login.php');
  }

  include "config.php";
  $term = pg_query($db, "SELECT * FROM TERM;");

?>
<!DOCTYPE html>
<html>
<head>
  <title>Buat Jadwal Sidang MKS | SISIDANG</title>
  <!--Import Google Icon Font-->
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <link type="text/css" rel="stylesheet" href="src/css/materialize.min.css"  media="screen,projection"/>
  <!--Import Google Slabo Font-->
  <link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
  <link type="text/css" rel="stylesheet" href="src/css/style.css"  media="screen,projection"/>
  <script type="text/javascript" src="src/js/jquery-3.1.1.js"></script>
  <script type="text/javascript" src="src/js/materialize.js"></script>        
  <script type="text/javascript" src="src/js/jquery.js"></script>

  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body class= "grey lighten-1">
    <?php include "views/navbar.php"; ?>
      <div id ="aboutus" class="section grey lighten-1">
        <div class="row container">
            <h2 class="center-align white-text header" style="border-bottom: 2px solid black; padding-bottom: 5px; width: auto">Buat Jadwal Sidang MKS</h2>
        </div>
            
    </div>
    <div class="row container">
        <div id="test1" class="col s8 offset-s2 grey darken-2">
          <form name="add_sidang" id="add_sidang" action="tambah_jadwal_sidang" method="post">
            <div class="col s12">
              <label id="asik" for='term'>Mahasiswa</label>
              <select onchange="getDosen()" name="mahasiswa" id="select_mahasiswa" required>
                <option></option>
                <?php include "models/mahasiswa_mks.php"; ?>
              </select><br>
              <label for='jenis'>Tanggal</label> 
              <input type="date" name="date" required />
              <br>
            </div>
            <div class="col s6">
              <label for='mahasiswa'>Jam Mulai</label>
              <input type="time" name="timestart" required />
            </div>
            <div class="col s6">
              <label for='judul'>Jam Selesai</label>
              <input type="time" name="timeend" required/><br>
              <br>
            </div>
            <div class="col s12" id="penguji">
              <label for="pb1">Ruangan</label>
              <select name="select_ruangan" id="select_ruangan" required>
                <?php include "models/ruangan_model.php"; ?>
              </select><br>
              <label for="pb2">Penguji 1</label>
              <select name="select_pb1" id="selectdosen" required>
              </select><br>
            </div>
            <?php 
              error_reporting( error_reporting() & ~E_NOTICE );
              $msg = $_GET['msg'];  //GET the message
              if($msg!='') echo '<span class="middle subline" style="color: red; text-align: center">'.$msg.'</span>'; //If message is set echo it
            ?>
            <input class="btn black" style="margin-bottom: 10px;" type="submit" id="selesai" name="action" value="Simpan">
          </form>

          <button class="btn black" id="tambah_pj">Tambah Penguji</button>
          <button class="btn black">Cancel</button>
        </div>
    </div>

      <footer class="page-footer grey darken-3">
          <div class="container">
              <div class="row">
                  <div class="col l6 s12">
                    <h5 class="white-text">SISIDANG</h5>
                    <p class="grey-text text-lighten-4">Website ini dibuat untuk memenuhi tugas akhir mata kuliah Basis Data Gasal 2016/2017</p>
                  </div>
                  <div class="col l4 offset-l2 s12">
                    <h5 class="white-text">Links</h5>
                    <ul>
                      <li><a class="grey-text text-lighten-3" href="#!">Sumber</a></li>  
                    </ul>
                </div>
              </div>
            </div>
            <div class="footer-copyright" >
              <div class="container">
                © 2016 Created by Kelompok T11 Basdat A
              </div>
            </div>
        </footer>
</body>
</html>