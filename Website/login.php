<?php
	session_start();
	if (isset($_SESSION['auth'])){
		header('Location: index.php');
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Login | SISIDANG</title>

	<!--Import Google Icon Font-->
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="src/css/materialize.min.css"  media="screen,projection"/>
	<!--Import Google Slabo Font-->
	<link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="src/css/style.css"  media="screen,projection"/>
	<script type="text/javascript" src="src/js/jquery-3.1.1.js"></script>
	<script type="text/javascript" src="src/js/materialize.js"></script>
    <script type="text/javascript" src="src/js/jquery.js"></script>

	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

</head>
<body class="grey darken-2">
	<div class="navbar-fixed">
			<nav>
	   			<div class="nav-wrapper grey darken-3">
	   				<div class="container">
	   				<ul id="nav-mobile" href="#" class="hide-on-med-and-down">
	   					<li><a href="#"><i class="material-icons">schedule</i></a></li>
	   					<li style="font-size: 20px;">SISTEM PENJADWALAN SISIDANG</li>
	   				</ul>
	      			</div>
	      		</div>
	  		</nav>
	  	</div>
		<div class="parallax-container" style="margin-bottom: 0px;">
		    <div class="parallax">
		   	 	<img class="blur" src="src/images/parallax1.jpg" style="opacity: 0.7">
			</div>
	   	 	<div class="centered-mid">
	   	 		<center><h2 class="white-text header">Sistem Penjadwalan SISIDANG</h2></center>
   	 			<div class="box">
   	 				<div class="row">
						<form class="col s12 m8 offset-m2 l6 offset-l3" method="POST" action="loginBack.php">
							<div class="row">
								<div class="input-field col s12">
									 <i class="material-icons prefix">account_circle</i>
									 <input id="icon_prefix" type="text" name="username" class="validate">
									 <label for="icon_prefix">Username</label>
								</div>
							</div>
							<div class='row'>
								<div class="input-field col s12">
									<i class="material-icons prefix">account_circle</i>
									<input id="icon_prefix" type="password" name="password" class="validate">
									<label for="icon_prefix">Password</label>
								</div>
							</div>
							<?php if (isset($_SESSION['error'])): ?>
							<div class="row">
								<div class="col s12">
									<center>
										<div class="chip">
											<?= $_SESSION['error']?>
											<i class="close material-icons">close</i>
										</div>
									</center>
								</div>
							</div>
							<?php unset($_SESSION['error']); endif; ?>
							<center><button class="btn black" type="submit" name="action"  value="login">Login
							</button></center>
						</form>
					</div>
   	 			</div>
	    	</div>
		</div>
		<footer class="page-footer grey darken-3" style="margin-top: 0px;">
         	<div class="container">
            	<div class="row">
              		<div class="col l6 s12">
                		<h5 class="white-text">SISIDANG</h5>
                		<p class="grey-text text-lighten-4">Website ini dibuat untuk memenuhi tugas akhir mata kuliah Basis Data Gasal 2016/2017</p>
              		</div>
              		<div class="col l4 offset-l2 s12">
                		<h5 class="white-text">Links</h5>
                		<ul>
                 			<li><a class="grey-text text-lighten-3" href="#!">Sumber</a></li>
                		</ul>
             		</div>
            	</div>
          	</div>
          	<div class="footer-copyright" style="">
            	<div class="container">
            		© 2016 Created by Kelompok T11 Basdat A
            	</div>
          	</div>
      	</footer>
		<!-- <h1 class="Header">Login</h1> -->
</body>
</html>
