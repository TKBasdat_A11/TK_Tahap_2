<?php
  session_start();
  if (!isset($_SESSION['auth']) || $_SESSION['role'] == 'mahasiswa'){
   header('Location: login.php');
  }
  require_once('config.php');

?>
<!DOCTYPE html>
<html>
<head>
  <title>Daftar MKS | SISIDANG</title>
  <!--Import Google Icon Font-->
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <link type="text/css" rel="stylesheet" href="src/css/materialize.min.css"  media="screen,projection"/>
  <!--Import Google Slabo Font-->
  <link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
  <link type="text/css" rel="stylesheet" href="src/css/style.css"  media="screen,projection"/>
  <script type="text/javascript" src="src/js/jquery-3.1.1.js"></script>
  <script type="text/javascript" src="src/js/materialize.js"></script>
    <script type="text/javascript" src="src/js/jquery.js"></script>

  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body class= "grey lighten-1">
    <?php include "views/navbar.php"; ?>

      <div id ="aboutus" class="section grey lighten-1">
        <div class="row container highlight">
       <h2>Daftar MK Spesial</h2>
    <?php if($_SESSION['role'] == 'admin'){?>
    <button class="btn black" style="margin-bottom: 20px;"><a href="tambah_mks.php">Tambah</a></button><br>
    <?php } ?>
    <span>Sort By:
      <button id="mahasiswa" class="btn black" style="margin-left: 10px; margin-right: 10px;">Mahasiswa</button>
      <button id="jenis" class="btn black" style="margin-left: 10px; margin-right: 10px;">Jenis MKS</button>
      <button id="term" class="btn black" style="margin-left: 10px; margin-right: 10px;">Term</button>
    </span>
    <input type="hidden" id="role" value=<?php echo $_SESSION['role']?>>
    <?php if($_SESSION['role'] == 'dosen') {?>
      <input type="hidden" id="nip" value=<?php echo $_SESSION['number_id']?>>
    <?php  }?>
    <table class="stripped centered">
      <thead>
        <tr>
          <th>ID</th>
          <th>Judul</th>
          <th>Mahasiswa</th>
          <th>Term</th>
          <th>Jenis MKS</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody id="mksList">
      </tbody>
    </table>
        </div>
    </div>
    <center><ul class="pagination container">
        <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
        <li class="active"><a href="#!">1</a></li>
        <li class="waves-effect"><a href="#!">2</a></li>
        <li class="waves-effect"><a href="#!">3</a></li>
        <li class="waves-effect"><a href="#!">4</a></li>
        <li class="waves-effect"><a href="#!">5</a></li>
        <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
    </ul></center>

      <footer class="page-footer grey darken-3">
          <div class="container">
              <div class="row">
                  <div class="col l6 s12">
                    <h5 class="white-text">SISIDANG</h5>
                    <p class="grey-text text-lighten-4">Website ini dibuat untuk memenuhi tugas akhir mata kuliah Basis Data Gasal 2016/2017</p>
                  </div>
                  <div class="col l4 offset-l2 s12">
                    <h5 class="white-text">Links</h5>
                    <ul>
                      <li><a class="grey-text text-lighten-3" href="#!">Sumber</a></li>
                    </ul>
                </div>
              </div>
            </div>
            <div class="footer-copyright" >
              <div class="container">
                © 2016 Created by Kelompok T11 Basdat A
              </div>
            </div>
        </footer>
        <script>
        $(document).ready(function(){
        var role = $('#role').val();
        var result = "";
        if(role == 'admin') {
          $.ajax({
            url : "daftar_mks_handler.php",
            method : "GET",
            dataType : "JSON",
            data : {"role": role},
            success : function(response) {
              var mksList = $('#mksList');
              for(var i = 0; i < response.length; i++) {
                var temp = '';
        				if(response[i].semester % 2 == 1) {
        					temp = 'Gasal';
        				} else {
        					temp = 'Genap';
        				}
                result += '<tr><td>'+response[i].idmks+'</td><td>'+response[i].judul+'</td><td>'+response[i].nama+'</td><td>'+temp+ ' ' + response[i].tahun + '/' + (parseInt(response[i].tahun)+1)+'</td><td>' + response[i].namamks + '</td><td><ul>';

                if(response[i].ijinmajusidang == 't') {
                  result+='<li>Izin Maju</li>';
                }
                if(response[i].pengumpulanhardcopy == 't'){
                  result+='<li>Kumpul Hard Copy</li>';
                }
                if(response[i].ijinmajusidang == 't') {
                  result+='<li>Siap Sidang</li>';
                }
                result+='</ul></td></tr>';
              }
              mksList.html(result);
            }
          });
        } else if(role == 'dosen') {
          $.ajax({
            url : "daftar_mks_handler.php",
            method : "GET",
            dataType : "JSON",
            data : {"role": role, "nip": $('#nip').val()},
            success : function(response) {
              var mksList = $('#mksList');
              for(var i = 0; i < response.length; i++) {
                var temp = '';
        				if(response[i].semester % 2 == 1) {
        					temp = 'Gasal';
        				} else {
        					temp = 'Genap';
        				}
                result += '<tr><td>'+response[i].idmks+'</td><td>'+response[i].judul+'</td><td>'+response[i].nama+'</td><td>'+temp+ ' ' + response[i].tahun + '/' + (parseInt(response[i].tahun)+1)+'</td><td>' + response[i].namamks + '</td><td><ul>';

                if(response[i].ijinmajusidang == 't') {
                  result+='<li>Izin Maju</li>';
                }
                if(response[i].pengumpulanhardcopy == 't'){
                  result+='<li>Kumpul Hard Copy</li>';
                }
                if(response[i].ijinmajusidang == 't') {
                  result+='<li>Siap Sidang</li>';
                }
                result+='</ul></td></tr>';
              }
              mksList.html(result);
            }
          });
        }

        $('#mahasiswa').click(function(){
          var role = $('#role').val();
          var result = "";
          if(role == 'admin') {
            $.ajax({
              url : "daftar_mks_handler.php",
              method : "GET",
              dataType : "JSON",
              data : {"role": role, "order": "mahasiswa"},
              success : function(response) {
                var mksList = $('#mksList');
                for(var i = 0; i < response.length; i++) {
                  var temp = '';
          				if(response[i].semester % 2 == 1) {
          					temp = 'Gasal';
          				} else {
          					temp = 'Genap';
          				}
                  result += '<tr><td>'+response[i].idmks+'</td><td>'+response[i].judul+'</td><td>'+response[i].nama+'</td><td>'+temp+ ' ' + response[i].tahun + '/' + (parseInt(response[i].tahun)+1)+'</td><td>' + response[i].namamks + '</td><td><ul>';

                  if(response[i].ijinmajusidang == 't') {
                    result+='<li>Izin Maju</li>';
                  }
                  if(response[i].pengumpulanhardcopy == 't'){
                    result+='<li>Kumpul Hard Copy</li>';
                  }
                  if(response[i].ijinmajusidang == 't') {
                    result+='<li>Siap Sidang</li>';
                  }
                  result+='</ul></td></tr>';
                }
                mksList.html(result);
              }
            });
          } else if(role == 'dosen') {
            $.ajax({
              url : "daftar_mks_handler.php",
              method : "GET",
              dataType : "JSON",
              data : {"role": role, "nip": $('#nip').val(), "order": "mahasiswa"},
              success : function(response) {
                var mksList = $('#mksList');
                for(var i = 0; i < response.length; i++) {
                  var temp = '';
          				if(response[i].semester % 2 == 1) {
          					temp = 'Gasal';
          				} else {
          					temp = 'Genap';
          				}
                  result += '<tr><td>'+response[i].idmks+'</td><td>'+response[i].judul+'</td><td>'+response[i].nama+'</td><td>'+temp+ ' ' + response[i].tahun + '/' + (parseInt(response[i].tahun)+1)+'</td><td>' + response[i].namamks + '</td><td><ul>';

                  if(response[i].ijinmajusidang == 't') {
                    result+='<li>Izin Maju</li>';
                  }
                  if(response[i].pengumpulanhardcopy == 't'){
                    result+='<li>Kumpul Hard Copy</li>';
                  }
                  if(response[i].ijinmajusidang == 't') {
                    result+='<li>Siap Sidang</li>';
                  }
                  result+='</ul></td></tr>';
                }
                mksList.html(result);
              }
            });
          }
        });

        $('#jenis').click(function(){
          var role = $('#role').val();
          var result = "";
          if(role == 'admin') {
            $.ajax({
              url : "daftar_mks_handler.php",
              method : "GET",
              dataType : "JSON",
              data : {"role": role, "order": "jenis"},
              success : function(response) {
                var mksList = $('#mksList');
                for(var i = 0; i < response.length; i++) {
                  var temp = '';
                  if(response[i].semester % 2 == 1) {
                    temp = 'Gasal';
                  } else {
                    temp = 'Genap';
                  }
                  result += '<tr><td>'+response[i].idmks+'</td><td>'+response[i].judul+'</td><td>'+response[i].nama+'</td><td>'+temp+ ' ' + response[i].tahun + '/' + (parseInt(response[i].tahun)+1)+'</td><td>' + response[i].namamks + '</td><td><ul>';

                  if(response[i].ijinmajusidang == 't') {
                    result+='<li>Izin Maju</li>';
                  }
                  if(response[i].pengumpulanhardcopy == 't'){
                    result+='<li>Kumpul Hard Copy</li>';
                  }
                  if(response[i].ijinmajusidang == 't') {
                    result+='<li>Siap Sidang</li>';
                  }
                  result+='</ul></td></tr>';
                }
                mksList.html(result);
              }
            });
          } else if(role == 'dosen') {
            $.ajax({
              url : "daftar_mks_handler.php",
              method : "GET",
              dataType : "JSON",
              data : {"role": role, "nip": $('#nip').val(), "order": "jenis"},
              success : function(response) {
                var mksList = $('#mksList');
                for(var i = 0; i < response.length; i++) {
                  var temp = '';
                  if(response[i].semester % 2 == 1) {
                    temp = 'Gasal';
                  } else {
                    temp = 'Genap';
                  }
                  result += '<tr><td>'+response[i].idmks+'</td><td>'+response[i].judul+'</td><td>'+response[i].nama+'</td><td>'+temp+ ' ' + response[i].tahun + '/' + (parseInt(response[i].tahun)+1)+'</td><td>' + response[i].namamks + '</td><td><ul>';

                  if(response[i].ijinmajusidang == 't') {
                    result+='<li>Izin Maju</li>';
                  }
                  if(response[i].pengumpulanhardcopy == 't'){
                    result+='<li>Kumpul Hard Copy</li>';
                  }
                  if(response[i].ijinmajusidang == 't') {
                    result+='<li>Siap Sidang</li>';
                  }
                  result+='</ul></td></tr>';
                }
                mksList.html(result);
              }
            });
          }
        });

        $('#term').click(function(){
          var role = $('#role').val();
          var result = "";
          if(role == 'admin') {
            $.ajax({
              url : "daftar_mks_handler.php",
              method : "GET",
              dataType : "JSON",
              data : {"role": role, "order": "term"},
              success : function(response) {
                var mksList = $('#mksList');
                for(var i = 0; i < response.length; i++) {
                  var temp = '';
                  if(response[i].semester % 2 == 1) {
                    temp = 'Gasal';
                  } else {
                    temp = 'Genap';
                  }
                  result += '<tr><td>'+response[i].idmks+'</td><td>'+response[i].judul+'</td><td>'+response[i].nama+'</td><td>'+temp+ ' ' + response[i].tahun + '/' + (parseInt(response[i].tahun)+1)+'</td><td>' + response[i].namamks + '</td><td><ul>';

                  if(response[i].ijinmajusidang == 't') {
                    result+='<li>Izin Maju</li>';
                  }
                  if(response[i].pengumpulanhardcopy == 't'){
                    result+='<li>Kumpul Hard Copy</li>';
                  }
                  if(response[i].ijinmajusidang == 't') {
                    result+='<li>Siap Sidang</li>';
                  }
                  result+='</ul></td></tr>';
                }
                mksList.html(result);
              }
            });
          } else if(role == 'dosen') {
            $.ajax({
              url : "daftar_mks_handler.php",
              method : "GET",
              dataType : "JSON",
              data : {"role": role, "nip": $('#nip').val(), "order": "term"},
              success : function(response) {
                var mksList = $('#mksList');
                for(var i = 0; i < response.length; i++) {
                  var temp = '';
                  if(response[i].semester % 2 == 1) {
                    temp = 'Gasal';
                  } else {
                    temp = 'Genap';
                  }
                  result += '<tr><td>'+response[i].idmks+'</td><td>'+response[i].judul+'</td><td>'+response[i].nama+'</td><td>'+temp+ ' ' + response[i].tahun + '/' + (parseInt(response[i].tahun)+1)+'</td><td>' + response[i].namamks + '</td><td><ul>';

                  if(response[i].ijinmajusidang == 't') {
                    result+='<li>Izin Maju</li>';
                  }
                  if(response[i].pengumpulanhardcopy == 't'){
                    result+='<li>Kumpul Hard Copy</li>';
                  }
                  if(response[i].ijinmajusidang == 't') {
                    result+='<li>Siap Sidang</li>';
                  }
                  result+='</ul></td></tr>';
                }
                mksList.html(result);
              }
            });
          }
        });

      });
        </script>
</body>
</html>
