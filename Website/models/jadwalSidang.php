<?php
	function retrieveJadwal($db,$page = 1, $sort = 0){
		 if ($sort == '1'){
			$sorted = "M.nama";
		} else if ($sort == '2'){
			$sorted = "J.ID";
		} else {
			$sorted = "JS.tanggal, JS.jammulai";
		}

		$result = pg_query_params($db, "SELECT MKS.IDMKS, M.Nama, J.NamaMKS, JS.idjadwal, MKS.judul, JS.Tanggal, JS.JamMulai, JS.JamSelesai, R.namaRuangan
				  FROM Mata_Kuliah_Spesial MKS
				  JOIN MAHASISWA M ON M.NPM = MKS.NPM
				  JOIN JADWAL_SIDANG JS ON MKS.IDMKS = JS.IDMKS
				  JOIN JENISMKS J ON MKS.IDJENISMKS = J.ID
				  JOIN RUANGAN R ON JS.IDRuangan = R.IDRUANGAN
				  WHERE MKS.isSiapSidang = True
				  ORDER BY ".$sorted."
				  LIMIT 10 OFFSET $1;"
				  , array(($page-1)*10));
		return $result;
	}

	function retrieveJadwalDosen($db, $page = 1, $sort = 0, $nip){
		if ($sort == '1'){
			$sorted = "T.nama";
		} else if ($sort == '2'){
			$sorted = "T.ID";
		} else {
			$sorted = "T.tanggal, T.jammulai";
		}

		$result = pg_query_params($db, "SELECT * FROM (
						SELECT M.nama, MKS.*, JS.jammulai, JS.jamselesai, J.ID, R.namaruangan, JS.tanggal, J.NamaMKS, 'pembimbing' AS Role
						FROM Mata_Kuliah_Spesial MKS
						JOIN DOSEN_PEMBIMBING DP ON MKS.IDMKS = DP.IDMKS
						JOIN MAHASISWA M ON M.NPM = MKS.NPM
						JOIN JADWAL_SIDANG JS ON MKS.IDMKS = JS.IDMKS
						JOIN JENISMKS J ON MKS.IDJENISMKS = J.ID
						JOIN RUANGAN R ON JS.IDRuangan = R.IDRUANGAN
						WHERE DP.NIPDosenPembimbing = '".$nip."' AND MKS.isSiapSidang = True
						UNION
						SELECT M.nama, MKS.*, JS.jammulai, JS.jamselesai, J.ID, R.namaruangan, JS.tanggal, J.NamaMKS, 'penguji' AS Role
						FROM Mata_Kuliah_Spesial MKS
						JOIN DOSEN_PENGUJI DP ON MKS.IDMKS = DP.IDMKS
						JOIN MAHASISWA M ON M.NPM = MKS.NPM
						JOIN JADWAL_SIDANG JS ON MKS.IDMKS = JS.IDMKS
						JOIN JENISMKS J ON MKS.IDJENISMKS = J.ID
						JOIN RUANGAN R ON JS.IDRuangan = R.IDRUANGAN
						WHERE DP.NIPDosenPenguji = '".$nip."'  AND MKS.isSiapSidang = True) T
					ORDER BY ".$sorted."
					LIMIT 10 OFFSET $1;", array(($page-1)*10));
		return $result;
	}

	function retriveJadwalDosenAll($db, $nip){
		$result = pg_query($db, "
						SELECT M.nama, MKS.*, JS.jammulai, JS.jamselesai, R.namaruangan, JS.tanggal, J.NamaMKS, 'pembimbing' AS Role
						FROM Mata_Kuliah_Spesial MKS
						JOIN DOSEN_PEMBIMBING DP ON MKS.IDMKS = DP.IDMKS
						JOIN MAHASISWA M ON M.NPM = MKS.NPM
						JOIN JADWAL_SIDANG JS ON MKS.IDMKS = JS.IDMKS
						JOIN JENISMKS J ON MKS.IDJENISMKS = J.ID
						JOIN RUANGAN R ON JS.IDRuangan = R.IDRUANGAN
						WHERE DP.NIPDosenPembimbing = '".$nip."'
						UNION
						SELECT M.nama, MKS.*, JS.jammulai, JS.jamselesai, R.namaruangan, JS.tanggal, J.NamaMKS, 'penguji' AS Role
						FROM Mata_Kuliah_Spesial MKS
						JOIN DOSEN_PENGUJI DP ON MKS.IDMKS = DP.IDMKS
						JOIN MAHASISWA M ON M.NPM = MKS.NPM
						JOIN JADWAL_SIDANG JS ON MKS.IDMKS = JS.IDMKS
						JOIN JENISMKS J ON MKS.IDJENISMKS = J.ID
						JOIN RUANGAN R ON JS.IDRuangan = R.IDRUANGAN
						WHERE DP.NIPDosenPenguji = '".$nip."'
					;");
		return $result;
	}

	function getPaginationDosen($db, $nip){
		$result = pg_query($db, "SELECT COUNT(*) FROM (
						SELECT M.nama, MKS.*, JS.jammulai, JS.jamselesai, J.ID, R.namaruangan, JS.tanggal, J.NamaMKS, 'pembimbing' AS Role
						FROM Mata_Kuliah_Spesial MKS
						JOIN DOSEN_PEMBIMBING DP ON MKS.IDMKS = DP.IDMKS
						JOIN MAHASISWA M ON M.NPM = MKS.NPM
						JOIN JADWAL_SIDANG JS ON MKS.IDMKS = JS.IDMKS
						JOIN JENISMKS J ON MKS.IDJENISMKS = J.ID
						JOIN RUANGAN R ON JS.IDRuangan = R.IDRUANGAN
						WHERE DP.NIPDosenPembimbing = '".$nip."' AND MKS.isSiapSidang = True
						UNION
						SELECT M.nama, MKS.*, JS.jammulai, JS.jamselesai, J.ID, R.namaruangan, JS.tanggal, J.NamaMKS, 'penguji' AS Role
						FROM Mata_Kuliah_Spesial MKS
						JOIN DOSEN_PENGUJI DP ON MKS.IDMKS = DP.IDMKS
						JOIN MAHASISWA M ON M.NPM = MKS.NPM
						JOIN JADWAL_SIDANG JS ON MKS.IDMKS = JS.IDMKS
						JOIN JENISMKS J ON MKS.IDJENISMKS = J.ID
						JOIN RUANGAN R ON JS.IDRuangan = R.IDRUANGAN
						WHERE DP.NIPDosenPenguji = '".$nip."'  AND MKS.isSiapSidang = True) T;");

		return $result;
	}

	function getPaginationAmount($db){
		$result = pg_query("SELECT COUNT(*)
				   FROM Mata_Kuliah_Spesial MKS JOIN JADWAL_SIDANG JS ON MKS.IDJENISMKS = JS.IDMKS
				   WHERE isSiapSidang = True;");
		return $result;
	}

	function retrieveDosenPenguji($db, $idMKS){
		$result = pg_query_params($db,
								  "SELECT D.nama
								   FROM DOSEN_PENGUJI DP JOIN DOSEN D ON DP.NIPDosenPenguji = D.NIP
								   WHERE DP.IDMKS = $1", array($idMKS));
		return $result;
	}

	function retrieveDosenPembimbing($db, $idMKS){
		$result = pg_query_params($db,
								  "SELECT D.nama
								   FROM DOSEN_PEMBIMBING DP JOIN DOSEN D ON DP.NIPDosenPembimbing = D.NIP
								   WHERE DP.IDMKS = $1", array($idMKS));
		return $result;
	}

	function retrieveDosenPembimbingLain($db, $idMKS, $nip){
		$result = pg_query_params($db,
								  "SELECT D.nama
								   FROM DOSEN_PEMBIMBING DP JOIN DOSEN D ON DP.NIPDosenPembimbing = D.NIP
								   WHERE DP.IDMKS = $1 AND DP.NIPDosenPembimbing != $2",
								   array($idMKS, $nip));
		return $result;
	}

	function searchByJenis($db, $jenis, $semester, $tahun){
		$result = pg_query_params($db, 'SELECT MKS.IDMKS, M.Nama, J.NamaMKS, MKS.judul, JS.Tanggal, JS.JamMulai, JS.JamSelesai, R.namaRuangan
				  FROM Mata_Kuliah_Spesial MKS
				  JOIN MAHASISWA M ON M.NPM = MKS.NPM
				  JOIN JADWAL_SIDANG JS ON MKS.IDMKS = JS.IDMKS
				  JOIN JENISMKS J ON MKS.IDJENISMKS = J.ID
				  JOIN RUANGAN R ON JS.IDRuangan = R.IDRUANGAN
				  WHERE MKS.isSiapSidang = True AND J.ID = $1 AND MKS.tahun = $2 AND MKS.semester = $3
				  ORDER BY JS.tanggal, JS.jammulai', array($jenis, $tahun, $semester));

		return $result;
	}

	function searchByNama($db, $nama){
		$result = pg_query_params($db, "SELECT MKS.IDMKS, M.Nama, J.NamaMKS, MKS.judul, JS.Tanggal, JS.JamMulai, JS.JamSelesai, R.namaRuangan
				  FROM Mata_Kuliah_Spesial MKS
				  JOIN MAHASISWA M ON M.NPM = MKS.NPM
				  JOIN JADWAL_SIDANG JS ON MKS.IDMKS = JS.IDMKS
				  JOIN JENISMKS J ON MKS.IDJENISMKS = J.ID
				  JOIN RUANGAN R ON JS.IDRuangan = R.IDRUANGAN
				  WHERE MKS.isSiapSidang = True AND M.nama LIKE $1
				  ORDER BY JS.tanggal, JS.jammulai;", array("%".$nama."%"));
		return $result;
	}

	function retrieveDataMahasiswa($db, $npm){
		$result = pg_query($db, "SELECT * FROM Mahasiswa WHERE npm='".$npm."';");
		return $result;
	}

	function retrieveSidangMahasiswa($db, $npm){
		$result = pg_query_params($db,
								 "SELECT *
								 FROM MATA_KULIAH_SPESIAL MKS
								 JOIN JADWAL_SIDANG JS ON MKS.IDMKS = JS.idMKS
								 JOIN RUANGAN R ON JS.IDRUANGAN = R.IDRUANGAN
								 WHERE MKS.NPM = $1 AND MKS.isSiapSidang = true;",
								 array($npm));

		return $result;
	}
