<?php 
if (!isset($_POST['nip'])){
	header('Location: ../index');
}

require_once('../config.php');
require_once('jadwalSidang.php');

getJadwalDosen($db,$_POST['nip']);

function getJadwalDosen($db,$nip){
	$result = retriveJadwalDosenAll($db, $nip);

	$arr = array();
	while(($row = pg_fetch_assoc($result))){
		$arr[] = array(
			"title" => $row['namamks'].' "'.$row['judul'].'" oleh '.$row['nama'].' sebagai '.$row['role'].' di '.$row['namaruangan'],
			"start" => $row['tanggal'].'T'.$row['jammulai'],
			"end" => $row['tanggal'].'T'.$row['jamselesai']
			);
	}

	echo json_encode($arr);
}