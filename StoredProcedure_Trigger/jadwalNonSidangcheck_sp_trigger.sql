create or replace function jadwalNonSidangCheck() returns TRIGGER
AS $$
DECLARE
	target_date date;
	term int;
BEGIN
	IF extract(month from timestamp 'now()') < 6 THEN
		term := 1;
	ELSE
		term := 2;
	END IF;

	select t.tanggal into target_date
		from timeline t
		where extract(year from timestamp 'now()') = t.tahun and term = t.semester
		and t.namaevent = 'Pengisian jadwal pribadi oleh dosen';
	IF (current_date > target_date) THEN
			BEGIN
				RAISE EXCEPTION 'submission is overdue';
				RETURN NULL;
			END;
    END IF;
  RETURN NEW;
END; $$ language 'plpgsql';

create TRIGGER jadwalNonSidangCheck_trigger
	before insert on jadwal_non_sidang
	for each row execute procedure jadwalNonSidangCheck();