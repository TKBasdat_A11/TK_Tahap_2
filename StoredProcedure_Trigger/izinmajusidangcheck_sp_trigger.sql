create or replace function izinMajuSidangCheck() returns TRIGGER
AS $$
DECLARE
	target_date date;
BEGIN
	select t.tanggal into target_date
		from timeline t, mata_kuliah_spesial mks
		where old.tahun = t.tahun and old.semester = t.semester
		and t.namaevent = 'Pemberian izin maju sidang oleh pembimbing';
	IF ((current_date > target_date) and (old.ijinmajusidang <> new.ijinmajusidang)) THEN
			BEGIN
				RAISE EXCEPTION 'submission is overdue';
				RETURN NULL;
			END;
    END IF;
  RETURN NEW;
END; $$ language 'plpgsql';

create TRIGGER izinMajuSidang_trigger
	before update on mata_kuliah_spesial
	for each row execute procedure izinMajuSidangCheck();