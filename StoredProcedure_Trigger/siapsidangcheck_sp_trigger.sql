create or replace function siapSidangCheck() returns TRIGGER
AS $$
DECLARE
	mks record;
BEGIN
	for mks in select *
		from mata_kuliah_spesial
		where issiapsidang = false
	LOOP
		IF ((mks.pengumpulanhardcopy = true) AND (mks.ijinmajusidang = true)) THEN
			BEGIN
				UPDATE mata_kuliah_spesial
				SET issiapsidang = true
				WHERE idmks = mks.idmks;
				RAISE NOTICE '%', mks.idmks;
			END;
    END IF;
	END LOOP;
	RETURN NULL;
END; $$ language 'plpgsql';

create TRIGGER siapSidang_trigger
	after insert or update on mata_kuliah_spesial
	for each row execute procedure siapSidangCheck();